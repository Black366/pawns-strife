// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;

public class PawnsStrife : ModuleRules
{
	public PawnsStrife(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;

		PublicDependencyModuleNames.AddRange(new string[] 
        {
            "Core",
            "CoreUObject",
            "Engine",
            "InputCore",
            "HeadMountedDisplay",
            "GameSparks",
            "MoviePlayer",
            "OnlineSubsystem",
            "OnlineSubsystemSteam"
        });


        PrivateDependencyModuleNames.AddRange(new string[] 
        {
            "Json",
            "JsonUtilities"
        });
	}
}
