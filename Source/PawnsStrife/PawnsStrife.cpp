// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.

#include "PawnsStrife.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, PawnsStrife, "PawnsStrife" );