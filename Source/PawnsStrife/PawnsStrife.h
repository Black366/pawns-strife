// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"

template<typename T>
T* GetCustomGameMode(UWorld* worldContext)
{
	return Cast<T>(worldContext->GetAuthGameMode());
}

template<typename T>
T* GetCustomGameInstance(UWorld* worldContext)
{
	return Cast<T>(worldContext->GetGameInstance());
}

template<typename T>
T* GetCustomGameState(UWorld* worldContext)
{
	return Cast<T>(worldContext->GetGameState());
}

template<typename T>
T* GetCustomPlayerState(APlayerController* controller)
{
	return Cast<T>(controller->PlayerState);
}

template<typename T>
T* GetCustomPlayerController(UWorld* worldContext)
{
	return Cast<T>(worldContext->GetFirstPlayerController());
}