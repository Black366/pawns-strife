// Fill out your copyright notice in the Description page of Project Settings.

#include "PawnsStrife_GI.h"
#include "GameSparksService.h"
#include "SetsSaveGame.h"
#include "UnitsInSetSaveGame.h"
#include "PawnsStrife.h"
#include "Playing_PC.h"
#include "PawnsStrife_PS.h"
#include "Playing_PS.h"
#include "LoginMenu_PS.h"
#include "MainMenu_PS.h"

#include "Engine/DataTable.h"
#include "OnlineSubsystem.h"
#include "OnlineIdentityInterface.h"
#include "OnlineSessionInterface.h"
#include "OnlineSessionSettings.h"
#include "Private/GameSparksObject.h"
#include "Private/GSApi.h"
#include "Runtime/UMG/Public/Blueprint/UserWidget.h"
#include "Runtime/CoreUObject/Public/UObject/ConstructorHelpers.h"

const static FName SESSION_NAME = TEXT("SessionName");
const static FName SERVER_NAME_SESSION_KEY = TEXT("ServerName");

UPawnsStrife_GI::UPawnsStrife_GI(const FObjectInitializer& ObjectInitializer)
{
	static ConstructorHelpers::FClassFinder<UUserWidget> LoginMenuBPClass(
		TEXT("/Game/Blueprints/UI/LoginMenu/LoginMenu_WB"));
	if (!ensure(LoginMenuBPClass.Class != nullptr)) return;
	LoginMenuClass = LoginMenuBPClass.Class;

	static ConstructorHelpers::FClassFinder<UUserWidget> MainMenuBPClass(
		TEXT("/Game/Blueprints/UI/MainMenu/MainMenu_WB"));
	if (!ensure(MainMenuBPClass.Class != nullptr)) return;
	MainMenuClass = MainMenuBPClass.Class;

	static ConstructorHelpers::FClassFinder<UUserWidget> LoadingScreenBPClass(
		TEXT("/Game/Blueprints/UI/AllLevels/LoadingScreen_WB"));
	if (!ensure(LoadingScreenBPClass.Class != nullptr)) return;
	LoadingScreenClass = LoadingScreenBPClass.Class;

	/*DATA TABLE*/
	/////////////////////////////////////////////////////////////////////////////////////
	static ConstructorHelpers::FObjectFinder<UDataTable> DataTableBPObject(
		TEXT("/Game/Data/SetInfo_DT"));
	if (!ensure(DataTableBPObject.Object != nullptr)) return;
	DataTable = DataTableBPObject.Object;

	/////////////////////////////////////////////////////////////////////////////////////
}

void UPawnsStrife_GI::Init()
{
	Super::Init();

	//create gs object and always first disconnect
	gameSparks = NewObject<UGameSparksObject>(this, UGameSparksObject::StaticClass());
	if (!ensure(gameSparks != nullptr)) return;
	gameSparks->Disconnect();

	//Get the online subsystem
	auto OnlineSub = IOnlineSubsystem::Get();

	if (OnlineSub != nullptr)
	{
		UE_LOG(LogTemp, Warning, TEXT("GI: Found subsystem %s"), *OnlineSub->GetSubsystemName().ToString());

		//get session interface from online subsystem steam and store it
		SessionInterface = OnlineSub->GetSessionInterface();

		//check to see if session interface pointer is pointing to an object
		if (SessionInterface.IsValid())
		{
			//Delegate fired OnCreateSessionComplete function 
			//when a session create request has completed
			SessionInterface->OnCreateSessionCompleteDelegates.
				AddUObject(this, &UPawnsStrife_GI::OnCreateSessionComplete);
			//Delegate fired OnDestroySessionComplete function 
			//when a session destroy request has completed
			SessionInterface->OnDestroySessionCompleteDelegates.
				AddUObject(this, &UPawnsStrife_GI::OnDestroySessionComplete);
			//Delegate fired OnFindSessionComplete function 
			//when the search for an online session has completed
			SessionInterface->OnFindSessionsCompleteDelegates.
				AddUObject(this, &UPawnsStrife_GI::OnFindSessionComplete);
			//Delegate fired OnJoinSessionComplete function 
			//when the search for an online session has completed
			SessionInterface->OnJoinSessionCompleteDelegates.
				AddUObject(this, &UPawnsStrife_GI::OnJoinSessionComplete);
		}
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("GI: Not found subsystem"));

	}

	//Delegate fired BeginLoadingScreen function
	//when a map load begins
	FCoreUObjectDelegates::PreLoadMap.AddUObject(this, &UPawnsStrife_GI::BeginLoadingScreen);

	//initial state to none
	currentState = EGameState::ENone;
	//current widget doesnt exist
	currentWidget = nullptr;
}

void UPawnsStrife_GI::Shutdown()
{
	Super::Shutdown();

	gameSparks->Disconnect();
}

void UPawnsStrife_GI::LoadLoginMenuLevel()
{
	//get local player controller and store it
	auto PlayerController = GetFirstLocalPlayerController();
	if (!ensure(PlayerController != nullptr)) return;

	//travel client to level Login Menu
	PlayerController->ClientTravel("/Game/Levels/LoginMenu", ETravelType::TRAVEL_Absolute);
}

void UPawnsStrife_GI::LoadMainMenuLevel()
{
	//get local player controller and store it
	auto PlayerController = GetFirstLocalPlayerController();
	if (!ensure(PlayerController != nullptr)) return;

	//travel client to level Main Menu
	PlayerController->ClientTravel("/Game/Levels/MainMenu", ETravelType::TRAVEL_Absolute);
}

void UPawnsStrife_GI::ReturnToMainMenu()
{
	UGameplayStatics::OpenLevel(GetWorld(), "/Game/Levels/MainMenu");

	ChangeState(EGameState::EMainMenu);

	if (SessionInterface.IsValid())
		SessionInterface->DestroySession(SESSION_NAME);
}

void UPawnsStrife_GI::BeginLoadingScreen(const FString& MapName)
{
	//set loading screen attributes
	FLoadingScreenAttributes LoadingScreen;
	LoadingScreen.bAutoCompleteWhenLoadingCompletes = false;
	LoadingScreen.WidgetLoadingScreen = FLoadingScreenAttributes::NewTestLoadingScreenWidget();

	//setup loading screen to show on screen
	GetMoviePlayer()->SetupLoadingScreen(LoadingScreen);
}

void UPawnsStrife_GI::LoadLoginMenuWidget()
{
	//ensure login menu class form bp is available
	if (!ensure(LoginMenuClass != nullptr)) return;
	//create widget
	currentWidget = CreateWidget<UUserWidget>(GetWorld()->GetFirstPlayerController(), LoginMenuClass);
	if (!ensure(currentWidget != nullptr)) return;

	//add the widget to the viewport
	currentWidget->AddToViewport();
	//go to the appropriate inout mode
	SetInputMode(EInputMode::EUIOnly, true);
}

void UPawnsStrife_GI::LoadMainMenuWidget()
{
	//ensure main menu class form bp is available
	if (!ensure(MainMenuClass != nullptr)) return;
	//create widget
	currentWidget = CreateWidget<UUserWidget>(GetWorld()->GetFirstPlayerController(), MainMenuClass);
	if (!ensure(currentWidget != nullptr)) return;

	//add the widget to the viewport
	currentWidget->AddToViewport();
	//go to the appropriate inout mode
	SetInputMode(EInputMode::EUIOnly, true);
}

void UPawnsStrife_GI::LoadLoadingScreenWidget()
{
	//ensure main menu class form bp is available
	if (!ensure(LoadingScreenClass != nullptr)) return;
	//create widget
	currentWidget = CreateWidget<UUserWidget>(GetWorld()->GetFirstPlayerController(), LoadingScreenClass);
	if (!ensure(currentWidget != nullptr)) return;

	//add the widget to the viewport
	currentWidget->AddToViewport();
	//go to the appropriate inout mode
	SetInputMode(EInputMode::EUIOnly, false);
}

void UPawnsStrife_GI::ChangeState(EGameState newState)
{
	LeaveState();
	EnterState(newState);
}

void UPawnsStrife_GI::EnterState(EGameState newState)
{
	//set the current state to new state
	currentState = newState;

	switch (currentState)
	{
	case EGameState::ELoadingScreen:
		LoadLoadingScreenWidget();
		break;
	case EGameState::ELoginMenu:
		LoadLoginMenuWidget();
		break;
	case EGameState::EMainMenu:
		LoadMainMenuWidget();
		break;
	case EGameState::EPlaying:
		SetInputMode(EInputMode::EUIAndGame, true);
		break;
	case EGameState::ENone:
		break;
	default:
		break;
	}
}

void UPawnsStrife_GI::LeaveState()
{
	//leave from specific state
	switch (currentState)
	{
	case EGameState::ELoadingScreen:
	case EGameState::ELoginMenu:
	case EGameState::EMainMenu:
	case EGameState::EPlaying:
		if (currentWidget)
		{
			currentWidget->RemoveFromViewport();
			currentWidget = nullptr;
		}
		break;
	case EGameState::ENone:
		break;
	default:
		break;
	}

	//set state to default ENone
	EnterState(EGameState::ENone);
}

EGameState UPawnsStrife_GI::GetGameState()
{
	return currentState;
}

void UPawnsStrife_GI::SetInputMode(EInputMode newInputMode, bool bShowMouseCursor)
{
	//set input mode according to newInputMode
	switch (newInputMode)
	{
	case EInputMode::EUIOnly:
		GetWorld()->GetFirstPlayerController()->SetInputMode(FInputModeUIOnly());
		break;
	case EInputMode::EUIAndGame:
		GetWorld()->GetFirstPlayerController()->SetInputMode(FInputModeGameAndUI());
		break;
	case EInputMode::EGameOnly:
		GetWorld()->GetFirstPlayerController()->SetInputMode(FInputModeGameOnly());
		break;
	default:
		break;
	}

	//show or hide mouse cursor
	GetWorld()->GetFirstPlayerController()->bShowMouseCursor = bShowMouseCursor;

	//retain the values for further use
	currentInputMode = newInputMode;
	bIsShowingMouseCursor = bShowMouseCursor;
}

void UPawnsStrife_GI::SteamLogin()
{
	UE_LOG(LogTemp, Warning, TEXT("Steam Login..."));

	//get gs instance from Game Sparks module and store it
	GameSparks::Core::GS& gs = UGameSparksModule::GetModulePtr()->GetGSInstance();

	GameSparks::Api::Requests::SteamConnectRequest steamAuthRequest(gs);

	auto OnlineSub = IOnlineSubsystem::Get();
	if (!ensure(OnlineSub != nullptr)) return;

	auto IdentityInterface = OnlineSub->GetIdentityInterface();
	if (!ensure(IdentityInterface.IsValid())) return;

	steamAuthRequest.SetSessionTicket(TCHAR_TO_UTF8(*IdentityInterface->GetAuthToken(0)));
	//send request with data to the server, use GameSparksService to response
	steamAuthRequest.Send(UGameSparksService::OnLoginResponse);
}

void UPawnsStrife_GI::LoginResult(bool isSuccess, FLogin_Response loginResponse)
{
	if (isSuccess)
	{
		ALoginMenu_PS* ps = GetCustomPlayerState<ALoginMenu_PS>(GetWorld()->GetFirstPlayerController());
		if (!ensure(ps != nullptr)) return;

		ps->SetPlayerName(loginResponse.DisplayName);
		ps->AuthToken = loginResponse.AuthToken;
		ps->UserId = loginResponse.UserId;
		ps->IsNewPlayer = loginResponse.IsNewPlayer;

		//TODO: change to request gamesparks
		SetupSaveGame();

		if (GEngine)
			GEngine->AddOnScreenDebugMessage(-1, 3.f, FColor::Green, TEXT("Login Successfull"));

		LoadMainMenuLevel();
	}
	else
	{
		if (GEngine)
			GEngine->AddOnScreenDebugMessage(-1, 3.f, FColor::Red, TEXT("Login failed"));

		ChangeState(EGameState::ELoginMenu);
	}
}

void UPawnsStrife_GI::AccountDetailsResp(bool isSuccess, FAccountDetailsResponse accDetailsResp)
{
	if (isSuccess)
	{
		AMainMenu_PS* ps = GetCustomPlayerState<AMainMenu_PS>(GetWorld()->GetFirstPlayerController());
		if (!ensure(ps != nullptr)) return;

		ps->Level = accDetailsResp.Level;
		ps->Gold = accDetailsResp.Gold;
			
		ChangeState(EGameState::EMainMenu);
	}
	else
	{
		if (GEngine)
			GEngine->AddOnScreenDebugMessage(-1, 3.f, FColor::Red,
				TEXT("Account Details Request Failed"));

		LoadLoginMenuLevel();
		ChangeState(EGameState::ELoginMenu);
	}
}

void UPawnsStrife_GI::SetupSaveGame()
{
	if (!ensure(DataTable != nullptr)) return;

	USetsSaveGame* SetsSaveGame = Cast<USetsSaveGame>(
		UGameplayStatics::CreateSaveGameObject(USetsSaveGame::StaticClass()));
	if (!ensure(SetsSaveGame != nullptr)) return;

	UUnitsInSetSaveGame* UnitsInSetsSaveGame = Cast<UUnitsInSetSaveGame>(
		UGameplayStatics::CreateSaveGameObject(UUnitsInSetSaveGame::StaticClass()));
	if (!ensure(UnitsInSetsSaveGame != nullptr)) return;


	//go through all of row names
	for (FName rowName : DataTable->GetRowNames())
	{
		UnitsInSetsSaveGame->Units.Empty();
		//find specific row name and store it
		FTableSetInfo* row = DataTable->FindRow<FTableSetInfo>(rowName, TEXT(""));
		if (!ensure(row != nullptr)) return;

		//add decks names to Sets array in save game object
		SetsSaveGame->SetsNames.Add(row->SetName);

		UGameplayStatics::SaveGameToSlot(SetsSaveGame, "SetsSaveSlot", 0);

		//go through all units in specific set
		for (auto unit : row->UnitsInSetInfo)
			//go through of unit amount
			for (int i = 0; i < unit.Quantity; i++)
				//add names of units to Units array in save game object
				UnitsInSetsSaveGame->Units.Add(unit.UnitName);
		UGameplayStatics::SaveGameToSlot(UnitsInSetsSaveGame, row->SetName, 0);
	}
}

void UPawnsStrife_GI::Host()
{
	//ensure World is	accesible
	if (!ensure(GetWorld() != nullptr)) return;
	auto ps = GetCustomPlayerState<APawnsStrife_PS>(GetWorld()->GetFirstPlayerController());
	if (!ensure(ps != nullptr)) return;

	//set server name that player will host by his name and curren time in seconds
	DesiredServerName = ps->GetPlayerName() +
		FString::SanitizeFloat(UGameplayStatics::GetRealTimeSeconds(GetWorld()));

	//ensure session interface is valid
	if (SessionInterface.IsValid())
	{
		//get session named SESSION_NAME
		auto ExistingSession = SessionInterface->GetNamedSession(SESSION_NAME);
		//check if that session exisst
		if (ExistingSession != nullptr)
			//destroy that session
			SessionInterface->DestroySession(SESSION_NAME);
		else
			//otherwise create session
			CreateSession();
	}
}

void UPawnsStrife_GI::Join()
{
	//ensure session inferface and pointer to session properties are valid
	if (!SessionInterface.IsValid()) return;
	if (!SessionSearch.IsValid()) return;

	//join session with local player, session name and first search result
	SessionInterface->JoinSession(0, SESSION_NAME, SessionSearch->SearchResults[0]);
}

void UPawnsStrife_GI::CreateSession()
{
	//ensure session interface is valid
	if (SessionInterface.IsValid())
	{
		//set property for session
		FOnlineSessionSettings SessionSettings;
		SessionSettings.bIsLANMatch = false;
		SessionSettings.NumPublicConnections = 2;
		SessionSettings.bShouldAdvertise = true;
		SessionSettings.bUsesPresence = true;
		SessionSettings.Set(SERVER_NAME_SESSION_KEY, DesiredServerName,
			EOnlineDataAdvertisementType::ViaOnlineServiceAndPing);

		//create session for local player with session name and settings
		SessionInterface->CreateSession(0, SESSION_NAME, SessionSettings);
	}
}

void UPawnsStrife_GI::FindSessions()
{
	//make pointer of structure FOnlineSessionSearch
	SessionSearch = MakeShareable(new FOnlineSessionSearch());

	if (SessionSearch.IsValid())
	{
		//set property to find session
		SessionSearch->bIsLanQuery = false;
		SessionSearch->MaxSearchResults = 100;
		SessionSearch->QuerySettings.Set(SEARCH_PRESENCE, true, EOnlineComparisonOp::Equals);

		UE_LOG(LogTemp, Warning, TEXT("Starting find session"));

		//start fing sessions with local player and session property
		SessionInterface->FindSessions(0, SessionSearch.ToSharedRef());
	}
}

void UPawnsStrife_GI::StartSession()
{
	//ensure session interface is valid
	if (SessionInterface.IsValid())
	{
		//start session with session name
		SessionInterface->StartSession(SESSION_NAME);
	}
}

void UPawnsStrife_GI::OnCreateSessionComplete(FName SessionName, bool Success)
{
	//if create session failed
	if (!Success)
	{
		UE_LOG(LogTemp, Warning, TEXT("Could not create session"));

		//back to main manu widget
		ChangeState(EGameState::EMainMenu);
		return;
	}

	UE_LOG(LogTemp, Warning, TEXT("Hosting"));

	if (!ensure(GetWorld() != nullptr)) return;

	//travel to lobby with default settings
	GetWorld()->ServerTravel("/Game/Levels/Lobby?listen");
}

void UPawnsStrife_GI::OnDestroySessionComplete(FName SessionName, bool Success)
{
	//check if destroy session is success
	if (Success)
	{
		//create new session
		CreateSession();
	}
}

void UPawnsStrife_GI::OnFindSessionComplete(bool Success)
{
	//ensure finding session is sucess and session search pointer is valid
	if (Success && SessionSearch.IsValid())
	{
		UE_LOG(LogTemp, Warning, TEXT("Finished find session"));

		//check it is find any sessions
		if (SessionSearch->SearchResults.Num() > 0)
		{
			for (const FOnlineSessionSearchResult& SearchResult : SessionSearch->SearchResults)
			{
				UE_LOG(LogTemp, Warning, TEXT("Found session named: %s"), *SearchResult.GetSessionIdStr());
			}
			//if find join on first session
			Join();
		}
		else
		{
			UE_LOG(LogTemp, Warning, TEXT("No find session"));
			//if not find session host game
			Host();
		}
	}
}

void UPawnsStrife_GI::OnJoinSessionComplete(FName SessionName, EOnJoinSessionCompleteResult::Type Result)
{
	//ensure session interface pointer is valid
	if (!SessionInterface.IsValid()) return;

	FString Address;
	//ensure connect string containing session name is valid
	if (!SessionInterface->GetResolvedConnectString(SessionName, Address))
	{
		//if not log and break function
		UE_LOG(LogTemp, Warning, TEXT("Could not get connect string"));
		return;
	}

	if (!ensure(GetEngine() != nullptr)) return;

	GetEngine()->AddOnScreenDebugMessage(0, 5.f, FColor::Green, FString::Printf(TEXT("Joining %s"), *Address));

	auto PlayerController = GetFirstLocalPlayerController();
	if (!ensure(PlayerController != nullptr)) return;

	//client travel to lobby by specific address
	PlayerController->ClientTravel(Address, ETravelType::TRAVEL_Absolute);
}
