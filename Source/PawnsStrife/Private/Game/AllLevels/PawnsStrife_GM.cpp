// Fill out your copyright notice in the Description page of Project Settings.

#include "PawnsStrife_GM.h"
#include "PawnsStrife.h"
#include "PawnsStrife_GI.h"
#include "GameSparksService.h"

#include "OnlineSubsystem.h"
#include "Private/GameSparksObject.h"
#include "Private/GSApi.h"

APawnsStrife_GM::APawnsStrife_GM(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{

}

void APawnsStrife_GM::BeginPlay()
{
	Super::BeginPlay();

	//get game instance and store it
	auto gameInstance = GetCustomGameInstance<UPawnsStrife_GI>(GetWorld());
	if (!ensure(gameInstance != nullptr)) return;

	//be sure gs module is not initialized before we connect
	if (!UGameSparksModule::GetModulePtr()->IsInitialized())
	{
		gameInstance->gameSparks->Disconnect();
		gameInstance->gameSparks->Connect(
			FString("A318662z0ud3"), FString("dRDM3wcodvsmgiBRdmVD3aqy6bWxy4f2"), true, false);
	}

	UGameSparksService::InitStatics(GetWorld());
}

void APawnsStrife_GM::EndPlay(EEndPlayReason::Type reason)
{
	Super::EndPlay(reason);
}

void APawnsStrife_GM::PostLogin(APlayerController* NewPlayer)
{
	Super::PostLogin(NewPlayer);
}

void APawnsStrife_GM::Logout(AController* Exisiting)
{
	Super::Logout(Exisiting);
}

bool APawnsStrife_GM::ReadyToStartMatch_Implementation()
{
	return true;
}

void APawnsStrife_GM::OnGameSparksAvailable(bool available)
{
	//get subsystem and store it
	auto OnlineSub = IOnlineSubsystem::Get();

	//check for online subsystem and game sparks are available
	//show right log
	if (available && OnlineSub)
	{
		UE_LOG(LogTemp, Warning, TEXT(
			"BaseGM: GameSparks and subsystem %s available"), *OnlineSub->GetSubsystemName().ToString());
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("BaseGM: GameSparks or Steam not available"));
	}
}
