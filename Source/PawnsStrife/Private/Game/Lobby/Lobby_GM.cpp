// Fill out your copyright notice in the Description page of Project Settings.

#include "Lobby_GM.h"
#include "PawnsStrife.h"
#include "PawnsStrife_GI.h"

#include "Runtime/Engine/Classes/Engine/World.h"
#include "Runtime/Engine/Classes/Kismet/GameplayStatics.h"

ALobby_GM::ALobby_GM(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	NumPlayers = 0;
}

void ALobby_GM::BeginPlay()
{
	Super::BeginPlay();

	UE_LOG(LogTemp, Warning, TEXT("LobbyGM: BeginPlay"));

	auto gameInstance = GetCustomGameInstance<UPawnsStrife_GI>(GetWorld());
	if (!ensure(gameInstance != nullptr)) return;

	gameInstance->ChangeState(EGameState::ELoadingScreen);
}

void ALobby_GM::EndPlay(EEndPlayReason::Type reason)
{
	Super::EndPlay(reason);

	auto gameInstance = GetCustomGameInstance<UPawnsStrife_GI>(GetWorld());
	if (!ensure(gameInstance != nullptr)) return;

	gameInstance->ChangeState(EGameState::ENone);
}

void ALobby_GM::PostLogin(APlayerController* NewPlayer)
{
	Super::PostLogin(NewPlayer);

	UE_LOG(LogTemp, Warning, TEXT("Post Login: %s"), *NewPlayer->GetName());

	if (!ensure(GetWorld() != nullptr)) return;

	auto gameInstance = GetCustomGameInstance<UPawnsStrife_GI>(GetWorld());
	if (!ensure(gameInstance != nullptr)) return;

	//check if number of players is 2
	if (NumPlayers >= MaxNumberOfPlayers)
	{
		//start session
		gameInstance->StartSession();
		UE_LOG(LogTemp, Warning, TEXT("Start Session"));

		GetWorld()->ServerTravel("/Game/Levels/PlayingMap?listen");
	}
}

void ALobby_GM::Logout(AController* Exisiting)
{
	Super::Logout(Exisiting);

	--NumPlayers;
}

