// Fill out your copyright notice in the Description page of Project Settings.

#include "LoginMenu_GM.h"
#include "PawnsStrife.h"
#include "PawnsStrife_GI.h"
#include "GameSparksService.h"
#include "LoginMenu_PS.h"

#include "OnlineSubsystem.h"
#include "OnlineIdentityInterface.h"
#include "Private/GSApi.h"
#include "Private/GameSparksObject.h"

ALoginMenu_GM::ALoginMenu_GM(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	PlayerStateClass = ALoginMenu_PS::StaticClass();
}

void ALoginMenu_GM::BeginPlay()
{
	Super::BeginPlay();

	UE_LOG(LogTemp, Warning, TEXT("LoginGM: BeginPlay"));

	//ensure World is	accesible
	if (!ensure(GetWorld() != nullptr)) return;

	//get game instance and store it
	auto gameInstance = GetCustomGameInstance<UPawnsStrife_GI>(GetWorld());
	if (!ensure(gameInstance != nullptr)) return;

	//bind delegate to dynamic call function OnGameSparksAvailable
	gameInstance->gameSparks->OnGameSparksAvailableDelegate.AddDynamic(
		this, &ALoginMenu_GM::OnGameSparksAvailable);

	//change state to login menu
	gameInstance->ChangeState(EGameState::ELoginMenu);
}

void ALoginMenu_GM::EndPlay(EEndPlayReason::Type reason)
{
	Super::EndPlay(reason);
}

void ALoginMenu_GM::SteamLogin_Implementation()
{
	UE_LOG(LogTemp, Warning, TEXT("Steam Login..."));

	//get gs instance from Game Sparks module and store it
	GameSparks::Core::GS& gs = UGameSparksModule::GetModulePtr()->GetGSInstance();

	GameSparks::Api::Requests::SteamConnectRequest steamAuthRequest(gs);

	auto OnlineSub = IOnlineSubsystem::Get();
	if (!ensure(OnlineSub != nullptr)) return;

	auto IdentityInterface = OnlineSub->GetIdentityInterface();
	if (!ensure(IdentityInterface.IsValid())) return;

	steamAuthRequest.SetSessionTicket(TCHAR_TO_UTF8(*IdentityInterface->GetAuthToken(0)));

	steamAuthRequest.Send(UGameSparksService::OnLoginResponse);
}

bool ALoginMenu_GM::SteamLogin_Validate()
{
	return true;
}

void ALoginMenu_GM::OnGameSparksAvailable(bool available)
{
	//call base function
	Super::OnGameSparksAvailable(available);

	//get subsystem and store it
	auto OnlineSub = IOnlineSubsystem::Get();

	//check for online subsystem and game sparks are available
	//show right log
	if (available && OnlineSub)
	{
		UE_LOG(LogTemp, Warning, TEXT(
			"LoginGM: GameSparks and subsystem %s available"), *OnlineSub->GetSubsystemName().ToString());
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("LoginGM: GameSparks or subsystem not available"));

		if (!ensure(GetWorld() != nullptr)) return;

		auto playerController = GetWorld()->GetFirstPlayerController();
		if (!ensure(playerController != nullptr)) return;

		playerController->ConsoleCommand("quit");
	}
}

