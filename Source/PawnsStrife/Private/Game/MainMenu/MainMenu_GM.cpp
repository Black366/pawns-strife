// Fill out your copyright notice in the Description page of Project Settings.

#include "MainMenu_GM.h"
#include "PawnsStrife.h"
#include "PawnsStrife_GI.h"
#include "MainMenu_PS.h"
#include "GameSparksService.h"

#include "OnlineSubsystem.h"
#include "Private/GSApi.h"
#include "Private/GameSparksObject.h"


AMainMenu_GM::AMainMenu_GM(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	PlayerStateClass = AMainMenu_PS::StaticClass();
}

void AMainMenu_GM::BeginPlay()
{
	Super::BeginPlay();

	UE_LOG(LogTemp, Warning, TEXT("MainMenuGM: BeginPlay"));

	//ensure World is	accesible
	if (!ensure(GetWorld() != nullptr)) return;

	//get game instance and store it
	auto gameInstance = GetCustomGameInstance<UPawnsStrife_GI>(GetWorld());
	if (!ensure(gameInstance != nullptr)) return;

	//bind delegate to dynamic call function OnGameSparksAvailable
	gameInstance->gameSparks->OnGameSparksAvailableDelegate.AddDynamic(
		this, &AMainMenu_GM::OnGameSparksAvailable);

	//gameInstance->ChangeState(EGameState::EMainMenu);
}

void AMainMenu_GM::EndPlay(EEndPlayReason::Type reason)
{
	Super::EndPlay(reason);
}

void AMainMenu_GM::OnGameSparksAvailable(bool available)
{
	//call base function
	Super::OnGameSparksAvailable(available);

	//get subsystem and store it
	auto OnlineSub = IOnlineSubsystem::Get();

	//check for online subsystem and game sparks are available
	//show right log
	if (available && OnlineSub)
	{
		UE_LOG(LogTemp, Warning, TEXT(
			"MainMenuGM: GameSparks and subsystem %s available"), *OnlineSub->GetSubsystemName().ToString());
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("MainMenuGM: GameSparks or subsystem not available"));

		//get game instance and store it
		auto gameInstance = GetCustomGameInstance<UPawnsStrife_GI>(GetWorld());
		if (!ensure(gameInstance != nullptr)) return;

		//if game sparks or online subsystem is not available return to login menu
		gameInstance->LoadLoginMenuLevel();
	}
}
