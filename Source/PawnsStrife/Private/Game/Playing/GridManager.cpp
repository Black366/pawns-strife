// Fill out your copyright notice in the Description page of Project Settings.

#include "GridManager.h"
#include "Unit.h"
#include "Tile.h"
#include "PawnsStrife.h"
#include "Playing_GS.h"

#include "Runtime/CoreUObject/Public/UObject/ConstructorHelpers.h"
#include "Runtime/Engine/Classes/Components/StaticMeshComponent.h"
#include "Runtime/Engine/Classes/Components/BoxComponent.h"
#include "Runtime/Engine/Classes/Engine/StaticMesh.h"
#include "Runtime/Engine/Classes/Components/InstancedStaticMeshComponent.h"
#include "UnrealNetwork.h"



AGridManager::AGridManager()
{
	bReplicates = true;

	static ConstructorHelpers::FObjectFinder<UStaticMesh> TileMesh(
		TEXT("/Game/Geometry/Meshes/TileStaticMesh"));
	if (!ensure(TileMesh.Object != nullptr)) return;
	DefaultTileMesh = TileMesh.Object;

	static ConstructorHelpers::FObjectFinder<UStaticMesh> TileMoveRangeMesh(
		TEXT("/Game/Geometry/Meshes/RangeMoveTIle_SM"));
	if (!ensure(TileMoveRangeMesh.Object != nullptr)) return;
	RangeMoveTileStaticMesh = TileMoveRangeMesh.Object;

	static ConstructorHelpers::FObjectFinder<UStaticMesh> TileAttackRangeMesh(
		TEXT("/Game/Geometry/Meshes/AttackTileStaticMesh"));
	if (!ensure(TileAttackRangeMesh.Object != nullptr)) return;
	AttackRangeTileStaticMesh = TileAttackRangeMesh.Object;

	static ConstructorHelpers::FObjectFinder<UStaticMesh> TileAttackTrueRangeMesh(
		TEXT("/Game/Geometry/Meshes/AttackTrueTileStaticMesh"));
	if (!ensure(TileAttackTrueRangeMesh.Object != nullptr)) return;
	AttackRangeTrueTileStaticMesh = TileAttackTrueRangeMesh.Object;


	SceneComponent = CreateDefaultSubobject<USceneComponent>(TEXT("Scene Component"));
	if (!ensure(SceneComponent != nullptr)) return;

	RootComponent = SceneComponent;

	TileBoundsX = DefaultTileMesh->GetBounds().GetBox().Max.X - DefaultTileMesh->GetBounds().GetBox().Min.X;
	TileBoundsY = DefaultTileMesh->GetBounds().GetBox().Max.Y - DefaultTileMesh->GetBounds().GetBox().Min.Y;
	TileBoundsZ = DefaultTileMesh->GetBounds().GetBox().Max.Z - DefaultTileMesh->GetBounds().GetBox().Min.Z;

	for (int i = 0; i < GridSizeX * GridSizeY; ++i)
	{
		VectorFieldArray.Add(IndexToLocation(i));
	}

	for (int i = 0; i < VectorFieldArray.Num(); i++)
	{
		UStaticMeshComponent* mesh = CreateDefaultSubobject<UStaticMeshComponent>(FName
		(*("TileMesh" + FString::FromInt(i))));

		mesh->SetStaticMesh(DefaultTileMesh);
		mesh->SetRelativeLocation(VectorFieldArray[i]);
		mesh->AttachToComponent(GetRootComponent(), FAttachmentTransformRules::KeepRelativeTransform);

		mesh->SetCollisionObjectType(ECollisionChannel::ECC_GameTraceChannel1);
		mesh->SetGenerateOverlapEvents(true);
		mesh->SetCollisionEnabled(ECollisionEnabled::QueryAndPhysics);

	}
}

void AGridManager::BeginPlay()
{
	Super::BeginPlay();

	GameStateInstance = GetCustomGameState<APlaying_GS>(GetWorld());
}

const FVector AGridManager::IndexToLocation(const int32 index)
{
	return FVector((index % GridSizeX * TileBoundsX), (index / GridSizeX * TileBoundsY), 0.f);
}

int32 AGridManager::LocationToIndex(FVector location)
{
	float x = location.X + TileBoundsX / 2;
	float y = location.Y + TileBoundsY / 2;

	float x2 = x - GetActorLocation().X;
	float y2 = y - GetActorLocation().Y;

	float x3 = (x2 / TileBoundsX);
	float y3 = (y2 / TileBoundsY);

	int flootXY = FMath::FloorToInt(x3) + (FMath::FloorToInt(y3) * GridSizeX);

	return flootXY;
}

void AGridManager::SpawnMoveMarkerTiles(TArray<int32> indexes)
{
	for (auto index : indexes)
	{
		if (index < 0 && index > 47)
			continue;

		FVector moveRangeTileLocation;
		moveRangeTileLocation.X = VectorFieldArray[index].X;
		moveRangeTileLocation.Y = VectorFieldArray[index].Y;
		moveRangeTileLocation.Z = 2.7f;

		UStaticMeshComponent* RangeMoveTileMeshComponent = NewObject<UStaticMeshComponent>(SceneComponent);
		RangeMoveTileMeshComponent->RegisterComponent();
		RangeMoveTileMeshComponent->AttachToComponent(
			GetRootComponent(), FAttachmentTransformRules::KeepWorldTransform);
		RangeMoveTileMeshComponent->SetStaticMesh(RangeMoveTileStaticMesh);
		RangeMoveTileMeshComponent->SetWorldLocation(moveRangeTileLocation);
		RangeMoveTileMeshComponent->SetRelativeScale3D(FVector(1.8f, 1.8f, 0.f));
		RangeMoveTileMeshComponent->MarkRenderStateDirty();

		RangeMoveTileMeshComponent->SetCollisionObjectType(ECollisionChannel::ECC_Visibility);
		RangeMoveTileMeshComponent->SetGenerateOverlapEvents(false);
		RangeMoveTileMeshComponent->SetCollisionEnabled(ECollisionEnabled::NoCollision);

		RangeMoveTileComponents.Add(RangeMoveTileMeshComponent);
	}
}

void AGridManager::DeleteMoveMarkerTiles()
{
	if (RangeMoveTileComponents.Num() > 0)
	{
		for (auto& tileComponent : RangeMoveTileComponents)
		{
			if (IsValid(tileComponent))
			{
				tileComponent->UnregisterComponent();
				tileComponent->DestroyComponent(false);
			}
		}

		RangeMoveTileComponents.Empty();
	}
}

void AGridManager::SpawnAttackMarkerTiles(TArray<int32> indexes)
{
	for (auto index : indexes)
	{
		if (index < 0 && index > 47)
			continue;

		FVector attackRangeTileLocation;
		attackRangeTileLocation.X = VectorFieldArray[index].X;
		attackRangeTileLocation.Y = VectorFieldArray[index].Y;
		attackRangeTileLocation.Z = -2.4f;

		UStaticMeshComponent* RangeAttackTileMeshComponent = NewObject<UStaticMeshComponent>(SceneComponent);
		RangeAttackTileMeshComponent->RegisterComponent();
		RangeAttackTileMeshComponent->AttachToComponent(
			GetRootComponent(), FAttachmentTransformRules::KeepWorldTransform);

		if (!IsUnitOnTile(index))
			RangeAttackTileMeshComponent->SetStaticMesh(AttackRangeTileStaticMesh);
		else
			RangeAttackTileMeshComponent->SetStaticMesh(AttackRangeTrueTileStaticMesh);


		RangeAttackTileMeshComponent->SetWorldLocation(attackRangeTileLocation);
		RangeAttackTileMeshComponent->SetRelativeScale3D(FVector(2.f, 2.f, 0.f));
		RangeAttackTileMeshComponent->MarkRenderStateDirty();

		RangeAttackTileMeshComponent->SetCollisionObjectType(ECollisionChannel::ECC_Visibility);
		RangeAttackTileMeshComponent->SetGenerateOverlapEvents(false);
		RangeAttackTileMeshComponent->SetCollisionEnabled(ECollisionEnabled::NoCollision);

		RangeAttackTileComponents.Add(RangeAttackTileMeshComponent);
	}
}

void AGridManager::DeleteAttackMarkerTiles()
{
	if (RangeAttackTileComponents.Num() > 0)
	{
		for (auto& tileComponent : RangeAttackTileComponents)
		{
			if (IsValid(tileComponent))
			{
				tileComponent->UnregisterComponent();
				tileComponent->DestroyComponent(false);
			}
		}

		RangeAttackTileComponents.Empty();
	}
}

bool AGridManager::IsUnitOnTile(const int32 index)
{
	bool isOnTile = GameStateInstance->GetAllUnitsOnBoardIndexes().ContainsByPredicate([&](
		int32 unitIndex)
	{
		return (unitIndex == index);
	});

	return isOnTile;
}

void AGridManager::GetLifetimeReplicatedProps(TArray<FLifetimeProperty> & OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(AGridManager, GameStateInstance);
}