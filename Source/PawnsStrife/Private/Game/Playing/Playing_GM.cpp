// Fill out your copyright notice in the Description page of Project Settings.

#include "Playing_GM.h"
#include "Playing_PS.h"
#include "Playing_GS.h"
#include "Playing_PC.h"
#include "PawnsStrife.h"
#include "PawnsStrife_GI.h"

#include "Runtime/Engine/Classes/Kismet/KismetSystemLibrary.h"
#include "Runtime/Engine/Classes/Engine/World.h"
#include "Runtime/Engine/Classes/Kismet/GameplayStatics.h"

#include "Runtime/Engine/Public/TimerManager.h"


APlaying_GM::APlaying_GM(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	PrimaryActorTick.bStartWithTickEnabled = false;
	PrimaryActorTick.bCanEverTick = false;

	DefaultPawnClass = nullptr;
	GameStateClass = APlaying_GS::StaticClass();
	PlayerControllerClass = APlaying_PC::StaticClass();
	PlayerStateClass = APlaying_PS::StaticClass();
}

void APlaying_GM::BeginPlay()
{
	Super::BeginPlay();
	bUseSeamlessTravel = true;
}

void APlaying_GM::EndPlay(EEndPlayReason::Type reason)
{
	Super::EndPlay(reason);
}

void APlaying_GM::PostLogin(APlayerController* NewPlayer)
{
	UE_LOG(LogTemp, Warning, TEXT("Playing_GM Post Login: %s"), *NewPlayer->GetName());

	PlayerControllerList.Add(Cast<APlaying_PC>(NewPlayer));

	APlaying_PS* ps = Cast<APlaying_PS>(NewPlayer->PlayerState);
	if (!ensure(ps != nullptr)) return;

	ps->PawnsStrifePlayerID = PlayerControllerList.Num();

	APlaying_PC* pc = Cast<APlaying_PC>(NewPlayer);
	if (!ensure(pc != nullptr)) return;

	pc->Client_OnPostLoginSetup();

	pc->OnSetupSet();

	Super::PostLogin(NewPlayer);
}

void APlaying_GM::Logout(AController* Exisiting)
{
	Super::Logout(Exisiting);
}

bool APlaying_GM::ReadyToStartMatch_Implementation()
{
	return MaxNumberOfPlayers == NumPlayers;
}

void APlaying_GM::HandleMatchHasStarted()
{
	Super::HandleMatchHasStarted();

	UE_LOG(LogTemp, Warning, TEXT("Playing_GM: Handle Match Has Started, Players in game:"));
	for (APlaying_PC* pc : PlayerControllerList)
		UE_LOG(LogTemp, Warning, TEXT("Id: %d, Name: %s"), pc->PlayerState->PlayerId, *pc->PlayerState->GetPlayerName());

	APlaying_GS* gs = GetCustomGameState<APlaying_GS>(GetWorld());
	if (!ensure(gs != nullptr)) return;

	gs->SetMatchState(MatchState::InProgress);

	FTimerHandle TimerHandle;
	FTimerDelegate TimerDel;

	TimerDel.BindUFunction(gs, FName("Server_OnGameStart"));
	GetWorldTimerManager().SetTimer(TimerHandle, TimerDel, 1.f, false);
}

