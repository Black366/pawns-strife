// Fill out your copyright notice in the Description page of Project Settings.

#include "Playing_GS.h"
#include "Unit.h"
#include "PawnsStrife.h"
#include "Playing_GM.h"
#include "Playing_PC.h"
#include "Playing_PS.h"
#include "PawnsStrife_GI.h"


#include "GameFramework/PlayerState.h"
#include "GameFramework/GameStateBase.h"
#include "UnrealNetwork.h"

#include "Runtime/Engine/Classes/Kismet/GameplayStatics.h"
#include "Runtime/Engine/Classes/Engine/World.h"


void APlaying_GS::OnGameEnd(bool isPlayer1Win)
{
	if (!ensure(Role == ROLE_Authority)) return;

	APlaying_GM* gm = Cast<APlaying_GM>(AuthorityGameMode);
	if (!ensure(gm != nullptr)) return;

	for (auto pc : gm->PlayerControllerList)
	{
		auto tempPC = Cast<APlaying_PC>(pc);
		auto ps = Cast<APlaying_PS>(tempPC->PlayerState);
		tempPC->Client_OnMatchEnd(isPlayer1Win, ps->PawnsStrifePlayerID);

		FTimerHandle UnusedHandle;
		GetWorldTimerManager().SetTimer(
			UnusedHandle, this, &APlaying_GS::ReturnToMainMenu, 5.f, false);
	}
}

TArray<AUnit*> APlaying_GS::GetUnitsOnBoard(const int32 playerID)
{
	if (playerID == 1)
		return UnitsOnBoardPlayer1;
	else
		return UnitsOnBoardPlayer2;
}

TArray<AUnit*> APlaying_GS::GetAllUnitsOnBoard()
{
	return AllUnitsOnBoard;
}

TArray<int> APlaying_GS::GetAllUnitsOnBoardIndexes()
{
	return AllUnitsOnBoardIndexes;
}

AUnit* APlaying_GS::GetUnitByTileIndex(int32 index)
{
	AUnit* tempUnit = nullptr;
	for (auto unit : AllUnitsOnBoard)
	{
		if (unit->CurrentIndex == index)
			tempUnit = unit;
	}

	return tempUnit;
}

void APlaying_GS::AddUnitToBoard(class AUnit* unit, int8 playerID)
{
	if (!ensure(Role == ROLE_Authority)) return;

	if (playerID == 1)
		UnitsOnBoardPlayer1.Add(unit);
	else
		UnitsOnBoardPlayer2.Add(unit);

	AllUnitsOnBoard.Add(unit);
	AllUnitsOnBoardIndexes.Add(unit->CurrentIndex);

	UE_LOG(LogTemp, Warning, TEXT(
		"APlaying_GS: PlayerId: %d Unit Added to board: %s"), playerID, *unit->DataTableName.ToString());
}

void APlaying_GS::RemoveUnitFromBoard(class AUnit* unit, int8 playerID)
{
	if (!ensure(Role == ROLE_Authority)) return;

	if (playerID == 1)
	{
		int32 unitToRemovePlayer1 = UnitsOnBoardPlayer1.Find(unit);
		if (unitToRemovePlayer1 > 0)
			UnitsOnBoardPlayer1.Remove(unit);

		if(UnitsOnBoardPlayer1.Num() <= 6)
			OnGameEnd(false);
	}
	else
	{
		int32 unitToRemovePlayer2 = UnitsOnBoardPlayer2.Find(unit);
		if (unitToRemovePlayer2 > 0)
			UnitsOnBoardPlayer2.Remove(unit);

		if(UnitsOnBoardPlayer2.Num() <= 6)
			OnGameEnd(true);
	}

	int32 unitToRemove = AllUnitsOnBoard.Find(unit);
	if (unitToRemove > 0)
		AllUnitsOnBoard.Remove(unit);

	UpdateBoardUnitsIndexes();
}

void APlaying_GS::UpdateBoardUnitsIndexes()
{
	AllUnitsOnBoardIndexes.Empty();

	for (auto unit : AllUnitsOnBoard)
	{
		AllUnitsOnBoardIndexes.Add(unit->CurrentIndex);
	}
}

void APlaying_GS::GameTimer(int32 playerID)
{
	if (playerID == 1)
	{
		if (GameTimePlayer1.SecondsGameTime <= 0)
		{
			--GameTimePlayer1.MinutesGameTime;
			GameTimePlayer1.SecondsGameTime = 59;
		}
		else
		{
			--GameTimePlayer1.SecondsGameTime;
			if (GameTimePlayer1.MinutesGameTime <= 0 && GameTimePlayer1.SecondsGameTime <= 0)
				OnGameEnd(false);
		}
	}
	else
	{
		if (GameTimePlayer2.SecondsGameTime <= 0)
		{
			--GameTimePlayer2.MinutesGameTime;
			GameTimePlayer2.SecondsGameTime = 59;
		}
		else
		{
			--GameTimePlayer2.SecondsGameTime;
			if (GameTimePlayer2.MinutesGameTime <= 0 && GameTimePlayer2.SecondsGameTime <= 0)
				OnGameEnd(true);
		}
	}
}

void APlaying_GS::HandleGameTimer(int32 playerID)
{
	FTimerDelegate TimerDel;

	TimerDel.BindUFunction(this, FName("GameTimer"), playerID);
	GetWorldTimerManager().SetTimer(TimerHandle, TimerDel, 1.0f, true);
}

void APlaying_GS::ReturnToMainMenu()
{
	if (!ensure(Role == ROLE_Authority)) return;

	UPawnsStrife_GI* gi = Cast<UPawnsStrife_GI>(UGameplayStatics::GetGameInstance(GetWorld()));
	if (!ensure(gi != nullptr)) return;

	gi->ReturnToMainMenu();
}

void APlaying_GS::Server_ChangeTurn_Implementation()
{
	APlaying_GM* gm = Cast<APlaying_GM>(AuthorityGameMode);
	if (!ensure(gm != nullptr)) return;

	//shuffle player turn
	auto playerTurnTemp = PlayerTurnArray[0];
	PlayerTurnArray.Add(playerTurnTemp);
	PlayerTurnArray.RemoveAt(0);

	for (auto pc : gm->PlayerControllerList)
	{
		APlaying_PC* playingPC = Cast<APlaying_PC>(pc);
		if (!ensure(playingPC != nullptr)) return;

		bool IsTurnActive = PlayerTurnArray[0] == playingPC;

		playingPC->ChangePlayerTurnState(IsTurnActive);
	}
}

bool APlaying_GS::Server_ChangeTurn_Validate()
{
	return true;
}

void APlaying_GS::Server_OnGameStart_Implementation()
{
	APlaying_GM* gm = Cast<APlaying_GM>(AuthorityGameMode);
	if (!ensure(gm != nullptr)) return;

	for (auto pc : gm->PlayerControllerList)
	{
		PlayerTurnArray.Add(Cast<APlaying_PC>(pc));
	}

	for (APlaying_PC* pc : PlayerTurnArray)
	{
		pc->OnGameStart();
	}

	for (auto pc : gm->PlayerControllerList)
	{
		APlaying_PC* playingPC = Cast<APlaying_PC>(pc);
		bool IsTurnActive = PlayerTurnArray[0] == playingPC;

		playingPC->ChangePlayerTurnState(IsTurnActive);
	}
}

bool APlaying_GS::Server_OnGameStart_Validate()
{
	return true;
}

void APlaying_GS::GetLifetimeReplicatedProps(TArray<FLifetimeProperty> & OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(APlaying_GS, UnitsOnBoardPlayer1);
	DOREPLIFETIME(APlaying_GS, UnitsOnBoardPlayer2);
	DOREPLIFETIME(APlaying_GS, AllUnitsOnBoard);
	DOREPLIFETIME(APlaying_GS, AllUnitsOnBoardIndexes);

	DOREPLIFETIME(APlaying_GS, GameTimePlayer1);
	DOREPLIFETIME(APlaying_GS, GameTimePlayer2);
}