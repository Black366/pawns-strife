// Fill out your copyright notice in the Description page of Project Settings.

#include "Tile.h"

#include "Runtime/CoreUObject/Public/UObject/ConstructorHelpers.h"
#include "Runtime/Engine/Classes/Engine/StaticMesh.h"


ATile::ATile()
{
	bReplicates = true;

	static ConstructorHelpers::FObjectFinder<UStaticMesh> TileMesh(
		TEXT("/Game/Geometry/Meshes/TileStaticMesh"));
	if (!ensure(TileMesh.Object != nullptr)) return;

	DefaultTileMesh = TileMesh.Object;


	SceneComponent = CreateDefaultSubobject<USceneComponent>(TEXT("Scene Component"));
	if (!ensure(SceneComponent != nullptr)) return;

	RootComponent = SceneComponent;

	TileMeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Tile Mesh Component"));
	TileMeshComponent->SetStaticMesh(DefaultTileMesh);
	TileMeshComponent->AttachToComponent(SceneComponent, FAttachmentTransformRules::KeepRelativeTransform);

	TileMeshComponent->SetCollisionObjectType(ECollisionChannel::ECC_WorldDynamic);
	TileMeshComponent->SetGenerateOverlapEvents(true);
	TileMeshComponent->SetCollisionEnabled(ECollisionEnabled::QueryAndPhysics);

	TileBoundsX = DefaultTileMesh->GetBounds().GetBox().Max.X - DefaultTileMesh->GetBounds().GetBox().Min.X;
	TileBoundsY = DefaultTileMesh->GetBounds().GetBox().Max.Y - DefaultTileMesh->GetBounds().GetBox().Min.Y;
}

const float ATile::GetTileBoundX()
{
	return TileBoundsX;
}

const float ATile::GetTileBoundY()
{
	return TileBoundsY;
}

