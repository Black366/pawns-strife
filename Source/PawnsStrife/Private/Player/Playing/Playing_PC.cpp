// Fill out your copyright notice in the Description page of Project Settings.

#include "Playing_PC.h"
#include "PawnsStrife.h"
#include "PawnsStrife_GI.h"
#include "Playing_PS.h"
#include "Playing_GS.h"
#include "UnitsInSetSaveGame.h"
#include "GridManager.h"
#include "UnitFunctionsLibrary.h"
#include "Unit.h"
#include "Playing_GM.h"
#include "GameUI_UW.h"
#include "PlayingUserPanel_UW.h"
#include "PlayingOponnentPanel_UW.h"
#include "SurrenderPanel_UW.h"
#include "GameResult_UW.h"
#include "PawnsStrife_GI.h"

#include "UnrealNetwork.h"
#include "Runtime/Engine/Classes/Kismet/GameplayStatics.h"
#include "Runtime/Engine/Public/EngineUtils.h"
#include "Runtime/Engine/Classes/Engine/World.h"
#include "Runtime/Engine/Classes/Components/StaticMeshComponent.h"
#include <Runtime/Engine/Classes/Engine/Engine.h>
#include "Runtime/UMG/Public/Blueprint/UserWidget.h"
#include "Runtime/CoreUObject/Public/UObject/ConstructorHelpers.h"
#include "UnrealMathUtility.h"

#include "Runtime/Engine/Public/TimerManager.h"


APlaying_PC::APlaying_PC(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	bShowMouseCursor = true;
	bEnableClickEvents = true;
	bEnableTouchEvents = true;
	bEnableMouseOverEvents = true;
	DefaultClickTraceChannel = ECollisionChannel::ECC_GameTraceChannel2;

	static ConstructorHelpers::FClassFinder<UUserWidget> PlayingPlayerUIBPClass(
		TEXT("/Game/Blueprints/UI/Playing/GameUI_WB"));
	if (!ensure(PlayingPlayerUIBPClass.Class != nullptr)) return;
	PlayingPlayerUIClass = PlayingPlayerUIBPClass.Class;

	static ConstructorHelpers::FClassFinder<UUserWidget> GameResultUIBPClass(
		TEXT("/Game/Blueprints/UI/Playing/GameResult_WB"));
	if (!ensure(GameResultUIBPClass.Class != nullptr)) return;
	GameResultUIClass = GameResultUIBPClass.Class;

	static ConstructorHelpers::FClassFinder<UUserWidget> SurrenderPanelUIBPClass(
		TEXT("/Game/Blueprints/UI/Playing/SurrenderPanel_WB"));
	if (!ensure(SurrenderPanelUIBPClass.Class != nullptr)) return;
	SurrenderPanelUIClass = SurrenderPanelUIBPClass.Class;
}

void APlaying_PC::BeginPlay()
{
	Super::BeginPlay();
}

void APlaying_PC::SetupInputComponent()
{
	Super::SetupInputComponent();

	InputComponent->BindAction("SelectUnit", IE_DoubleClick, this, &APlaying_PC::OnLeftMouseDoubleClicked);
	InputComponent->BindAction("MoveUnit", IE_Pressed, this, &APlaying_PC::OnLeftMouseClicked);
	InputComponent->BindAction("AttackUnit", IE_Pressed, this, &APlaying_PC::OnRightMouseClicked);
	InputComponent->BindAction("DeleteUnit", IE_Pressed, this, &APlaying_PC::OnMiddleMouseClicked);
	InputComponent->BindAction("SurrenderGame", IE_Pressed, this, &APlaying_PC::OnSurrenderGame);

}

void APlaying_PC::Client_OnMatchEnd_Implementation(bool isPlayer1Win, int8 playerID)
{
	if (IsLocalController())
	{
		if (!ensure(GameResultUIClass != nullptr)) return;
		//create widget
		GameResultWidget = CreateWidget<UGameResult_UW>(GetWorld()->GetFirstPlayerController(), GameResultUIClass);
		if (!ensure(GameResultWidget != nullptr)) return;

		//add the widget to the viewport
		GameResultWidget->AddToViewport(2);

		auto gi = Cast<UPawnsStrife_GI>(GetWorld()->GetGameInstance());
		if (!ensure(gi != nullptr)) return;

		gi->SetInputMode(EInputMode::EUIOnly, false);

		GameResultWidget->SetGameResultWidget(isPlayer1Win, playerID);
	}
}

bool APlaying_PC::Client_OnMatchEnd_Validate(bool isPlayer1Win, int8 playerID)
{
	return true;
}

void APlaying_PC::OnGameStart()
{
	if (!ensure(Role == ROLE_Authority)) return;

	for (TActorIterator<AGridManager> GridManagerItr(GetWorld()); GridManagerItr; ++GridManagerItr)
	{
		GridManagerInstance = *GridManagerItr;
	}

	if (!ensure(GridManagerInstance != nullptr)) return;

	auto ps = GetCustomPlayerState<APlaying_PS>(this);
	if (!ensure(ps != nullptr)) return;

	if (ps->PawnsStrifePlayerID == 1)
	{
		for (int i = 0; i < GridManagerInstance->GridSizeX * 2; ++i)
		{
			PlayUnitFromSet(PlayerSet[0], i, FRotator(0.f, 0.f, 0.f));
		}
	}
	else
	{
		for (int i = GridManagerInstance->GridSizeX * GridManagerInstance->GridSizeY - 1;
			i > 35; --i)
		{
			PlayUnitFromSet(PlayerSet[0], i, FRotator(0.f, 180.f, 0.f));
		}
	}
}

void APlaying_PC::PlayUnitFromSet(const FString& unitName, int32 boardIndex, FRotator rotation)
{
	if (!ensure(Role == ROLE_Authority)) return;

	UWorld* world = GetWorld();
	if (!ensure(world != nullptr)) return;

	APlaying_GS* gs = world->GetGameState<APlaying_GS>();

	APlaying_PS* ps = GetCustomPlayerState<APlaying_PS>(this);
	if (!ensure(ps != nullptr)) return;


	FActorSpawnParameters SpawnInfo;
	SpawnInfo.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;

	AUnit* spawnedUnit = GetWorld()->SpawnActor<AUnit>(
		GridManagerInstance->IndexToLocation(boardIndex), rotation, SpawnInfo);

	RemoveUnitFromSet(0);

	UUnitFunctionsLibrary::SetupUnit(spawnedUnit, ps->PawnsStrifePlayerID, unitName);

	gs->AddUnitToBoard(spawnedUnit, ps->PawnsStrifePlayerID);

	Server_UpdatePlayerState();
}

void APlaying_PC::OnSetupSet()
{
	Client_LoadSet();

	Server_UpdatePlayerState();

	UE_LOG(LogTemp, Warning, TEXT("Deck Created"));

	FTimerDelegate TimerDel;
	FTimerHandle TimerHandle;

	TimerDel.BindUFunction(this, FName("Client_RequestChangeState"), EGameState::EPlaying);
	GetWorldTimerManager().SetTimer(TimerHandle, TimerDel, 1.0f, false, 1.f);
}

void APlaying_PC::SetupSet(FString SetName, TArray<FString> Units)
{
	if (Role == ROLE_Authority)
	{
		UE_LOG(LogTemp, Warning, TEXT("PlayerName: %s, Choosen Deck: %s"), *this->GetName(), *SetName);

		FGenericPlatformMath::SRandInit(0);

		for (int32 i = Units.Num() - 1; i > 0; i--)
		{
			int32 j = FMath::FloorToInt(FMath::Rand() * (i + 1)) % Units.Num();
			auto tempSet = Units[i];
			Units[i] = Units[j];
			Units[j] = tempSet;
		}

		PlayerSet = Units;

		auto ps = GetCustomPlayerState<APlaying_PS>(this);
		if (!ensure(ps != nullptr)) return;

		ps->MaxUnitsInSet = Units.Num();
	}
}

void APlaying_PC::Client_LoadSet_Implementation()
{
	Server_ReturnPlayerSet(GetPlayerSetName(), LoadSetUnits());
}

void APlaying_PC::Server_ReturnPlayerSet_Implementation(const FString& SetName, const TArray<FString>& Units)
{
	SetupSet(SetName, Units);
}

bool APlaying_PC::Server_ReturnPlayerSet_Validate(const FString& SetName, const TArray<FString>& Units)
{
	return true;
}

void APlaying_PC::Server_RequestChangeTurn_Implementation()
{
	auto gs = Cast<APlaying_GS>(UGameplayStatics::GetGameState(GetWorld()));
	if (!ensure(gs != nullptr)) return;

	gs->Server_ChangeTurn();
}

bool APlaying_PC::Server_RequestChangeTurn_Validate()
{
	return true;
}

void APlaying_PC::Server_RequestEndGame_Implementation(bool isPlayer1Win)
{
	APlaying_GS* gs = GetWorld()->GetGameState<APlaying_GS>();
	if(!ensure(gs != nullptr)) return;
	
	gs->OnGameEnd(isPlayer1Win);
}

bool APlaying_PC::Server_RequestEndGame_Validate(bool isPlayer1Win)
{
	return true;
}

void APlaying_PC::ChangePlayerTurnState(bool isTurnActive)
{
	if (!ensure(Role == ROLE_Authority)) return;

	if (isTurnActive)
	{
		TurnState = ETurnState::ETurnActive;
		OnRep_TurnState();
		IsTurnActive = true;
		OnRep_IsTurnActive();

		auto ps = Cast<APlaying_PS>(PlayerState);
		if (!ensure(ps != nullptr)) return;
		ps->TurnNumber++;
		ps->OnRep_TurnNumber();

		auto gs = Cast<APlaying_GS>(UGameplayStatics::GetGameState(GetWorld()));
		if (!ensure(gs != nullptr)) return;

		gs->HandleGameTimer(ps->PawnsStrifePlayerID);
		Client_UpdateGameUI();
	}
	else
	{
		TurnState = ETurnState::ETurnInactive;
		IsTurnActive = false;

		Client_UpdateGameUI();
	}
}

void APlaying_PC::Server_RequestDealDamageToUnit_Implementation(AUnit* DamageCauser, class AUnit* unitToDamage)
{
	DealDamageToUnit(DamageCauser, unitToDamage);
}

bool APlaying_PC::Server_RequestDealDamageToUnit_Validate(AUnit* DamageCauser, class AUnit* unitToDamage)
{
	return true;
}

void APlaying_PC::DealDamageToUnit(AUnit* DamageCauser, class AUnit* unitToDamage)
{
	if (IsValid(unitToDamage) && IsValid(DamageCauser))
	{
		unitToDamage->TakesDamage(DamageCauser);

		Server_UpdatePlayerState();
	}
}

void APlaying_PC::Server_OnSelectedUnit_Implementation(AUnit* interactionUnit, int8 selectingPlayerID)
{
	if (IsValid(interactionUnit))
	{
		interactionUnit->OnSelected(selectingPlayerID);
	}
}

bool APlaying_PC::Server_OnSelectedUnit_Validate(AUnit* interactionUnit, int8 selectingPlayerID)
{
	return true;
}

void APlaying_PC::Server_DisableUnitSelection_Implementation(AUnit* unitToDeselection)
{
	if (IsValid(unitToDeselection))
	{
		unitToDeselection->OnDeselected();
	}
}

bool APlaying_PC::Server_DisableUnitSelection_Validate(AUnit* unitToDeselection)
{
	return true;
}

void APlaying_PC::Server_UpdatePlayerState_Implementation()
{
	if (Role == ROLE_Authority)
	{
		if (!ensure(GetWorld() != nullptr)) return;
		auto gs = GetCustomGameState<APlaying_GS>(GetWorld());
		if (!ensure(gs != nullptr)) return;

		auto ps = GetCustomPlayerState<APlaying_PS>(this);
		if (!ensure(ps != nullptr)) return;

		ps->UpdatePlayerUnitsState(
			gs->GetUnitsOnBoard(ps->PawnsStrifePlayerID).Num(), PlayerSet.Num());
	}
}

bool APlaying_PC::Server_UpdatePlayerState_Validate()
{
	return true;
}

void APlaying_PC::Client_OnPostLoginSetup_Implementation()
{
	auto gi = Cast<UPawnsStrife_GI>(GetWorld()->GetGameInstance());
	if (!IsValid(gi)) return;
	gi->ChangeState(EGameState::ELoadingScreen);

	FTimerHandle TimerHandle;
	GetWorld()->GetTimerManager().SetTimer(TimerHandle, this, &APlaying_PC::Client_UpdateGameUI, 2.f, false);

	SetupGameUI();
}

bool APlaying_PC::Client_OnPostLoginSetup_Validate()
{
	return true;
}

void APlaying_PC::SetupGameUI()
{
	if (IsLocalController())
	{
		if (!ensure(PlayingPlayerUIClass != nullptr)) return;
		GameUIWidget = CreateWidget<UGameUI_UW>(GetWorld()->GetFirstPlayerController(), PlayingPlayerUIClass);
		if (!ensure(GameUIWidget != nullptr)) return;

		GameUIWidget->AddToViewport(-3);
	}
}

void APlaying_PC::Client_UpdateGameUI_Implementation()
{
	if (IsValid(GameUIWidget))
	{
		auto ps = Cast<APlaying_PS>(PlayerState);

		if (ps != nullptr)
		{
			GameUIWidget->UserPanel_WB->UpdateTurnImage(IsTurnActive);
			GameUIWidget->UserPanel_WB->UpdatePlayerUserPanel();
			GameUIWidget->UserPanel_WB->UpdateGameTime(IsTurnActive, ps->PawnsStrifePlayerID);


			GameUIWidget->OponnentPanel_WB->UpdateOponnentPanel();
			GameUIWidget->OponnentPanel_WB->UpdateGameTime(IsTurnActive, ps->PawnsStrifePlayerID);
		}
	}
}

bool APlaying_PC::Client_UpdateGameUI_Validate()
{
	return true;
}

void APlaying_PC::RemoveUnitFromSet(const int32 setIndex)
{
	PlayerSet.RemoveAt(setIndex);

	Client_UpdateGameUI();

	Server_UpdatePlayerState();
}

void APlaying_PC::OnLeftMouseDoubleClicked()
{
	if (PlayerStateEnum == EPlayerState::EPendingAction && IsTurnActive)
	{
		if (DetectGridTiles())
		{
			UnitInterations();

			if (IsValid(InteractionUnit))
			{
				bEnableMouseOverEvents = false;
				AllowedMovedIndexes = InteractionUnit->PossibleMovedIndexes();
				GridManagerInstance->SpawnMoveMarkerTiles(AllowedMovedIndexes);

				AllowedAttackIndexes = InteractionUnit->PossibleAttackIndexes();
				GridManagerInstance->SpawnAttackMarkerTiles(AllowedAttackIndexes);
			}
		}
	}
}

void APlaying_PC::OnLeftMouseClicked()
{
	if (PlayerStateEnum == EPlayerState::EUnitInteraction && IsTurnActive)
	{
		if (DetectGridTiles() && IsValid(InteractionUnit))
		{
			if (AllowedMovedIndexes.Contains(HitTileIndex))
			{
				Server_SetUnitLocation(GridManagerInstance->IndexToLocation(HitTileIndex), InteractionUnit);

				GridManagerInstance->DeleteMoveMarkerTiles();
				GridManagerInstance->DeleteAttackMarkerTiles();

				Server_RequestChangeTurn();

				CleanUnitInteractionState();

				bEnableMouseOverEvents = true;
			}
		}
	}
}

void APlaying_PC::OnRightMouseClicked()
{
	if (IsTurnActive && PlayerStateEnum == EPlayerState::EUnitInteraction)
	{
		if (DetectGridTiles() && IsValid(InteractionUnit))
		{
			APlaying_GS* gs = GetWorld()->GetGameState<APlaying_GS>();
			if (!ensure(gs != nullptr)) return;

			if (AllowedAttackIndexes.Contains(HitTileIndex) && GridManagerInstance->IsUnitOnTile(HitTileIndex))
			{
				AUnit* unitToDamage = gs->GetUnitByTileIndex(HitTileIndex);
				if (!ensure(unitToDamage != nullptr)) return;

				//damage to unit
				Server_RequestDealDamageToUnit(InteractionUnit, unitToDamage);

				SetPlayerState(EPlayerState::EPendingAction);

				GridManagerInstance->DeleteMoveMarkerTiles();
				GridManagerInstance->DeleteAttackMarkerTiles();
				CleanUnitInteractionState();
				bEnableMouseOverEvents = true;

				FTimerHandle TimerHandle;
				GetWorld()->GetTimerManager().SetTimer(
					TimerHandle, this, &APlaying_PC::Server_RequestChangeTurn, 2.f, false);
			}
		}
	}
}

void APlaying_PC::OnMiddleMouseClicked()
{
	if (IsTurnActive && PlayerStateEnum == EPlayerState::EUnitInteraction)
	{
		if (IsValid(InteractionUnit))
		{
			//damage to own
			Server_RequestDealDamageToUnit(InteractionUnit, InteractionUnit);

			SetPlayerState(EPlayerState::EPendingAction);

			GridManagerInstance->DeleteMoveMarkerTiles();
			GridManagerInstance->DeleteAttackMarkerTiles();
			bEnableMouseOverEvents = true;

			FTimerHandle TimerHandle;
			GetWorld()->GetTimerManager().SetTimer(
				TimerHandle, this, &APlaying_PC::Server_RequestChangeTurn, 2.f, false);

			CleanUnitInteractionState();
		}
	}
}

void APlaying_PC::OnSurrenderGame()
{
	if (IsLocalController())
	{
		if (!ensure(SurrenderPanelUIClass != nullptr)) return;
		//create widget
		SurrenderPanelWidget = CreateWidget<USurrenderPanel_UW>(GetWorld()->GetFirstPlayerController(), SurrenderPanelUIClass);
		if (!ensure(SurrenderPanelWidget != nullptr)) return;

		//add the widget to the viewport
		SurrenderPanelWidget->AddToViewport(1);

		auto gi = Cast<UPawnsStrife_GI>(GetWorld()->GetGameInstance());
		if (!ensure(gi != nullptr)) return;

		gi->SetInputMode(EInputMode::EUIOnly, true);
	}
}

void APlaying_PC::UnitInterations()
{
	APlaying_GS* gs = GetWorld()->GetGameState<APlaying_GS>();
	if (!ensure(gs != nullptr)) return;

	HitUnit = gs->GetUnitByTileIndex(HitTileIndex);
	if (IsValid(HitUnit))
	{
		APlaying_PS* ps = GetCustomPlayerState<APlaying_PS>(this);
		if (!ensure(ps != nullptr)) return;

		if (HitUnit->OwningPlayerID == ps->PawnsStrifePlayerID)
		{
			if (!IsUnitSelected)
			{
				IsUnitSelected = true;

				SetPlayerState(EPlayerState::EUnitInteraction);

				InteractionUnit = HitUnit;
				Server_OnSelectedUnit(InteractionUnit, ps->PawnsStrifePlayerID);
			}
		}
	}
	else
	{
		CleanUnitInteractionState();
	}
}

bool APlaying_PC::DetectGridTiles()
{
	TArray<TEnumAsByte<EObjectTypeQuery>> ObjectTypes;
	ObjectTypes.Add(EObjectTypeQuery::ObjectTypeQuery1);

	FHitResult Hit;
	bool bHitOccurred = GetHitResultUnderCursorByChannel(UEngineTypes::ConvertToTraceType(ECC_Visibility), true, Hit);

	if (bHitOccurred)
	{
		if (Hit.GetActor()->IsA(AGridManager::StaticClass()))
		{
			if (Hit.GetComponent()->IsInA(AGridManager::StaticClass()))
			{
				UStaticMeshComponent* HitTile = Cast<UStaticMeshComponent>(Hit.GetComponent());
				if (!ensure(GridManagerInstance != nullptr)) return false;

				HitTileIndex = GridManagerInstance->LocationToIndex(HitTile->GetComponentLocation());
				UE_LOG(LogTemp, Warning, TEXT("Tile number %d was clicked"), HitTileIndex);

				return true;
			}
		}
	}

	return false;
}

void APlaying_PC::SetPlayerState(EPlayerState newPlayerState)
{
	if (!(PlayerStateEnum == newPlayerState))
		PlayerStateEnum = newPlayerState;
}

void APlaying_PC::CleanUnitInteractionState()
{
	Server_DisableUnitSelection(InteractionUnit);
	SetPlayerState(EPlayerState::EPendingAction);
	HitUnit = nullptr;
	InteractionUnit = nullptr;
	IsUnitSelected = false;
	HitTileIndex = -1;
}

void APlaying_PC::Client_RequestChangeState_Implementation(EGameState newState)
{
	auto gi = Cast<UPawnsStrife_GI>(GetWorld()->GetGameInstance());
	if (!IsValid(gi)) return;
	gi->ChangeState(newState);
}

void APlaying_PC::Server_SetUnitLocation_Implementation(FVector location, AUnit* unit)
{
	unit->Server_SetUnitLocation(location);
}

bool APlaying_PC::Server_SetUnitLocation_Validate(FVector location, AUnit* unit)
{
	return true;
}

TArray<FString> APlaying_PC::LoadSetUnits()
{
	auto gameInstance = GetCustomGameInstance<UPawnsStrife_GI>(GetWorld());
	check(gameInstance);

	UUnitsInSetSaveGame* UnitsInSetSaveGame = Cast<UUnitsInSetSaveGame>(UGameplayStatics::LoadGameFromSlot(
		gameInstance->SelectedSetName, 0));
	check(UnitsInSetSaveGame);

	return UnitsInSetSaveGame->Units;
}

FString APlaying_PC::GetPlayerSetName()
{
	auto gameInstance = Cast<UPawnsStrife_GI>(GetGameInstance());
	check(gameInstance);

	return gameInstance->SelectedSetName;
}

void APlaying_PC::OnRep_TurnState()
{
	Client_UpdateGameUI();
}

void APlaying_PC::OnRep_IsTurnActive()
{
	Client_UpdateGameUI();
}

void APlaying_PC::GetLifetimeReplicatedProps(TArray<FLifetimeProperty> & OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(APlaying_PC, PlayerSet);
	DOREPLIFETIME(APlaying_PC, TurnState);
	DOREPLIFETIME(APlaying_PC, IsTurnActive);
	DOREPLIFETIME(APlaying_PC, HitUnit);
	DOREPLIFETIME(APlaying_PC, InteractionUnit);

	DOREPLIFETIME(APlaying_PC, GridManagerInstance);
}
