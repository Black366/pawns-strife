// Fill out your copyright notice in the Description page of Project Settings.

#include "Playing_PS.h"
#include "Playing_PC.h"
#include "PawnsStrife_GI.h"

#include "UnrealNetwork.h"
#include "Runtime/Engine/Classes/Kismet/GameplayStatics.h"
#include "Runtime/Engine/Classes/Engine/World.h"




APlaying_PS::APlaying_PS(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
}

void APlaying_PS::UpdatePlayerUnitsState(int32 unitsOnBoard, int32 unitsInSet)
{
	if (Role == ROLE_Authority)
	{
		UnitsOnBoard = unitsOnBoard;
		OnRep_UnitsOnBoard();

		UnitsInSet = unitsInSet;
		OnRep_UnitsInSet();
	}
}

void APlaying_PS::OnRep_UnitsOnBoard()
{
	auto pc = Cast<APlaying_PC>(UGameplayStatics::GetPlayerController(GetWorld(), 0));

	if (pc != nullptr)
		pc->Client_UpdateGameUI();

	UE_LOG(LogTemp, Warning, TEXT(
		"Playing_PS: ID: %d, UnitsOnBoard: %d"), PawnsStrifePlayerID, UnitsOnBoard);
}

void APlaying_PS::OnRep_UnitsInSet()
{
	auto pc = Cast<APlaying_PC>(UGameplayStatics::GetPlayerController(GetWorld(), 0));

	if (pc != nullptr)
		pc->Client_UpdateGameUI();

	UE_LOG(LogTemp, Warning, TEXT(
		"Playing_PS: ID: %d, UnitsInSet: %d"), PawnsStrifePlayerID, UnitsInSet);
}

void APlaying_PS::OnRep_TurnNumber()
{
	auto pc = Cast<APlaying_PC>(UGameplayStatics::GetPlayerController(GetWorld(), 0));

	if (pc != nullptr)
		pc->Client_UpdateGameUI();

	UE_LOG(LogTemp, Warning, TEXT(
		"Playing_PS: ID: %d, TurnNumber: %d"), PawnsStrifePlayerID, TurnNumber);
}

const int32 APlaying_PS::GetUnitsOnBoardCount()
{
	return UnitsOnBoard;
}

const int32 APlaying_PS::GetUnitsInSetCount()
{
	return UnitsInSet;
}

void APlaying_PS::GetLifetimeReplicatedProps(TArray<FLifetimeProperty> & OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(APlaying_PS, PSPlayerName);
	DOREPLIFETIME(APlaying_PS, PSPlayerLevel);
	DOREPLIFETIME(APlaying_PS, PawnsStrifePlayerID);
	DOREPLIFETIME(APlaying_PS, MaxUnitsInSet);
	DOREPLIFETIME(APlaying_PS, TurnNumber);
	DOREPLIFETIME(APlaying_PS, UnitsOnBoard);
	DOREPLIFETIME(APlaying_PS, UnitsInSet);
}
