// Fill out your copyright notice in the Description page of Project Settings.

#include "GameSparksService.h"
#include "PawnsStrife.h"
#include "PawnsStrife_GI.h"
#include "Requests_Responses.h"

#include "Private/GSApi.h"

#include "Json.h"
#include "JsonUtilities.h"

using namespace GameSparks::Core;

/* Init static variables */
UWorld* UGameSparksService::World = nullptr;
bool UGameSparksService::Initialized = false;


void UGameSparksService::InitStatics(UWorld* world)
{
	Initialized = false;
	if (!Initialized)
	{
		if (!ensure(world != nullptr)) return;
		World = world;
		Initialized = true;
	}
}
//
//template <typename StructType>
//StructType UGameSparksService::GetStructFromJsonString(std::string Response)
//{
//
//}


void UGameSparksService::OnLoginResponse
(
	GameSparks::Core::GS& gs,
	const GameSparks::Api::Responses::AuthenticationResponse& response
)
{
	if (Initialized)
	{
		//get game instance from world and store it
		auto gameInstance = GetCustomGameInstance<UPawnsStrife_GI>(World);
		if (!ensure(gameInstance != nullptr)) return;


		if (!response.GetHasErrors())
		{
			UE_LOG(LogTemp, Warning, TEXT("Login Successfull"));

			//change state -> display loading screen widget when response process
			gameInstance->ChangeState(EGameState::ELoadingScreen);

			FLogin_Response logResponse;
			(
				response.GetAuthToken().GetValue().c_str(),
				response.GetDisplayName().GetValue().c_str(),
				response.GetNewPlayer().GetValue() ? TEXT("true") : TEXT("false"),
				response.GetUserId().GetValue().c_str()
			);

			//back to game instance data from success authentication response
			gameInstance->LoginResult(true, logResponse);

			//create account details request after sucessfull login 
			GameSparks::Api::Requests::AccountDetailsRequest accountDetailsRequest(gs);
			//send request to the server, use OnLoginResponse to response
			accountDetailsRequest.Send(UGameSparksService::OnAccountDetailsResponse);
		}
		else
		{
			UE_LOG(LogTemp, Warning, TEXT("Login Failed"));
			//back to game instance default data from failed authentication response
			gameInstance->LoginResult(false);
		}
	}
}

void UGameSparksService::OnAccountDetailsResponse
(
	GameSparks::Core::GS& gs,
	const GameSparks::Api::Responses::AccountDetailsResponse& response
)
{
	if (Initialized)
	{
		//get game instance from world and store it
		auto gameInstance = GetCustomGameInstance<UPawnsStrife_GI>(World);
		if (!ensure(gameInstance != nullptr)) return;

		if (!response.GetHasErrors())
		{
			UE_LOG(LogTemp, Warning, TEXT("Account Details Request Successfull"));

			//create structure and store data from AccountDetails response
			FAccountDetailsResponse accountDetailsResponse
			(
				response.GetDisplayName().GetValue().c_str(),
				response.GetUserId().GetValue().c_str(),
				response.GetScriptData().GetValue().GetInt("Level").GetValue(),
				response.GetCurrencies().GetValue().GetInt("GOLD").GetValue()
			);

			//back to game instance data from success AccountDetails response
			gameInstance->AccountDetailsResp(true, accountDetailsResponse);
		}
		else
		{
			UE_LOG(LogTemp, Warning, TEXT("Account Details Request Failed"));
			//back to game instance default data from failed AccountDetails response
			gameInstance->AccountDetailsResp(false);
		}
	}
}
