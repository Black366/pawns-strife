// Fill out your copyright notice in the Description page of Project Settings.

#include "UnitFunctionsLibrary.h"
#include "PlayerInfo.h"
#include "Unit.h"

#include "Runtime/CoreUObject/Public/UObject/ConstructorHelpers.h"


UDataTable* UUnitFunctionsLibrary::UnitsDataTable = nullptr;


UUnitFunctionsLibrary::UUnitFunctionsLibrary(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	static ConstructorHelpers::FObjectFinder<UDataTable> UnitsDataTableBPObject(
		TEXT("/Game/Data/UnitsInfo_DT"));
	if (!ensure(UnitsDataTableBPObject.Object != nullptr)) return;
	UnitsDataTable = UnitsDataTableBPObject.Object;
}

FUnitInfo UUnitFunctionsLibrary::GetUnitData(const FString& UnitName)
{
	//find specific row name and store it
	FUnitInfo* row = UnitsDataTable->FindRow<FUnitInfo>(FName(*UnitName), TEXT(""));

	return *row;
}

void UUnitFunctionsLibrary::SetupUnit(AUnit* unit, int8 owningPlayerID, FString unitName)
{
	unit->OwningPlayerID = owningPlayerID;

	unit->Server_SetUnitData(unitName);
}
