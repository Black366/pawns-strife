// Fill out your copyright notice in the Description page of Project Settings.

#include "LoginMenu_UW.h"
#include "PawnsStrife.h"
#include "PawnsStrife_GI.h"
#include "LoginMenu_GM.h"

#include "Components/Button.h"


bool ULoginMenu_UW::Initialize()
{
	//call base Initialize function and check return value
	bool success = Super::Initialize();
	if (!success) return false;

	//call LoginPressed function when Login Button is clicked
	if (!ensure(LoginButton != nullptr)) return false;
	LoginButton->OnClicked.AddDynamic(this, &ULoginMenu_UW::LoginPressed);

	//call LoginPressed function when Login Button is clicked
	if (!ensure(QuitButton != nullptr)) return false;
	QuitButton->OnClicked.AddDynamic(this, &ULoginMenu_UW::QuitPressed);

	return true;
}

void ULoginMenu_UW::LoginPressed()
{
	if (!ensure(LoginButton != nullptr)) return;

	//TODO: delete game instance ?
	//get game instance and store it
	//auto gameInstance = GetCustomGameInstance<UPawnsStrife_GI>(GetWorld());
	//if (!ensure(gameInstance != nullptr)) return;
	auto gameMode = GetCustomGameMode<ALoginMenu_GM>(GetWorld());
	if (!ensure(gameMode != nullptr)) return;

	gameMode->SteamLogin();
	//gameInstance->SteamLogin();
}

void ULoginMenu_UW::QuitPressed()
{
	//ensure World is	accesible
	if (!ensure(GetWorld() != nullptr)) return;

	//get first player controller from world and store it
	auto PlayerController = GetWorld()->GetFirstPlayerController();
	if (!ensure(PlayerController != nullptr)) return;

	//execute quit command -> quit game
	PlayerController->ConsoleCommand("quit");
}
