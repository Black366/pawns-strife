// Fill out your copyright notice in the Description page of Project Settings.

#include "CollectionMenu_UW.h"
#include "MainMenu_UW.h"
#include "GameSparksService.h"
#include "UnitCollection_UW.h"
#include "SetCollectionRow_UW.h"
#include "Requests_Responses.h"

#include "Private/GSApi.h"
#include "Components/Button.h"
#include "Runtime/UMG/Public/Components/UniformGridPanel.h"
#include "Runtime/UMG/Public/Components/UniformGridSlot.h"
#include "Runtime/UMG/Public/Components/VerticalBox.h"
#include "Runtime/CoreUObject/Public/UObject/ConstructorHelpers.h"

#include "Json.h"
#include "JsonUtilities.h"


UCollectionMenu_UW::UCollectionMenu_UW(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	static ConstructorHelpers::FClassFinder<UUserWidget> UnitCollectionBPClass(
		TEXT("/Game/Blueprints/UI/MainMenu/UnitCollection_WB"));
	if (!ensure(UnitCollectionBPClass.Class != nullptr)) return;
	UnitCollectionClass = UnitCollectionBPClass.Class;

	static ConstructorHelpers::FClassFinder<UUserWidget> SetCollectionRowBPClass(
		TEXT("/Game/Blueprints/UI/MainMenu/SetCollectionRow_WB"));
	if (!ensure(SetCollectionRowBPClass.Class != nullptr)) return;
	SetCollectionRowClass = SetCollectionRowBPClass.Class;
}

bool UCollectionMenu_UW::Initialize()
{
	bool success = Super::Initialize();
	if (!success) return false;

	//call SavePressed function when Save Button is clicked
	if (!ensure(SaveButton != nullptr)) return false;
	SaveButton->OnClicked.AddDynamic(this, &UCollectionMenu_UW::SavePressed);

	//call ReturnPressed function when Return Button is clicked
	if (!ensure(Returnbutton != nullptr)) return false;
	Returnbutton->OnClicked.AddDynamic(this, &UCollectionMenu_UW::ReturnPressed);

	return true;
}

void UCollectionMenu_UW::RefreshUnitsList()
{
	if (!ensure(UnitsUniformGridPanel != nullptr)) return;

	UnitsUniformGridPanel->ClearChildren();

	GameSparks::Core::GS& gs = UGameSparksModule::GetModulePtr()->GetGSInstance();

	GameSparks::Api::Requests::ListVirtualGoodsRequest virtualGoodsRequest(gs);

	virtualGoodsRequest.Send([&](GameSparks::Core::GS& gs,
		const GameSparks::Api::Responses::ListVirtualGoodsResponse& response)
	{
		if (!response.GetHasErrors())
		{
			//TODO: refactor?
			//auto virtualGoods = response.GetVirtualGoods();

			int8 row = 0, column = 0, i = 0;

			for (auto virtualGood : response.GetVirtualGoods())
			{
				if (virtualGood.GetName().HasValue())
				{
					if (!ensure(UnitCollectionClass != nullptr)) return;

					UUnitCollection_UW* unitCollection = CreateWidget<UUnitCollection_UW>(
						GetWorld()->GetFirstPlayerController(), UnitCollectionClass);

					unitCollection->SetUnitCollectionData
					(
						virtualGood.GetName().GetValue().c_str(),
						virtualGood.GetPropertySet().GetValue().GetGSDataObject(virtualGood.GetShortCode().GetValue()
							+ "_AP").GetValue().GetInt("attackPoints").GetValue(),
						virtualGood.GetPropertySet().GetValue().GetGSDataObject(virtualGood.GetShortCode().GetValue()
							+ "_HP").GetValue().GetInt("healthPoints").GetValue(),
						0
					);


					if (UUniformGridSlot* slot = UnitsUniformGridPanel->AddChildToUniformGrid(unitCollection))
					{
						column = i % 4;

						slot->SetColumn(column);
						slot->SetRow(row);

						if (column == 3)
							row++;
						i++;
					}
				}
			}
		}
		else
		{
			UE_LOG(LogTemp, Warning, TEXT("ListVirtualGoodsResponse error"));

			//TODO: change ?
			ReturnPressed();
		}
	});
}

void UCollectionMenu_UW::RefreshSetsList()
{
	if (!ensure(SetsVerticalBox != nullptr)) return;

	SetsVerticalBox->ClearChildren();

	GameSparks::Core::GS& gs = UGameSparksModule::GetModulePtr()->GetGSInstance();

	GameSparks::Api::Requests::LogEventRequest setsRequest(gs);
	setsRequest.SetEventKey("Get_Sets");


	setsRequest.Send([&](GameSparks::Core::GS& gs,
		const GameSparks::Api::Responses::LogEventResponse& response)
	{
		if (!response.GetHasErrors())
		{
			//UE_LOG(LogTemp, Warning, TEXT("%s"), *FString(UTF8_TO_TCHAR(response.GetJSONString().c_str())));

			FCollectionSetsResponse collectionSets =
				UGameSparksService::GetStructFromJsonString<FCollectionSetsResponse>(
					FString(UTF8_TO_TCHAR(response.GetJSONString().c_str())));

			for (FCollectionSetResponse set : collectionSets.sets)
			{
				//UE_LOG(LogTemp, Warning, TEXT("Name: %s"), *set.Name);

				if (!ensure(SetCollectionRowClass != nullptr)) return;

				USetCollectionRow_UW* setCollectionRow = CreateWidget<USetCollectionRow_UW>(
					GetWorld()->GetFirstPlayerController(), SetCollectionRowClass);
				if (!ensure(setCollectionRow != nullptr)) return;

				setCollectionRow->SetupRow(set.Name);

				SetsVerticalBox->AddChild(setCollectionRow);

				setCollectionRow->ParentWidget = this;

				//TODO: delete this and create in collection row action when click then show units in that set
				for (auto unit : set.Units)
				{
					UE_LOG(LogTemp, Warning, TEXT("%s"), *unit);
				}
			}

		}
		else
		{
			UE_LOG(LogTemp, Warning, TEXT("GetSetsResponse error"));
		}
	});
}

void UCollectionMenu_UW::SavePressed()
{
	//TODO: request save sets and units

	if (!ensure(ParentWidget != nullptr)) return;

	UMainMenu_UW* mainMenuWidget = Cast<UMainMenu_UW>(ParentWidget);
	if (!ensure(mainMenuWidget != nullptr)) return;

	mainMenuWidget->OpenMainPanel();
}

void UCollectionMenu_UW::ReturnPressed()
{
	if (!ensure(ParentWidget != nullptr)) return;

	UMainMenu_UW* mainMenuWidget = Cast<UMainMenu_UW>(ParentWidget);
	if (!ensure(mainMenuWidget != nullptr)) return;

	mainMenuWidget->OpenMainPanel();
}
