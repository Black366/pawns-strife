// Fill out your copyright notice in the Description page of Project Settings.

#include "MainMenu_UW.h"
#include "PawnsStrife.h"
#include "PawnsStrife_GI.h"
#include "MainMenu_PS.h"
#include "SetSelectionMenu_UW.h"
#include "UserPanelMenu_UW.h"
#include "CollectionMenu_UW.h"

#include "Private/GSApi.h"
#include "GameSparksService.h"
#include "Components/Button.h"
#include "Components/WidgetSwitcher.h"


bool UMainMenu_UW::Initialize()
{
	//call base Initialize function and check return value
	bool success = Super::Initialize();
	if (!success) return false;

	if (!ensure(SetSelectionMenu_WB != nullptr)) return false;
	SetSelectionMenu_WB->ParentWidget = this;

	if (!ensure(UserPanelMenu_WB != nullptr)) return false;
	UserPanelMenu_WB->ParentWidget = this;

	if (!ensure(CollectionMenu_WB != nullptr)) return false;
	CollectionMenu_WB->ParentWidget = this;

	//auto gameInstance = GetCustomGameInstance<UPawnsStrife_GI>(GetWorld());
	//if (!ensure(gameInstance != nullptr)) return false;
	auto ps = GetCustomPlayerState<AMainMenu_PS>(GetWorld()->GetFirstPlayerController());

	if (IsValid(ps))
		UserPanelMenu_WB->SetUserPanelMenuData(
			ps->GetPlayerName(), ps->Level, ps->Gold);

	//Delegate fired open muliplayer sub menu when Multiplayer Button is clicked
	if (!ensure(MultiplayerButton != nullptr)) return false;
	MultiplayerButton->OnClicked.AddDynamic(this, &UMainMenu_UW::OpenMultiplayerMenu);
	//Delegate fired Finding Oponent Panel when FastGameButton is clicked
	if (!ensure(FastGameButton != nullptr)) return false;
	FastGameButton->OnClicked.AddDynamic(this, &UMainMenu_UW::OpenSelectionDeckMenu);


	if (!ensure(CollectionPanelButton != nullptr)) return false;
	CollectionPanelButton->OnClicked.AddDynamic(this, &UMainMenu_UW::OpenCollectionPanelMenu);

	//Delegate fired open singleplayer sub menu when Singleplayer Button is clicked
	if (!ensure(SingleplayerButton != nullptr)) return false;
	SingleplayerButton->OnClicked.AddDynamic(this, &UMainMenu_UW::OpenSingleplayerMenu);
	//Delegate fired open collection sub menu when Collection Button is clicked
	if (!ensure(CollectionButton != nullptr)) return false;
	CollectionButton->OnClicked.AddDynamic(this, &UMainMenu_UW::OpenCollectionMenu);
	//Delegate fired open options sub menu when Options Button is clicked
	if (!ensure(ShopButton != nullptr)) return false;
	ShopButton->OnClicked.AddDynamic(this, &UMainMenu_UW::OpenShopMenu);
	//Delegate fired QuitPressed function when Quit Button is clicked
	if (!ensure(ExitButton != nullptr)) return false;
	ExitButton->OnClicked.AddDynamic(this, &UMainMenu_UW::QuitPressed);

	return true;
}

void UMainMenu_UW::OpenMultiplayerMenu()
{
	//ensure switcher and sub menu are not null
	if (!ensure(MenuFieldsWidgetSwitcher != nullptr)) return;
	if (!ensure(MultiplayerMenu != nullptr)) return;

	//set active widget to Mulitplayer sub menu
	MenuFieldsWidgetSwitcher->SetActiveWidget(MultiplayerMenu);
}

void UMainMenu_UW::OpenSingleplayerMenu()
{
	//ensure switcher and sub menu are not null
	if (!ensure(MenuFieldsWidgetSwitcher != nullptr)) return;
	if (!ensure(SingleplayerMenu != nullptr)) return;

	//set active widget to Singleplayer sub menu
	MenuFieldsWidgetSwitcher->SetActiveWidget(SingleplayerMenu);
}

void UMainMenu_UW::OpenCollectionMenu()
{
	//ensure switcher and sub menu are not null
	if (!ensure(MenuFieldsWidgetSwitcher != nullptr)) return;
	if (!ensure(CollectionMenu != nullptr)) return;

	//set active widget to Collection sub menu
	MenuFieldsWidgetSwitcher->SetActiveWidget(CollectionMenu);
}

void UMainMenu_UW::OpenShopMenu()
{
	//ensure switcher and sub menu are not null
	if (!ensure(MenuFieldsWidgetSwitcher != nullptr)) return;
	if (!ensure(ShopMenu != nullptr)) return;

	//set active widget to Options sub menu
	MenuFieldsWidgetSwitcher->SetActiveWidget(ShopMenu);
}

void UMainMenu_UW::QuitPressed()
{
	//get world and store it
	UWorld* World = GetWorld();
	if (!ensure(World != nullptr)) return;

	//get first player controller from world and store it
	APlayerController* PlayerController = World->GetFirstPlayerController();
	if (!ensure(PlayerController != nullptr)) return;

	//execute console command -> quit
	PlayerController->ConsoleCommand("quit");
}


void UMainMenu_UW::OpenMainPanel()
{
	if(!ensure(PanelWidgetSwitcher != nullptr)) return;
	if (!ensure(MainPanel != nullptr)) return;

	PanelWidgetSwitcher->SetActiveWidget(MainPanel);
}

void UMainMenu_UW::OpenSelectionDeckMenu()
{
	//ensure main switcher and set selection widget are not null
	if(!ensure(PanelWidgetSwitcher != nullptr)) return;
	if (!ensure(SetSelectionMenu_WB != nullptr)) return;

	SetSelectionMenu_WB->RefreshDeckList();

	 
	PanelWidgetSwitcher->SetActiveWidget(SetSelectionMenu_WB);
}
	
void UMainMenu_UW::OpenCollectionPanelMenu()
{
	if (!ensure(PanelWidgetSwitcher != nullptr)) return;
	if (!ensure(CollectionMenu_WB != nullptr)) return;

	//TODO: change to one function
	CollectionMenu_WB->RefreshUnitsList();
	CollectionMenu_WB->RefreshSetsList();

	PanelWidgetSwitcher->SetActiveWidget(CollectionMenu_WB);
}

void UMainMenu_UW::StartFindOpponent()
{
	//ensure World is	accesible
	if (!ensure(GetWorld() != nullptr)) return;

	//get game instance and store it
	auto gameInstance = GetCustomGameInstance<UPawnsStrife_GI>(GetWorld());
	if (!ensure(gameInstance != nullptr)) return;

	UE_LOG(LogTemp, Warning, TEXT("Selected Deck: %s"), *gameInstance->SelectedSetName);

	//change state to loading screen
	gameInstance->ChangeState(EGameState::ELoadingScreen);

	//start find session
	gameInstance->FindSessions();
}

