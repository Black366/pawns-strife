// Fill out your copyright notice in the Description page of Project Settings.

#include "SetCollectionRow_UW.h"
#include "CollectionMenu_UW.h"

#include "Runtime/UMG/Public/Components/TextBlock.h"
#include "Components/Button.h"


bool USetCollectionRow_UW::Initialize()
{
	//call base Initialize function and check return value
	bool success = Super::Initialize();
	if (!success) return false;

	if (!ensure(SetButton != nullptr)) return false;
	SetButton->OnClicked.AddDynamic(
		this, &USetCollectionRow_UW::OnSelectionButtonClicked);

	return true;
}

void USetCollectionRow_UW::SetupRow(FString setName)
{
	SetName = setName;

	if (!ensure(SetNameTextBlock != nullptr)) return;
	SetNameTextBlock->Text = FText::FromString(SetName);
}

void USetCollectionRow_UW::OnSelectionButtonClicked()
{
	if (!ensure(ParentWidget != nullptr)) return;

	UCollectionMenu_UW* collectionMenuWidget = Cast<UCollectionMenu_UW>(ParentWidget);
	if (!ensure(collectionMenuWidget != nullptr)) return;
}


