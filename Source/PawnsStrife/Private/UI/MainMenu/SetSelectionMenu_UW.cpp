// Fill out your copyright notice in the Description page of Project Settings.

#include "SetSelectionMenu_UW.h"
#include "SetsSaveGame.h"
#include "SetSelectionRow_UW.h"
#include "MainMenu_UW.h"

#include "Runtime/UMG/Public/Components/PanelWidget.h"
#include "Runtime/Engine/Classes/Kismet/GameplayStatics.h"
#include "Runtime/CoreUObject/Public/UObject/ConstructorHelpers.h"
#include "Components/Button.h"



USetSelectionMenu_UW::USetSelectionMenu_UW(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	static ConstructorHelpers::FClassFinder<UUserWidget> SetSelectionRowBPClass(
		TEXT("/Game/Blueprints/UI/MainMenu/SetSelectionRow_WB"));
	if (!ensure(SetSelectionRowBPClass.Class != nullptr)) return;
	SetSelectionRowClass = SetSelectionRowBPClass.Class;
}

bool USetSelectionMenu_UW::Initialize()
{
	//call base Initialize function and check return value
	bool success = Super::Initialize();
	if (!success) return false;

	//Delegate fired return to main panel when Return Button is clicked
	if (!ensure(ReturnButton != nullptr)) return false;
	ReturnButton->OnClicked.AddDynamic(this, &USetSelectionMenu_UW::ReturnToMainPanel);

	return true;
}

void USetSelectionMenu_UW::RefreshDeckList()
{
	if (!ensure(SetsVerticalBox != nullptr)) return;
	SetsVerticalBox->ClearChildren();

	USetsSaveGame* SetSaveGame = Cast<USetsSaveGame>(
		UGameplayStatics::LoadGameFromSlot("SetsSaveSlot", 0));
	if (!ensure(SetSaveGame != nullptr)) return;

	for (auto setName : SetSaveGame->SetsNames)
	{
		if (!ensure(SetSelectionRowClass != nullptr)) return;

		USetSelectionRow_UW* setSelectionRow = CreateWidget<USetSelectionRow_UW>(
			GetWorld()->GetFirstPlayerController(), SetSelectionRowClass);
		if (!ensure(setSelectionRow != nullptr)) return;

		setSelectionRow->SetupRow(setName);

		SetsVerticalBox->AddChild(setSelectionRow);

		setSelectionRow->ParentWidget = this;
	}
}

void USetSelectionMenu_UW::ReturnToMainPanel()
{
	if (!ensure(ParentWidget != nullptr)) return;

	UMainMenu_UW* mainMenuWidget = Cast<UMainMenu_UW>(ParentWidget);
	if (!ensure(mainMenuWidget != nullptr)) return;

	mainMenuWidget->OpenMainPanel();
}
