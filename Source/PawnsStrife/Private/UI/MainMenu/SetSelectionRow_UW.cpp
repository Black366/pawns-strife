// Fill out your copyright notice in the Description page of Project Settings.

#include "SetSelectionRow_UW.h"
#include "PawnsStrife.h"
#include "SetSelectionMenu_UW.h"
#include "MainMenu_UW.h"
#include "PawnsStrife_GI.h"

#include "Runtime/UMG/Public/Components/TextBlock.h"
#include "Components/Button.h"


bool USetSelectionRow_UW::Initialize()
{
	//call base Initialize function and check return value
	bool success = Super::Initialize();
	if (!success) return false;

	if (!ensure(SetButton != nullptr)) return false;
	SetButton->OnClicked.AddDynamic(
		this, &USetSelectionRow_UW::OnSelectionButtonClicked);

	return true;
}

void USetSelectionRow_UW::SetupRow(FString setName)
{
	SetName = setName;

	if (!ensure(SetNameTextBlock != nullptr)) return;
	SetNameTextBlock->Text = FText::FromString(SetName);
}

void USetSelectionRow_UW::OnSelectionButtonClicked()
{
	if (!ensure(ParentWidget != nullptr)) return;

	USetSelectionMenu_UW* setSelectionMenuWidget = Cast<USetSelectionMenu_UW>(ParentWidget);
	if (!ensure(setSelectionMenuWidget != nullptr)) return;


	if (!ensure(setSelectionMenuWidget->ParentWidget != nullptr)) return;

	UMainMenu_UW* mainMenuWidget = Cast<UMainMenu_UW>(setSelectionMenuWidget->ParentWidget);
	if (!ensure(mainMenuWidget != nullptr)) return;

	auto gameInstance = GetCustomGameInstance<UPawnsStrife_GI>(GetWorld());
	if (!ensure(gameInstance != nullptr)) return;

	gameInstance->SelectedSetName = SetName;

	mainMenuWidget->StartFindOpponent();
}
