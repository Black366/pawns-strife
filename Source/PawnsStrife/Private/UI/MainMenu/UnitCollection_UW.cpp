// Fill out your copyright notice in the Description page of Project Settings.

#include "UnitCollection_UW.h"

#include "Runtime/UMG/Public/Components/TextBlock.h"


bool UUnitCollection_UW::Initialize()
{
	bool success = Super::Initialize();
	if (!success) return false;

	return true;
}

void UUnitCollection_UW::SetUnitCollectionData(FString name, int32 attackPoints, int32 healthPoints, int32 count)
{
	if (!ensure(NameTextBlock != nullptr)) return;
	NameTextBlock->Text = FText::FromString(name);

	if (!ensure(AttackPointsTextBlock != nullptr)) return;
	AttackPointsTextBlock->Text = FText::AsNumber(attackPoints);

	if (!ensure(HealthPointsTextBlock != nullptr)) return;
	HealthPointsTextBlock->Text = FText::AsNumber(healthPoints);

	if (!ensure(CountTextBlock != nullptr)) return;
	CountTextBlock->Text = FText::AsNumber(count);
}

