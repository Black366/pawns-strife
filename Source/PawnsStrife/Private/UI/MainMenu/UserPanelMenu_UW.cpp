// Fill out your copyright notice in the Description page of Project Settings.

#include "UserPanelMenu_UW.h"

#include "Runtime/UMG/Public/Components/TextBlock.h"


bool UUserPanelMenu_UW::Initialize()
{
	bool success = Super::Initialize();
	if (!success) return false;

	return true;
}

void UUserPanelMenu_UW::SetUserPanelMenuData(FString playerName, int level, int gold)
{
	if (!ensure(PlayerNameTextBlock != nullptr)) return;
	PlayerNameTextBlock->SetText(FText::FromString(playerName));

	if (!ensure(LevelTextBlock != nullptr)) return;
	LevelTextBlock->SetText(FText::AsNumber(level));

	if (!ensure(GoldTextBlock != nullptr)) return;
	GoldTextBlock->SetText(FText::AsNumber(gold));
}
