// Fill out your copyright notice in the Description page of Project Settings.

#include "GameResult_UW.h"

#include "Runtime/UMG/Public/Components/TextBlock.h"


bool UGameResult_UW::Initialize()
{
	bool success = Super::Initialize();
	if (!success) return false;

	return true;
}

void UGameResult_UW::SetGameResultWidget(bool isPlayer1Win, int8 playerID)
{
	if (isPlayer1Win)
	{
		if (playerID == 1)
		{
			if (!ensure(ResultGameTextBlock != nullptr)) return;
			ResultGameTextBlock->SetText(FText::FromString("Victory"));
		}
		else
		{
			if (!ensure(ResultGameTextBlock != nullptr)) return;
			ResultGameTextBlock->SetText(FText::FromString("Defeat"));
		}

	}
	else
	{
		if (playerID == 1)
		{
			if (!ensure(ResultGameTextBlock != nullptr)) return;
			ResultGameTextBlock->SetText(FText::FromString("Defeat"));
		}
		else
		{
			if (!ensure(ResultGameTextBlock != nullptr)) return;
			ResultGameTextBlock->SetText(FText::FromString("Victory"));
		}
	}
}
