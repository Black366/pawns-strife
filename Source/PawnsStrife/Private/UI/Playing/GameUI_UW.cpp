// Fill out your copyright notice in the Description page of Project Settings.

#include "GameUI_UW.h"
#include "PlayingUserPanel_UW.h"
#include "PlayingOponnentPanel_UW.h"

#include "Runtime/UMG/Public/Components/Image.h"


bool UGameUI_UW::Initialize()
{
	bool success = Super::Initialize();
	if (!success) return false;

	if (!ensure(UserPanel_WB != nullptr)) return false;
	//set parents widgets
	UserPanel_WB->ParentWidget = this;

	if (!ensure(OponnentPanel_WB != nullptr)) return false;
	//set parents widgets
	OponnentPanel_WB->ParentWidget = this;

	return true;
}

