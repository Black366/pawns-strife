// Fill out your copyright notice in the Description page of Project Settings.

#include "PlayingOponnentPanel_UW.h"
#include "Playing_GS.h"
#include "Playing_PC.h"
#include "Playing_PS.h"

#include "Runtime/Engine/Classes/Engine/World.h"
#include "Runtime/UMG/Public/Components/TextBlock.h"

#include "Runtime/Engine/Public/TimerManager.h"
#include "Runtime/Engine/Classes/Kismet/GameplayStatics.h"
#include "Runtime/Engine/Classes/Engine/World.h"

bool UPlayingOponnentPanel_UW::Initialize()
{
	bool success = Super::Initialize();
	if (!success) return false;

	FTimerHandle TimerHandle1;
	FTimerHandle TimerHandle2;

	GetWorld()->GetTimerManager().SetTimer(
		TimerHandle1, this, &UPlayingOponnentPanel_UW::SetTextGameTimePlayer1, 0.3f, true);

	GetWorld()->GetTimerManager().SetTimer(
		TimerHandle2, this, &UPlayingOponnentPanel_UW::SetTextGameTimePlayer2, 0.3f, true);

	return true;
}

void UPlayingOponnentPanel_UW::UpdateOponnentPanel()
{
	GS = Cast<APlaying_GS>(GetWorld()->GetGameState());
	if (GS == nullptr) return;

	PC = Cast<APlaying_PC>(GetOwningPlayer());
	if (PC == nullptr) return;

	for (auto ps : GS->PlayerArray)
	{
		auto playingPS = ps;
		if (playingPS != PC->PlayerState)
			OponnentPS = Cast<APlaying_PS>(playingPS);
	}

	if (!IsValid(OponnentPS)) return;

	if (!ensure(BoardUnitsTextBlock != nullptr)) return;
	BoardUnitsTextBlock->SetText(FText::AsNumber(OponnentPS->GetUnitsOnBoardCount()));

	if (!ensure(TurnsNumberTextBlock != nullptr)) return;
	TurnsNumberTextBlock->SetText(FText::AsNumber(OponnentPS->TurnNumber));
}

void UPlayingOponnentPanel_UW::UpdateGameTime(bool isTurnActive, int8 playerID)
{
	PlayerID = playerID;

	if (playerID == 1)
	{
		if (isTurnActive)
			SetTextGameTimePlayer2();
		else
			SetTextGameTimePlayer2();
	}
	else
	{
		if (isTurnActive)
			SetTextGameTimePlayer1();
		else
			SetTextGameTimePlayer1();
	}
}

void UPlayingOponnentPanel_UW::SetTextGameTimePlayer1()
{
	if (PlayerID == 1)
	{
		auto gs = Cast<APlaying_GS>(GetWorld()->GetGameState());
		if (!ensure(gs != nullptr)) return;

		FString minutes = FString::FromInt(gs->GameTimePlayer2.MinutesGameTime);
		FString seconds = FString::FromInt(gs->GameTimePlayer2.SecondsGameTime);

		if (gs->GameTimePlayer2.SecondsGameTime <= 9)
			seconds = "0" + FString::FromInt(gs->GameTimePlayer2.SecondsGameTime);

		FString time = minutes + ":" + seconds;

		PlayerGameTimeTextBlock->SetText(FText::FromString(time));
	}
}

void UPlayingOponnentPanel_UW::SetTextGameTimePlayer2()
{
	if (PlayerID == 2)
	{
		auto gs = Cast<APlaying_GS>(GetWorld()->GetGameState());
		if (!ensure(gs != nullptr)) return;

		FString minutes = FString::FromInt(gs->GameTimePlayer1.MinutesGameTime);
		FString seconds = FString::FromInt(gs->GameTimePlayer1.SecondsGameTime);

		if (gs->GameTimePlayer1.SecondsGameTime <= 9)
			seconds = "0" + FString::FromInt(gs->GameTimePlayer1.SecondsGameTime);

		FString time = minutes + ":" + seconds;

		PlayerGameTimeTextBlock->SetText(FText::FromString(time));
	}
}
