// Fill out your copyright notice in the Description page of Project Settings.

#include "PlayingUserPanel_UW.h"
#include "PawnsStrife.h"
#include "Playing_PC.h"
#include "Playing_PS.h"
#include "Playing_GS.h"

#include "Runtime/UMG/Public/Components/TextBlock.h"
#include "Runtime/Engine/Public/TimerManager.h"
#include "Runtime/Engine/Classes/Engine/World.h"
#include "Runtime/UMG/Public/Components/Image.h"

bool UPlayingUserPanel_UW::Initialize()
{
	bool success = Super::Initialize();
	if (!success) return false;

	FTimerHandle TimerHandle1;
	FTimerHandle TimerHandle2;

	GetWorld()->GetTimerManager().SetTimer(
		TimerHandle1, this, &UPlayingUserPanel_UW::SetTextGameTimePlayer1, 0.3f, true);

	GetWorld()->GetTimerManager().SetTimer(
		TimerHandle2, this, &UPlayingUserPanel_UW::SetTextGameTimePlayer2, 0.3f, true);

	return true;
}

void UPlayingUserPanel_UW::UpdatePlayerUserPanel()
{
	pc = Cast<APlaying_PC>(GetOwningPlayer());
	if (!ensure(pc != nullptr)) return;

	ps = Cast<APlaying_PS>(GetOwningPlayer()->PlayerState);
	if (!ensure(ps != nullptr)) return;

	if (!ensure(BoardUnitsTextBlock != nullptr)) return;
	BoardUnitsTextBlock->SetText(FText::AsNumber(ps->GetUnitsOnBoardCount()));

	if (!ensure(TurnsNumberTextBlock != nullptr)) return;
	TurnsNumberTextBlock->SetText(FText::AsNumber(ps->TurnNumber));
}

void UPlayingUserPanel_UW::UpdateTurnImage(bool IsTurnActive)
{
	if (!ensure(TurnImage != nullptr)) return;

	if (IsTurnActive)
		TurnImage->SetColorAndOpacity(FLinearColor(FColor::Blue));
	else
		TurnImage->SetColorAndOpacity(FLinearColor(FColor::Red));
}

void UPlayingUserPanel_UW::UpdateGameTime(bool isTurnActive, int8 playerID)
{
	PlayerID = playerID;

	if (playerID == 1)
	{
		if (isTurnActive)
			SetTextGameTimePlayer1();
		else
			SetTextGameTimePlayer1();
	}
	else
	{
		if (isTurnActive)
			SetTextGameTimePlayer2();
		else
			SetTextGameTimePlayer2();
	}
}

void UPlayingUserPanel_UW::SetTextGameTimePlayer1()
{
	if (PlayerID == 1)
	{
		auto gs = GetCustomGameState<APlaying_GS>(GetWorld());
		if (!ensure(gs != nullptr)) return;

		FString minutes = FString::FromInt(gs->GameTimePlayer1.MinutesGameTime);
		FString seconds = FString::FromInt(gs->GameTimePlayer1.SecondsGameTime);

		if (gs->GameTimePlayer1.SecondsGameTime <= 9)
			seconds = "0" + FString::FromInt(gs->GameTimePlayer1.SecondsGameTime);

		FString time = minutes + ":" + seconds;

		PlayerGameTimeTextBlock->SetText(FText::FromString(time));
	}
}

void UPlayingUserPanel_UW::SetTextGameTimePlayer2()
{
	if (PlayerID == 2)
	{
		auto gs = GetCustomGameState<APlaying_GS>(GetWorld());

		if(!IsValid(gs)) return;

		FString minutes = FString::FromInt(gs->GameTimePlayer2.MinutesGameTime);
		FString seconds = FString::FromInt(gs->GameTimePlayer2.SecondsGameTime);

		if (gs->GameTimePlayer2.SecondsGameTime <= 9)
			seconds = "0" + FString::FromInt(gs->GameTimePlayer2.SecondsGameTime);

		FString time = minutes + ":" + seconds;

		PlayerGameTimeTextBlock->SetText(FText::FromString(time));
	}

}

