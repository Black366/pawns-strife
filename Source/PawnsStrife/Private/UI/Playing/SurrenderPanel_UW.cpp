// Fill out your copyright notice in the Description page of Project Settings.

#include "SurrenderPanel_UW.h"
#include "PawnsStrife.h"
#include "Playing_GS.h"
#include "Playing_PS.h"
#include "PawnsStrife_GI.h"
#include "Playing_PC.h"

#include "Runtime/UMG/Public/Components/Button.h"
#include "Runtime/Engine/Classes/Kismet/GameplayStatics.h"


bool USurrenderPanel_UW::Initialize()
{
	bool success = Super::Initialize();
	if (!success) return false;

	if (!ensure(YesSurrenderButton != nullptr)) return false;
	YesSurrenderButton->OnClicked.AddDynamic(this, &USurrenderPanel_UW::OnSurrender);

	if (!ensure(NoSurrenderButton != nullptr)) return false;
	NoSurrenderButton->OnClicked.AddDynamic(this, &USurrenderPanel_UW::OnNoSurrender);

	return true;
}

void USurrenderPanel_UW::OnSurrender()
{
	auto pc = Cast<APlaying_PC>(GetOwningPlayer());
	if (!IsValid(pc)) return;

	auto ps = Cast<APlaying_PS>(GetOwningPlayer()->PlayerState);
	if (!IsValid(ps)) return;

	auto gs = Cast<APlaying_GS>(UGameplayStatics::GetGameState(GetWorld()));
	if (!IsValid(gs)) return;

	if (ps->PawnsStrifePlayerID == 1)
		pc->Server_RequestEndGame(false);
	else
		pc->Server_RequestEndGame(true);

	this->RemoveFromViewport();

}

void USurrenderPanel_UW::OnNoSurrender()
{
	auto gi = Cast<UPawnsStrife_GI>(GetWorld()->GetGameInstance());
	if (!ensure(gi != nullptr)) return;

	this->RemoveFromViewport();

	gi->SetInputMode(EInputMode::EUIAndGame, true);
}
