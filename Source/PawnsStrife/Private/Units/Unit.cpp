// Fill out your copyright notice in the Description page of Project Settings.

#include "Unit.h"
#include "PlayerInfo.h"
#include "UnitFunctionsLibrary.h"
#include "GridManager.h"
#include "PawnsStrife.h"
#include "Playing_GS.h"
#include "Playing_PC.h"

#include "UnrealNetwork.h"
#include "Runtime/Engine/Classes/Engine/SkeletalMesh.h"
#include "Runtime/Engine/Classes/Components/SkeletalMeshComponent.h"
#include "Runtime/Engine/Classes/Components/TextRenderComponent.h"
#include "Runtime/CoreUObject/Public/UObject/ConstructorHelpers.h"
#include "Runtime/Engine/Public/EngineUtils.h"
#include "Runtime/Engine/Classes/Particles/ParticleSystemComponent.h"
#include "Runtime/Engine/Classes/Kismet/GameplayStatics.h"
#include "Runtime/Engine/Classes/Components/SceneCaptureComponent2D.h"
#include "Runtime/Engine/Classes/Components/BoxComponent.h"
#include "Runtime/Engine/Classes/Engine/TextureRenderTarget2D.h"
#include "Runtime/Engine/Classes/Engine/SceneCapture2D.h"
#include "Runtime/Engine/Classes/Animation/AnimBlueprint.h"


AUnit::AUnit()
{
	static ConstructorHelpers::FObjectFinder<UTextureRenderTarget2D> TextureRenderTargetBP(
		TEXT("/Game/Materials/TextureRenderUnitTarget2D"));
	if (!ensure(TextureRenderTargetBP.Object != nullptr)) return;
	TextureRenderTarget = TextureRenderTargetBP.Object;

	bReplicates = true;
	bReplicateMovement = true;

	SceneComponent = CreateDefaultSubobject<USceneComponent>(TEXT("Scene Component"));
	RootComponent = SceneComponent;

	BoxComponent = CreateDefaultSubobject<UBoxComponent>(TEXT("BoxCollision"));
	BoxComponent->SetupAttachment(SceneComponent);
	BoxComponent->SetBoxExtent(FVector(80, 80, 20));

	NameTextComponent = CreateDefaultSubobject<UTextRenderComponent>("Text Name Component");
	NameTextComponent->SetupAttachment(SceneComponent);
	NameTextComponent->SetTextRenderColor(FColor::Black);
	NameTextComponent->SetHorizontalAlignment(EHTA_Center);
	NameTextComponent->SetXScale(1.2f);
	NameTextComponent->SetYScale(1.2f);
	NameTextComponent->SetRelativeRotation(FRotator(90, 0, 90));
	NameTextComponent->SetRelativeLocation(FVector(0, 65, 10));

	AttackPointsTextComponent = CreateDefaultSubobject<UTextRenderComponent>(TEXT("Attack Points Text Component"));
	AttackPointsTextComponent->SetupAttachment(SceneComponent);
	AttackPointsTextComponent->SetTextRenderColor(FColor::Black);
	AttackPointsTextComponent->SetHorizontalAlignment(EHTA_Center);
	AttackPointsTextComponent->SetXScale(1.6f);
	AttackPointsTextComponent->SetYScale(1.6f);
	AttackPointsTextComponent->SetRelativeRotation(FRotator(90, 0, 90));
	AttackPointsTextComponent->SetRelativeLocation(FVector(70, 20, 10));

	HealthPointsTextComponent = CreateDefaultSubobject<UTextRenderComponent>(TEXT("Health Points Text Component"));
	HealthPointsTextComponent->SetupAttachment(SceneComponent);
	HealthPointsTextComponent->SetTextRenderColor(FColor::Red);
	HealthPointsTextComponent->SetHorizontalAlignment(EHTA_Center);
	HealthPointsTextComponent->SetXScale(1.6f);
	HealthPointsTextComponent->SetYScale(1.6f);
	HealthPointsTextComponent->SetRelativeRotation(FRotator(90, 0, 90));
	HealthPointsTextComponent->SetRelativeLocation(FVector(-70, 20, 10));

	SkeletalMeshComponent = CreateDefaultSubobject<USkeletalMeshComponent>("Skeletal Mesh Component");
	SkeletalMeshComponent->SetupAttachment(SceneComponent);
	SkeletalMeshComponent->SetCollisionObjectType(ECollisionChannel::ECC_GameTraceChannel2);
	SkeletalMeshComponent->SetGenerateOverlapEvents(true);
	SkeletalMeshComponent->SetCollisionEnabled(ECollisionEnabled::QueryAndPhysics);

	OnDamagePSC = CreateDefaultSubobject<UParticleSystemComponent>("PSC");

	OnBeginCursorOver.AddDynamic(this, &AUnit::OnBeginCursor);
	OnEndCursorOver.AddDynamic(this, &AUnit::OnEndCursor);
}

void AUnit::OnConstruction(const FTransform& Transform)
{
	for (TActorIterator<AGridManager> GridManagerItr(GetWorld()); GridManagerItr; ++GridManagerItr)
	{
		GridManagerInstance = *GridManagerItr;
	}

	for (TActorIterator<ASceneCapture2D> SceneCapture2DItr(GetWorld()); SceneCapture2DItr; ++SceneCapture2DItr)
	{
		SceneCapture = *SceneCapture2DItr;
	}

	GameStateInstance = GetCustomGameState<APlaying_GS>(GetWorld());
}

void AUnit::Server_SetUnitData_Implementation(const FString& unitName)
{
	if (!ensure(Role == ROLE_Authority)) return;

	SetUnitData(unitName);

	Mulitcast_SetUnitVisual();
}

bool AUnit::Server_SetUnitData_Validate(const FString& unitName)
{
	return true;
}

void AUnit::Server_SetUnitLocation_Implementation(FVector Location)
{
	SetActorLocation(Location);

	CurrentIndex = GridManagerInstance->LocationToIndex(Location);

	GameStateInstance->UpdateBoardUnitsIndexes();

	UE_LOG(LogTemp, Warning, TEXT("%s index: %d"), *Name, CurrentIndex);
}

bool AUnit::Server_SetUnitLocation_Validate(FVector Location)
{
	return true;
}

void AUnit::Mulitcast_UpdateUnitVisual_Implementation(bool bRenderDepth)
{
	SkeletalMeshComponent->SetWorldScale3D(Scale);
	OnRep_Scale();
	SkeletalMeshComponent->SetRenderCustomDepth(bRenderDepth);
}

void AUnit::SpawnEffects(class UParticleSystem* PS, FTransform transform)
{
	OnDamagePSC = UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), PS, transform);
}

void AUnit::Mulitcast_SpawnEffects_Implementation(UParticleSystem* PS, FTransform transform)
{
	SpawnEffects(PS, transform);
}

void AUnit::TakesDamage(class AUnit* DamageCauser)
{
	if (!ensure(Role == ROLE_Authority)) return;

	if ((HealthPoints - DamageCauser->GetAttackPoints()) <= 0)
		HealthPoints = 0;
	else
		HealthPoints -= DamageCauser->GetAttackPoints();

	OnRep_HealthPoints();

	Mulitcast_SpawnEffects(DamageCauser->OnDamagePS, GetActorTransform());

	if (HealthPoints <= 0)
	{
		GameStateInstance->RemoveUnitFromBoard(this, OwningPlayerID);
		OnDamagePSC->OnSystemFinished.AddDynamic(this, &AUnit::Server_OnDestroy);
	}
	else
	{
		Multicast_UpdateVisualComponents();
	}
}

void AUnit::Server_OnDestroy_Implementation(UParticleSystemComponent* psc)
{
	Destroy(true);
	GridManagerInstance->DeleteMoveMarkerTiles();
	GridManagerInstance->DeleteAttackMarkerTiles();
}

bool AUnit::Server_OnDestroy_Validate(UParticleSystemComponent* psc)
{
	return true;
}

void AUnit::Server_OnSelected_Implementation(int8 selectingPlayerID)
{
	OnSelected(selectingPlayerID);
}

bool AUnit::Server_OnSelected_Validate(int8 selectingPlayerID)
{
	return true;
}

void AUnit::OnSelected(int8 selectingPlayerID)
{
	if (!ensure(Role == ROLE_Authority)) return;

	if (!IsSelected)
	{
		IsSelected = true;

		Scale += FVector(.1f, .1f, .1f);
		SkeletalMeshComponent->SetWorldScale3D(Scale);
		SkeletalMeshComponent->SetRenderCustomDepth(true);
		Mulitcast_UpdateUnitVisual(true);

		UE_LOG(LogTemp, Warning, TEXT("Unit %s selected!"), *Name);
	}
}

void AUnit::OnDeselected()
{
	if (IsSelected)
	{
		IsSelected = false;

		Scale -= FVector(.1f, .1f, .1f);
		SkeletalMeshComponent->SetWorldScale3D(Scale);
		SkeletalMeshComponent->SetRenderCustomDepth(false);
		Mulitcast_UpdateUnitVisual(false);

		UE_LOG(LogTemp, Warning, TEXT("Unit %s deselected!"), *Name);
	}
}

void AUnit::OnBeginCursor(AActor* TouchedActor)
{
	GridManagerInstance->SpawnMoveMarkerTiles(PossibleMovedIndexes());
	GridManagerInstance->SpawnAttackMarkerTiles(PossibleAttackIndexes());

	if (!ensure(SceneCapture != nullptr)) return;
	if (OwningPlayerID == 1)
	{
		FVector location = FVector(GetActorLocation() + SceneCaptureLocation);
		SceneCapture->SetActorLocationAndRotation(location, GetActorRotation() + FRotator(0, -90, 0));
	}
	else
	{
		FVector location = FVector(
			GetActorLocation().X - SceneCaptureLocation.X,
			GetActorLocation().Y - SceneCaptureLocation.Y,
			GetActorLocation().Z + SceneCaptureLocation.Z);

		SceneCapture->SetActorLocationAndRotation(location, GetActorRotation() + FRotator(0, -90, 0));
	}


	SceneCapture->GetCaptureComponent2D()->TextureTarget = TextureRenderTarget;
}

void AUnit::OnEndCursor(class AActor* TouchedActor)
{
	GridManagerInstance->DeleteMoveMarkerTiles();
	GridManagerInstance->DeleteAttackMarkerTiles();

	SceneCapture->GetCaptureComponent2D()->TextureTarget = nullptr;
}

TArray<int> AUnit::PossibleMovedIndexes()
{
	switch (UnitInfo.MoveType)
	{
	case EMoveType::ESimple:
		return GetSimpleMoveIndexes();
		break;
	case EMoveType::ESimple2:
		return GetSimple2MoveIndexes();
		break;
	case EMoveType::ESimple3:
		return GetSimple3MoveIndexes();
		break;
	case EMoveType::ESimple4:
		return GetSimple4MoveIndexes();
		break;
	case EMoveType::ESimple5:
		return GetSimple5MoveIndexes();
		break;
	case EMoveType::ESTraightInfinitely:
		return GetStraightInfinitelyMoveIndexes();
		break;
	case EMoveType::EAround:
		return GetAroundMoveIndexes();
		break;
	case EMoveType::EAround2:
		return GetAround2MoveIndexes();
		break;
	case EMoveType::ESlantInfinitely:
		return GetSlantInfinitelyMoveIndexes();
		break;
	case EMoveType::EUpInfSimple:
		return GetUpInfSimpleMoveIndexes();
		break;
	case EMoveType::ERightLeftInfUp:
		return GetRightLeftInfUpMoveIndexes();
		break;
	case EMoveType::ELeftRightSlantDown:
		return GetLeftRightSlantDownMoveIndexes();
		break;
	case EMoveType::EHard:
		return GetHardMoveIndexes();
		break;
	default:
		return GetSimpleMoveIndexes();
		break;

	}
}

TArray<int> AUnit::PossibleAttackIndexes()
{
	switch (UnitInfo.AttackType)
	{
	case EAttackType::ESimple:
		return GetSimpleAttackIndexes();
		break;
	case EAttackType::ESimple2:
		return GetSimple2AttackIndexes();
		break;
	case EAttackType::ESimple3:
		return GetSimple3AttackIndexes();
		break;
	case EAttackType::ESimple4:
		return GetSimple4AttackIndexes();
		break;
	case EAttackType::ESimple5:
		return GetSimple5AttackIndexes();
		break;
	default:
		return GetSimpleAttackIndexes();
		break;
	}
}

void AUnit::SetUnitData(const FString& dataTableName)
{
	UnitInfo = UUnitFunctionsLibrary::GetUnitData(dataTableName);

	DataTableName = FText::FromString(dataTableName);

	Name = UnitInfo.Name.ToString();
	AttackPoints = UnitInfo.Damage;
	HealthPoints = UnitInfo.Health;

	MoveType = UnitInfo.MoveType;
	AttackType = UnitInfo.AttackType;

	CurrentIndex = GridManagerInstance->LocationToIndex(GetActorLocation());

	UE_LOG(LogTemp, Warning, TEXT("AUnit SetUnitData: DataTableName: %s, UnitName: %s, AP: %d, HP: %d, Index: %d"),
		*DataTableName.ToString(), *Name, AttackPoints, HealthPoints, CurrentIndex);
}

void AUnit::Mulitcast_SetUnitVisual_Implementation()
{
	SetUnitVisual();
}

bool AUnit::Mulitcast_SetUnitVisual_Validate()
{
	return true;
}

void AUnit::SetUnitVisual()
{
	SkeletalMesh = UnitInfo.VisualInfo.SkeletalMesh;
	Animation = UnitInfo.VisualInfo.Animation;
	Scale = UnitInfo.VisualInfo.Scale;
	OnDamagePS = UnitInfo.VisualInfo.OnDamagePS;
	SceneCaptureLocation = UnitInfo.VisualInfo.SceneCaptureLocation;

	SkeletalMeshComponent->SetSkeletalMesh(SkeletalMesh);
	OnRep_SkeletalMesh();
	SkeletalMeshComponent->SetWorldScale3D(Scale);
	OnRep_Scale();
	SkeletalMeshComponent->SetAnimInstanceClass(Animation->GeneratedClass);
	OnRep_Animation();

	NameTextComponent->SetText(FText::FromString(Name));
	OnRep_Name();
	AttackPointsTextComponent->SetText(FText::AsNumber(AttackPoints));
	OnRep_AttackPoints();
	HealthPointsTextComponent->SetText(FText::AsNumber(HealthPoints));
	OnRep_HealthPoints();
}

void AUnit::Multicast_UpdateVisualComponents_Implementation()
{
	AttackPointsTextComponent->SetText(FText::AsNumber(AttackPoints));
	HealthPointsTextComponent->SetText(FText::AsNumber(HealthPoints));
}

bool AUnit::Multicast_UpdateVisualComponents_Validate()
{
	return true;
}

TArray<int> AUnit::GetSimpleMoveIndexes()
{
	TArray<int> tempIndexes;

	//UpLeft
	UpLeft1(tempIndexes);
	//UpRight
	UpRight1(tempIndexes);
	//Down
	Down1(tempIndexes);

	return tempIndexes;
}

TArray<int> AUnit::GetSimple2MoveIndexes()
{
	TArray<int> tempIndexes;

	//up
	Up1(tempIndexes);
	//down
	Down1(tempIndexes);

	return tempIndexes;
}

TArray<int> AUnit::GetSimple3MoveIndexes()
{
	TArray<int> tempIndexes;

	//Up
	Up1(tempIndexes);
	//Left
	Left1(tempIndexes);
	//Right
	Right1(tempIndexes);


	return tempIndexes;
}

TArray<int> AUnit::GetSimple4MoveIndexes()
{
	TArray<int> tempIndexes;

	//Up
	Up1(tempIndexes);
	//Down
	Down1(tempIndexes);
	//Left
	Left1(tempIndexes);
	//Right
	Right1(tempIndexes);


	return tempIndexes;
}

TArray<int> AUnit::GetSimple5MoveIndexes()
{
	TArray<int> tempIndexes;

	//Up
	Up1(tempIndexes);
	//Down
	Down1(tempIndexes);
	//UpLeft
	UpLeft1(tempIndexes);
	//UpRight
	UpRight1(tempIndexes);
	//DownLeft
	DownLeft1(tempIndexes);
	//DownRight
	DownRight1(tempIndexes);

	return tempIndexes;
}

TArray<int> AUnit::GetStraightInfinitelyMoveIndexes()
{
	TArray<int> tempIndexes;

	UpInfinity(tempIndexes);
	DownInfinity(tempIndexes);
	LeftInfinity(tempIndexes);
	RightInfinity(tempIndexes);

	return tempIndexes;
}

TArray<int> AUnit::GetAroundMoveIndexes()
{
	TArray<int> tempIndexes;

	//UpLeft
	UpLeft1(tempIndexes);
	//UpRight
	UpRight1(tempIndexes);
	//DownLeft
	DownLeft1(tempIndexes);
	//DownRight
	DownRight1(tempIndexes);
	//Up
	Up1(tempIndexes);
	//Down
	Down1(tempIndexes);
	//Left
	Left1(tempIndexes);
	//Right
	Right1(tempIndexes);

	return tempIndexes;
}

TArray<int> AUnit::GetAround2MoveIndexes()
{
	TArray<int> tempIndexes;

	if (OwningPlayerID == 1)
	{
		//1. UpLeft
		if (CurrentIndex < 36 && CurrentIndex % 6 != 5)
		{
			if (!GridManagerInstance->IsUnitOnTile((CurrentIndex + 1) + 2 * 6))
				tempIndexes.Add((CurrentIndex + 1) + 2 * 6);
		}
		//2. UpRight
		if (CurrentIndex < 36 && CurrentIndex % 6 != 0)
		{
			if (!GridManagerInstance->IsUnitOnTile((CurrentIndex - 1) + 2 * 6))
				tempIndexes.Add((CurrentIndex - 1) + 2 * 6);
		}
		//3. LeftUp
		if (CurrentIndex < 42 && CurrentIndex % 6 != 5 && CurrentIndex % 6 != 4)
		{
			if (!GridManagerInstance->IsUnitOnTile(CurrentIndex + 2 + 6))
				tempIndexes.Add(CurrentIndex + 2 + 6);
		}
		//4. RightUp
		if (CurrentIndex < 42 && CurrentIndex % 6 != 0 && CurrentIndex % 6 != 1)
		{
			if (!GridManagerInstance->IsUnitOnTile(CurrentIndex - 2 + 6))
				tempIndexes.Add(CurrentIndex - 2 + 6);
		}
		//5. DownLeft
		if (CurrentIndex > 11 && CurrentIndex % 6 != 5)
		{
			if (!GridManagerInstance->IsUnitOnTile(CurrentIndex - 12 + 1))
				tempIndexes.Add(CurrentIndex - 12 + 1);
		}
		//6. DownRight
		if (CurrentIndex > 11 && CurrentIndex % 6 != 0)
		{
			if (!GridManagerInstance->IsUnitOnTile(CurrentIndex - 12 - 1))
				tempIndexes.Add(CurrentIndex - 12 - 1);
		}
		//7. LeftDown
		if (CurrentIndex > 5 && CurrentIndex % 6 != 5 && CurrentIndex % 6 != 4)
		{
			if (!GridManagerInstance->IsUnitOnTile(CurrentIndex - 6 + 2))
				tempIndexes.Add(CurrentIndex - 6 + 2);
		}
		//8. RightDown
		if (CurrentIndex > 5 && CurrentIndex % 6 != 0 && CurrentIndex % 6 != 1)
		{
			if (!GridManagerInstance->IsUnitOnTile(CurrentIndex - 6 - 2))
				tempIndexes.Add(CurrentIndex - 6 - 2);
		}
	}
	else
	{
		//1. UpLeft
		if (CurrentIndex > 11 && CurrentIndex % 6 != 0)
		{
			if (!GridManagerInstance->IsUnitOnTile(CurrentIndex - 12 - 1))
				tempIndexes.Add(CurrentIndex - 12 - 1);
		}
		//2. UpRight
		if (CurrentIndex > 11 && CurrentIndex % 6 != 5)
		{
			if (!GridManagerInstance->IsUnitOnTile(CurrentIndex - 12 + 1))
				tempIndexes.Add(CurrentIndex - 12 + 1);
		}
		//3. LeftUp
		if (CurrentIndex > 5 && CurrentIndex % 6 != 0 && CurrentIndex % 6 != 1)
		{
			if (!GridManagerInstance->IsUnitOnTile(CurrentIndex - 6 - 2))
				tempIndexes.Add(CurrentIndex - 6 - 2);
		}
		//4. RightUp
		if (CurrentIndex > 5 && CurrentIndex % 6 != 5 && CurrentIndex % 6 != 4)
		{
			if (!GridManagerInstance->IsUnitOnTile(CurrentIndex - 6 + 2))
				tempIndexes.Add(CurrentIndex - 6 + 2);
		}
		//5. DownLeft
		if (CurrentIndex < 36 && CurrentIndex % 6 != 0)
		{
			if (!GridManagerInstance->IsUnitOnTile((CurrentIndex - 1) + 2 * 6))
				tempIndexes.Add((CurrentIndex - 1) + 2 * 6);
		}
		//6. DownRight
		if (CurrentIndex < 36 && CurrentIndex % 6 != 5)
		{
			if (!GridManagerInstance->IsUnitOnTile((CurrentIndex + 1) + 2 * 6))
				tempIndexes.Add((CurrentIndex + 1) + 2 * 6);
		}
		//7. LeftDown
		if (CurrentIndex < 42 && CurrentIndex % 6 != 0 && CurrentIndex % 6 != 1)
		{
			if (!GridManagerInstance->IsUnitOnTile(CurrentIndex - 2 + 6))
				tempIndexes.Add(CurrentIndex - 2 + 6);
		}
		//8. RightDown
		if (CurrentIndex < 42 && CurrentIndex % 6 != 5 && CurrentIndex % 6 != 4)
		{
			if (!GridManagerInstance->IsUnitOnTile(CurrentIndex + 2 + 6))
				tempIndexes.Add(CurrentIndex + 2 + 6);
		}
	}

	return tempIndexes;
}

TArray<int> AUnit::GetSlantInfinitelyMoveIndexes()
{
	TArray<int> tempIndexes;

	UpLeftSlantInfinity(tempIndexes);
	UpRightSlantInfinity(tempIndexes);
	DownLeftSlantInfinity(tempIndexes);
	DownRightSlantInfinity(tempIndexes);

	return tempIndexes;
}

TArray<int> AUnit::GetUpInfSimpleMoveIndexes()
{
	TArray<int> tempIndexes;

	UpInfinity(tempIndexes);
	Left1(tempIndexes);
	Right1(tempIndexes);
	Down1(tempIndexes);

	return tempIndexes;
}

TArray<int> AUnit::GetRightLeftInfUpMoveIndexes()
{
	TArray<int> tempIndexes;

	LeftInfinity(tempIndexes);
	RightInfinity(tempIndexes);
	Up1(tempIndexes);

	return tempIndexes;
}

TArray<int> AUnit::GetLeftRightSlantDownMoveIndexes()
{
	TArray<int> tempIndexes;

	UpLeftSlantInfinity(tempIndexes);
	UpRightSlantInfinity(tempIndexes);
	Down1(tempIndexes);

	return tempIndexes;
}

TArray<int> AUnit::GetHardMoveIndexes()
{
	TArray<int> tempIndexes;

	UpInfinity(tempIndexes);
	DownLeftSlantInfinity(tempIndexes);
	DownRightSlantInfinity(tempIndexes);
	Left1(tempIndexes);
	Right1(tempIndexes);

	return tempIndexes;
}


void AUnit::Up1(TArray<int>& tempIndexes)
{
	if (OwningPlayerID == 1)
	{
		if (CurrentIndex < 42)
		{
			if (!GridManagerInstance->IsUnitOnTile(CurrentIndex + GridManagerInstance->GridSizeX))
				tempIndexes.Add(CurrentIndex + GridManagerInstance->GridSizeX);
		}
	}
	else
	{
		if (CurrentIndex > 5)
		{
			if (!GridManagerInstance->IsUnitOnTile(CurrentIndex - GridManagerInstance->GridSizeX))
				tempIndexes.Add(CurrentIndex - GridManagerInstance->GridSizeX);
		}
	}
}

void AUnit::UpInfinity(TArray<int>& indexes)
{
	int i = 0;

	if (OwningPlayerID == 1)
	{
		i = CurrentIndex;
		while (i < 42)
		{
			i = i + 6;

			if (!GridManagerInstance->IsUnitOnTile(i))
				indexes.Add(i);
			else
				break;
		}
	}
	else
	{
		i = CurrentIndex;
		while (i > 5)
		{
			i -= 6;

			if (!GridManagerInstance->IsUnitOnTile(i))
				indexes.Add(i);
			else
				break;
		}
	}
}

void AUnit::Down1(TArray<int>& tempIndexes)
{
	if (OwningPlayerID == 1)
	{
		if (CurrentIndex > 5)
		{
			if (!GridManagerInstance->IsUnitOnTile(CurrentIndex - GridManagerInstance->GridSizeX))
				tempIndexes.Add(CurrentIndex - GridManagerInstance->GridSizeX);
		}
	}
	else
	{
		if (CurrentIndex < 42)
		{
			if (!GridManagerInstance->IsUnitOnTile(CurrentIndex + GridManagerInstance->GridSizeX))
				tempIndexes.Add(CurrentIndex + GridManagerInstance->GridSizeX);
		}
	}
}

void AUnit::DownInfinity(TArray<int>& indexes)
{
	int i = 0;

	if (OwningPlayerID == 1)
	{
		i = CurrentIndex;
		while (i > 5)
		{
			i = i - 6;

			if (!GridManagerInstance->IsUnitOnTile(i))
				indexes.Add(i);
			else
				break;
		}
	}
	else
	{
		i = CurrentIndex;
		while (i < 42)
		{
			i += 6;

			if (!GridManagerInstance->IsUnitOnTile(i))
				indexes.Add(i);
			else
				break;
		}
	}
}

void AUnit::Left1(TArray<int>& tempIndexes)
{
	if (OwningPlayerID == 1)
	{
		if (CurrentIndex % 6 != 5 && CurrentIndex < 42)
		{
			if (!GridManagerInstance->IsUnitOnTile(CurrentIndex + 1))
				tempIndexes.Add(CurrentIndex + 1);
		}
	}
	else
	{
		if (CurrentIndex % 6 != 0 && CurrentIndex < 42)
		{
			if (!GridManagerInstance->IsUnitOnTile(CurrentIndex - 1))
				tempIndexes.Add(CurrentIndex - 1);
		}
	}
}

void AUnit::LeftInfinity(TArray<int>& indexes)
{
	int i = 0;

	if (OwningPlayerID == 1)
	{
		i = CurrentIndex;
		while (i % 6 != 5)
		{
			i++;

			if (!GridManagerInstance->IsUnitOnTile(i))
				indexes.Add(i);
			else
				break;
		}
	}
	else
	{
		i = CurrentIndex;
		while (i % 6 != 0)
		{
			i--;

			if (!GridManagerInstance->IsUnitOnTile(i))
				indexes.Add(i);
			else
				break;
		}
	}
}

void AUnit::Right1(TArray<int>& tempIndexes)
{
	if (OwningPlayerID == 1)
	{
		if (CurrentIndex % 6 != 0 && CurrentIndex < 42)
		{
			if (!GridManagerInstance->IsUnitOnTile(CurrentIndex - 1))
				tempIndexes.Add(CurrentIndex - 1);
		}
	}
	else
	{
		if (CurrentIndex % 6 != 5 && CurrentIndex < 42)
		{
			if (!GridManagerInstance->IsUnitOnTile(CurrentIndex + 1))
				tempIndexes.Add(CurrentIndex + 1);
		}
	}
}

void AUnit::RightInfinity(TArray<int>& indexes)
{
	int i = 0;

	if (OwningPlayerID == 1)
	{
		i = CurrentIndex;
		while (i % 6 != 0)
		{
			i--;

			if (!GridManagerInstance->IsUnitOnTile(i))
				indexes.Add(i);
			else
				break;
		}
	}
	else
	{
		i = CurrentIndex;
		while (i % 6 != 5)
		{
			i++;

			if (!GridManagerInstance->IsUnitOnTile(i))
				indexes.Add(i);
			else
				break;
		}
	}
}

void AUnit::UpLeft1(TArray<int>& tempIndexes)
{
	if (OwningPlayerID == 1)
	{
		if (CurrentIndex < 42 && CurrentIndex % 6 != 5)
		{
			if (!GridManagerInstance->IsUnitOnTile(CurrentIndex + 6 + 1))
				tempIndexes.Add(CurrentIndex + 6 + 1);
		}
	}
	else
	{
		if (CurrentIndex > 5 && CurrentIndex % 6 != 0)
		{
			if (!GridManagerInstance->IsUnitOnTile(CurrentIndex - 6 - 1))
				tempIndexes.Add(CurrentIndex - 6 - 1);
		}
	}
}

void AUnit::UpLeftSlantInfinity(TArray<int>& tempIndexes)
{
	int i = 0;

	if (OwningPlayerID == 1)
	{
		i = CurrentIndex;
		while (i % 6 != 5 && i < 42)
		{
			i = i + 6 + 1;

			if (!GridManagerInstance->IsUnitOnTile(i))
				tempIndexes.Add(i);
			else
				break;
		}
	}
	else
	{
		i = CurrentIndex;
		while (i % 6 != 0 && i > 5)
		{
			i = i - 6 - 1;

			if (!GridManagerInstance->IsUnitOnTile(i))
				tempIndexes.Add(i);
			else
				break;
		}
	}
}

void AUnit::UpRight1(TArray<int>& tempIndexes)
{
	if (OwningPlayerID == 1)
	{
		if (CurrentIndex < 42 && CurrentIndex % 6 != 0)
		{
			if (!GridManagerInstance->IsUnitOnTile(CurrentIndex + 6 - 1))
				tempIndexes.Add(CurrentIndex + 6 - 1);
		}
	}
	else
	{
		if (CurrentIndex > 5 && CurrentIndex % 6 != 5)
		{
			if (!GridManagerInstance->IsUnitOnTile(CurrentIndex - 6 + 1))
				tempIndexes.Add(CurrentIndex - 6 + 1);
		}
	}
}

void AUnit::UpRightSlantInfinity(TArray<int>& tempIndexes)
{
	int i = 0;

	if (OwningPlayerID == 1)
	{
		i = CurrentIndex;
		while (i % 6 != 0 && i < 42)
		{
			i = i + 6 - 1;

			if (!GridManagerInstance->IsUnitOnTile(i))
				tempIndexes.Add(i);
			else
				break;
		}
	}
	else
	{
		i = CurrentIndex;
		while (i % 6 != 5 && i > 5)
		{
			i = i - 6 + 1;

			if (!GridManagerInstance->IsUnitOnTile(i))
				tempIndexes.Add(i);
			else
				break;
		}
	}
}

void AUnit::DownLeft1(TArray<int>& tempIndexes)
{
	if (OwningPlayerID == 1)
	{
		if (CurrentIndex > 5 && CurrentIndex % 6 != 5)
		{
			if (!GridManagerInstance->IsUnitOnTile(CurrentIndex - 6 + 1))
				tempIndexes.Add(CurrentIndex - 6 + 1);
		}
	}
	else
	{
		if (CurrentIndex < 42 && CurrentIndex % 6 != 0)
		{
			if (!GridManagerInstance->IsUnitOnTile(CurrentIndex + 6 - 1))
				tempIndexes.Add(CurrentIndex + 6 - 1);
		}
	}
}

void AUnit::DownLeftSlantInfinity(TArray<int>& tempIndexes)
{
	int i = 0;

	if (OwningPlayerID == 1)
	{
		i = CurrentIndex;
		while (i % 6 != 5 && i > 5)
		{
			i = i - 6 + 1;

			if (!GridManagerInstance->IsUnitOnTile(i))
				tempIndexes.Add(i);
			else
				break;
		}
	}
	else
	{
		i = CurrentIndex;
		while (i % 6 != 0 && i < 42)
		{
			i = i + 6 - 1;

			if (!GridManagerInstance->IsUnitOnTile(i))
				tempIndexes.Add(i);
			else
				break;
		}
	}
}

void AUnit::DownRight1(TArray<int>& tempIndexes)
{
	if (OwningPlayerID == 1)
	{
		if (CurrentIndex > 5 && CurrentIndex % 6 != 0)
		{
			if (!GridManagerInstance->IsUnitOnTile(CurrentIndex - 6 - 1))
				tempIndexes.Add(CurrentIndex - 6 - 1);
		}
	}
	else
	{
		if (CurrentIndex < 42 && CurrentIndex % 6 != 5)
		{
			if (!GridManagerInstance->IsUnitOnTile(CurrentIndex + 6 + 1))
				tempIndexes.Add(CurrentIndex + 6 + 1);
		}
	}
}

void AUnit::DownRightSlantInfinity(TArray<int>& tempIndexes)
{
	int i = 0;

	if (OwningPlayerID == 1)
	{
		i = CurrentIndex;
		while (i % 6 != 0 && i > 5)
		{
			i = i - 6 - 1;

			if (!GridManagerInstance->IsUnitOnTile(i))
				tempIndexes.Add(i);
			else
				break;
		}
	}
	else
	{
		i = CurrentIndex;
		while (i % 6 != 5 && i < 42)
		{
			i = i + 6 + 1;

			if (!GridManagerInstance->IsUnitOnTile(i))
				tempIndexes.Add(i);
			else
				break;
		}
	}
}



TArray<int> AUnit::GetSimpleAttackIndexes()
{
	TArray<int> tempIndexes;

	//UpLeft
	UpLeft1Attack(tempIndexes);
	//UpRight
	UpRight1Attack(tempIndexes);

	return tempIndexes;
}

TArray<int> AUnit::GetSimple2AttackIndexes()
{
	TArray<int> tempIndexes;

	//up
	Up1Attack(tempIndexes);
	//down
	Down1Attack(tempIndexes);

	return tempIndexes;
}

TArray<int> AUnit::GetSimple3AttackIndexes()
{
	TArray<int> tempIndexes;

	//Up
	Up1Attack(tempIndexes);
	//Down
	Down1Attack(tempIndexes);
	//Left
	Left1Attack(tempIndexes);
	//Right
	Right1Attack(tempIndexes);


	return tempIndexes;
}

TArray<int> AUnit::GetSimple4AttackIndexes()
{
	TArray<int> tempIndexes;

	//UpLeft
	UpLeft1Attack(tempIndexes);
	//UpRight
	UpRight1Attack(tempIndexes);
	//DownLeft
	DownLeft1Attack(tempIndexes);
	//DownRight
	DownRight1Attack(tempIndexes);

	return tempIndexes;
}

TArray<int> AUnit::GetSimple5AttackIndexes()
{
	TArray<int> tempIndexes;

	//UpLeft
	UpLeft1Attack(tempIndexes);
	//UpRight
	UpRight1Attack(tempIndexes);
	//up
	Up1Attack(tempIndexes);
	//Left
	Left1Attack(tempIndexes);
	//Right
	Right1Attack(tempIndexes);

	return tempIndexes;
}


void AUnit::Up1Attack(TArray<int>& tempIndexes)
{
	if (OwningPlayerID == 1)
	{
		if (CurrentIndex < 42)
			tempIndexes.Add(CurrentIndex + GridManagerInstance->GridSizeX);
	}
	else
	{
		if (CurrentIndex > 5)
			tempIndexes.Add(CurrentIndex - GridManagerInstance->GridSizeX);
	}
}

void AUnit::Down1Attack(TArray<int>& tempIndexes)
{
	if (OwningPlayerID == 1)
	{
		if (CurrentIndex > 5)
			tempIndexes.Add(CurrentIndex - GridManagerInstance->GridSizeX);
	}
	else
	{
		if (CurrentIndex < 42)
			tempIndexes.Add(CurrentIndex + GridManagerInstance->GridSizeX);
	}
}

void AUnit::Left1Attack(TArray<int>& tempIndexes)
{
	if (OwningPlayerID == 1)
	{
		if (CurrentIndex % 6 != 5 && CurrentIndex < 42)
			tempIndexes.Add(CurrentIndex + 1);
	}
	else
	{
		if (CurrentIndex % 6 != 0 && CurrentIndex < 42)
			tempIndexes.Add(CurrentIndex - 1);
	}
}

void AUnit::Right1Attack(TArray<int>& tempIndexes)
{
	if (OwningPlayerID == 1)
	{
		if (CurrentIndex % 6 != 0 && CurrentIndex < 42)
			tempIndexes.Add(CurrentIndex - 1);
	}
	else
	{
		if (CurrentIndex % 6 != 5 && CurrentIndex < 42)
			tempIndexes.Add(CurrentIndex + 1);
	}
}

void AUnit::UpLeft1Attack(TArray<int>& tempIndexes)
{
	if (OwningPlayerID == 1)
	{
		if (CurrentIndex < 42 && CurrentIndex % 6 != 5)
			tempIndexes.Add(CurrentIndex + 6 + 1);
	}
	else
	{
		if (CurrentIndex > 5 && CurrentIndex % 6 != 0)
			tempIndexes.Add(CurrentIndex - 6 - 1);
	}
}

void AUnit::UpRight1Attack(TArray<int>& tempIndexes)
{
	if (OwningPlayerID == 1)
	{
		if (CurrentIndex < 42 && CurrentIndex % 6 != 0)
			tempIndexes.Add(CurrentIndex + 6 - 1);
	}
	else
	{
		if (CurrentIndex > 5 && CurrentIndex % 6 != 5)
			tempIndexes.Add(CurrentIndex - 6 + 1);
	}
}

void AUnit::DownLeft1Attack(TArray<int>& tempIndexes)
{
	if (OwningPlayerID == 1)
	{
		if (CurrentIndex > 5 && CurrentIndex % 6 != 5)
			tempIndexes.Add(CurrentIndex - 6 + 1);
	}
	else
	{
		if (CurrentIndex < 42 && CurrentIndex % 6 != 0)
			tempIndexes.Add(CurrentIndex + 6 - 1);
	}
}

void AUnit::DownRight1Attack(TArray<int>& tempIndexes)
{
	if (OwningPlayerID == 1)
	{
		if (CurrentIndex > 5 && CurrentIndex % 6 != 0)
			tempIndexes.Add(CurrentIndex - 6 - 1);
	}
	else
	{
		if (CurrentIndex < 42 && CurrentIndex % 6 != 5)
			tempIndexes.Add(CurrentIndex + 6 + 1);
	}
}



void AUnit::GetLifetimeReplicatedProps(TArray<FLifetimeProperty> & OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(AUnit, Name);
	DOREPLIFETIME(AUnit, AttackPoints);
	DOREPLIFETIME(AUnit, HealthPoints);
	DOREPLIFETIME(AUnit, OwningPlayerID);
	DOREPLIFETIME(AUnit, UnitInfo);
	DOREPLIFETIME(AUnit, DataTableName);
	DOREPLIFETIME(AUnit, MoveType);
	DOREPLIFETIME(AUnit, CurrentIndex);

	DOREPLIFETIME(AUnit, TurnsAlive);

	DOREPLIFETIME(AUnit, SkeletalMesh);
	DOREPLIFETIME(AUnit, Animation);

	DOREPLIFETIME(AUnit, OnDamagePS);

	DOREPLIFETIME(AUnit, Scale);
	DOREPLIFETIME(AUnit, SceneCaptureLocation);

	DOREPLIFETIME(AUnit, SkeletalMeshComponent);

	DOREPLIFETIME(AUnit, GridManagerInstance);
	DOREPLIFETIME(AUnit, GameStateInstance);
}


/////////////////////////////////////////////////////

void AUnit::OnRep_Name()
{
	NameTextComponent->SetText(FText::FromString(Name));
}

void AUnit::OnRep_AttackPoints()
{
	Multicast_UpdateVisualComponents();
}

void AUnit::OnRep_HealthPoints()
{
	Multicast_UpdateVisualComponents();
}

void AUnit::OnRep_SkeletalMesh()
{
	SkeletalMeshComponent->SetSkeletalMesh(SkeletalMesh);
}

void AUnit::OnRep_Animation()
{
	SkeletalMeshComponent->SetAnimInstanceClass(Animation->GeneratedClass);
}

void AUnit::OnRep_Scale()
{
	SkeletalMeshComponent->SetWorldScale3D(Scale);
}

void AUnit::OnRep_SceneCaptureLocation()
{
}

void AUnit::OnRep_TurnsAlive()
{
}

const int32 AUnit::GetAttackPoints()
{
	return AttackPoints;
}
