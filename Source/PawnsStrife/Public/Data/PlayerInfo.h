// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/DataTable.h"
#include "PlayerInfo.generated.h"


UENUM(BlueprintType)
enum class EMoveType : uint8
{
	ESimple						UMETA(DisplayName = "Simple Move"),
	ESimple2						UMETA(DisplayName = "Simple Move 2"),
	ESimple3						UMETA(DisplayName = "Simple Move 3"),
	ESimple4						UMETA(DisplayName = "Simple Move 4"),
	ESimple5						UMETA(DisplayName = "Simple Move 5"),
	EAround						UMETA(DisplayName = "Simple Around Move"),
	EAround2						UMETA(DisplayName = "Simple Around2 Move"),
	ESTraightInfinitely		UMETA(DisplayName = "Straight Infinitely Move"),
	ESlantInfinitely			UMETA(DisplayName = "Slant Infinitely Move"),
	EUpInfSimple				UMETA(DisplayName = "Up Infinity and Simple"),
	ERightLeftInfUp			UMETA(DisplayName = "Left and Right Infinity and Up"),
	ELeftRightSlantDown		UMETA(DisplayName = "UpLeft and UpRight Infinity Slant and Down"),
	EHard							UMETA(DisplayName = "UpInf DownLeftInfSlant DownRightInfSlant Left1 Right1"),
};

UENUM(BlueprintType)
enum class EAttackType : uint8
{
	ESimple						UMETA(DisplayName = "Simple Attack"),
	ESimple2						UMETA(DisplayName = "Simple Attack 2"),
	ESimple3						UMETA(DisplayName = "Simple Attack 3"),
	ESimple4						UMETA(DisplayName = "Simple Attack 4"),
	ESimple5						UMETA(DisplayName = "Simple Attack 5"),
};

/**
 *
 */
USTRUCT()
struct FPlayerInfo
{
	GENERATED_BODY()

	UPROPERTY()
		FString PlayerName;
	UPROPERTY()
		FString AuthToken;
	UPROPERTY()
		FString UserId;
	UPROPERTY()
		bool IsNewPlayer;
	UPROPERTY(meta = (ClampMin = 0))
		int Level;
	UPROPERTY(meta = (ClampMin = 0))
		int Gold;

	FPlayerInfo() {}
};


/**
 *
 */
USTRUCT(BlueprintType)
struct FTableUnitInfo
{
	GENERATED_BODY()

		UPROPERTY(EditAnywhere)
		FString UnitName;
	UPROPERTY(EditAnywhere)
		int Quantity;
};

/**
 *
 */
USTRUCT(BlueprintType)
struct FTableSetInfo : public FTableRowBase
{
	GENERATED_BODY()

		UPROPERTY(VisibleAnywhere)
		FString SetName;
	UPROPERTY(VisibleAnywhere)
		TArray<FTableUnitInfo> UnitsInSetInfo;
};

/**
 *
 */
USTRUCT(BlueprintType)
struct FVisualUnitInfo
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere)
		class USkeletalMesh* SkeletalMesh;
	UPROPERTY(EditAnywhere)
		class UAnimBlueprint* Animation;
	UPROPERTY(EditAnywhere)
		class UParticleSystem* OnDamagePS;

	UPROPERTY(EditAnywhere)
		FVector Scale;
	UPROPERTY(EditAnywhere)
		FVector SceneCaptureLocation;
};

/**
 *
 */
USTRUCT(BlueprintType)
struct FUnitInfo : public FTableRowBase
{
	GENERATED_BODY()

		UPROPERTY(VisibleAnywhere)
		FText Name;
	UPROPERTY(VisibleAnywhere)
		int32 Health;
	UPROPERTY(VisibleAnywhere)
		int32 Damage;

	UPROPERTY(VisibleAnywhere)
		EMoveType MoveType;
	UPROPERTY(VisibleAnywhere)
		EAttackType AttackType;

	UPROPERTY(EditAnywhere)
		FVisualUnitInfo VisualInfo;
};



