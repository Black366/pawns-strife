// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Requests_Responses.generated.h"



/**
*
*/
USTRUCT()
struct FLogin_Response
{
	GENERATED_BODY()

		UPROPERTY()
		FString AuthToken;
	UPROPERTY()
		FString DisplayName;
	UPROPERTY()
		bool IsNewPlayer;
	UPROPERTY()
		FString UserId;

	FLogin_Response() : AuthToken(""), DisplayName(""), IsNewPlayer(false), UserId("")
	{}

	FLogin_Response(FString authToken, FString displayName, bool isNewPlayer, FString userId)
	{
		AuthToken = authToken;
		DisplayName = displayName;
		IsNewPlayer = isNewPlayer;
		UserId = userId;
	}
};



/**
*
*/
USTRUCT()
struct FAccountDetailsResponse
{
	GENERATED_BODY()

		UPROPERTY()
		FString DisplayName;
	UPROPERTY()
		FString UserId;
	UPROPERTY()
		int Level;
	UPROPERTY()
		int Gold;

	FAccountDetailsResponse() : DisplayName(""), UserId(""), Level(0), Gold(0) {}

	FAccountDetailsResponse(FString displayName, FString userId, int level, int gold)
		: DisplayName(displayName), UserId(userId), Level(level), Gold(gold) {}
};


/**
*
*/
USTRUCT()
struct FListVirtualGoodsResponse
{
	GENERATED_BODY()

		UPROPERTY()
		FString name;
	UPROPERTY()
		FString description;
	UPROPERTY()
		bool disabled;
	UPROPERTY()
		FString shortCode;

};

/**
*
*/
USTRUCT()
struct FCollectionSetResponse
{
	GENERATED_BODY()

		UPROPERTY()
		TArray<FString> Units;
	UPROPERTY()
		FString Name;
};

/**
*
*/
USTRUCT()
struct FCollectionSetsResponse
{
	GENERATED_BODY()

		UPROPERTY()
		TArray<FCollectionSetResponse> sets;
};