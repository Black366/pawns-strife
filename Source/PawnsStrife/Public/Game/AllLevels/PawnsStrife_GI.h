// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Requests_Responses.h"

#include "CoreMinimal.h"
#include "Engine/GameInstance.h"
#include "OnlineSessionInterface.h"
#include "MoviePlayer.h"
#include "PawnsStrife_GI.generated.h"


typedef TSharedPtr<class IOnlineSession, ESPMode::ThreadSafe> IOnlineSessionPtr;
namespace EOnJoinSessionCompleteResult { enum Type; }

/* ENUM FOR TRACKING GAME STATE */
UENUM()
enum class EGameState : uint8
{
	ENone				UMETA(DisplayName = "None"),
	ELoadingScreen	UMETA(DisplayName = "Loading Screen"),
	ELoginMenu		UMETA(DisplayName = "Login Menu"),
	EMainMenu		UMETA(DisplayName = "Main Menu"),
	EPlaying			UMETA(DisplayName = "Multiplayer In Game"),
};

/* ENUM TO TRACK INPUT STATES */
UENUM()
enum class EInputMode : uint8
{
	EUIOnly			UMETA(DisplayName = "UI Only"),
	EUIAndGame		UMETA(DisplayName = "UI And Game"),
	EGameOnly		UMETA(DisplayName = "Game Only"),
};

/**
 *
 */
UCLASS()
class PAWNSSTRIFE_API UPawnsStrife_GI : public UGameInstance
{
	GENERATED_BODY()

protected:
	/* Called when game start */
	virtual void Init() override;
	/* Called when game ends */
	virtual void Shutdown() override;

public:
	UPawnsStrife_GI(const FObjectInitializer& ObjectInitializer);


	/* LEVELS, TRAVELLING */

	/* Load Login Menu level */
	UFUNCTION()
		void LoadLoginMenuLevel();
	/* Load Main Menu Level */
	UFUNCTION()
		void LoadMainMenuLevel();
	UFUNCTION()
		void ReturnToMainMenu();

	/* Begin loading screen between change map. From Movie Player */
	UFUNCTION()
		virtual void BeginLoadingScreen(const FString& MapName);


	/* PLAYER, SAVING, ONLINE SUBSYSTEM, SESSIONS, GAMESPARKS */

	/* Set Name to know in battle which deck player select */
	UPROPERTY()
		FString SelectedSetName;

	/* Data Table Object */
	UPROPERTY()
		class UDataTable* DataTable;

	/* instance of Game Sparks */
	UPROPERTY()
		class UGameSparksObject* gameSparks;

	/* Start finding session */
	void FindSessions();
	/* Start session */
	void StartSession();

	/* Log in user with steam */
	UFUNCTION()
		void SteamLogin();

	/* Result of authentication response */
	void LoginResult(bool isSuccess, FLogin_Response loginResponse = FLogin_Response());
	/* Result of account details response */
	void AccountDetailsResp(bool isSuccess, 
		FAccountDetailsResponse accDetailsResp = FAccountDetailsResponse());

	UFUNCTION()
		void SetupSaveGame();


	/* GAME STATE, INPUT MODE */

	/* Change current state to new game state */
	UFUNCTION()
		void ChangeState(EGameState newState);

	/* Get the current game state */
	UFUNCTION()
		EGameState GetGameState();

	/* setting input mode */
	UFUNCTION()
		void SetInputMode(EInputMode newInputMode, bool bShowMouseCursor);

private:
	/* current input mode */
	EInputMode currentInputMode;
	/* should currently display the mouse cursor */
	bool bIsShowingMouseCursor;

	/* current game state */
	EGameState currentState;

	/* entering new state */
	void EnterState(EGameState newState);
	/* leaving current state */
	void LeaveState();


	/* SESSION INTERFACE */

	/* Instance of session interface */
	IOnlineSessionPtr SessionInterface;

	/* Pointer to search session */
	TSharedPtr<class FOnlineSessionSearch> SessionSearch;

	/* Name of Server which will player travel */
	FString DesiredServerName;

	/* Call when process of creating session is completed */
	void OnCreateSessionComplete(FName SessionName, bool Success);
	/* Call when process of destroing session is completed */
	void OnDestroySessionComplete(FName SessionName, bool Success);
	/* Call when process of finding session is completed */
	void OnFindSessionComplete(bool Success);
	/* Call when process of joining session is complete */
	void OnJoinSessionComplete(FName SessionName, EOnJoinSessionCompleteResult::Type Result);

	/* Host game when not find other hosted games */
	void Host();

	/* Join to existing hosted game */
	void Join();

	/* Create session */
	void CreateSession();


	/* UMG */

	/* Current displayed widget */
	UPROPERTY()
		class UUserWidget* currentWidget;

	/* Store Login Menu from BP */
	TSubclassOf<class UUserWidget> LoginMenuClass;
	/* Store Main Menu from BP */
	TSubclassOf<class UUserWidget> MainMenuClass;
	/* Store Loading Screen from BP */
	TSubclassOf<class UUserWidget> LoadingScreenClass;

	/* Create Login Menu Widget and add to viewport */
	void LoadLoginMenuWidget();
	/* Create Main Menu Widget and add to viewport */
	void LoadMainMenuWidget();
	/* Create Loading Widget and add to viewport */
	void LoadLoadingScreenWidget();
};


