// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameMode.h"
#include "PawnsStrife_GM.generated.h"

/**
 *
 */
UCLASS()
class PAWNSSTRIFE_API APawnsStrife_GM : public AGameMode
{
	GENERATED_BODY()

protected:
	virtual void BeginPlay() override;
	virtual void EndPlay(EEndPlayReason::Type reason) override;

	virtual void PostLogin(APlayerController* NewPlayer) override;
	virtual void Logout(AController* Exisiting) override;

	virtual bool ReadyToStartMatch_Implementation() override;

	/* Base function called from child gm class when gs change available status */
	UFUNCTION()
		virtual void OnGameSparksAvailable(bool available);

	int MaxNumberOfPlayers = 2;

public:
	APawnsStrife_GM(const FObjectInitializer& ObjectInitializer);
};
