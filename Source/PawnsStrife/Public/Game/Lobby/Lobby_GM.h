// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Game/AllLevels/PawnsStrife_GM.h"
#include "Lobby_GM.generated.h"

/**
 * 
 */
UCLASS()
class PAWNSSTRIFE_API ALobby_GM : public APawnsStrife_GM
{

	GENERATED_BODY()
	
	
protected:
	virtual void BeginPlay() override;
	virtual void EndPlay(EEndPlayReason::Type reason) override;

	virtual void PostLogin(APlayerController* NewPlayer) override;
	virtual void Logout(AController* Exisiting) override;

public:
	ALobby_GM(const FObjectInitializer& ObjectInitializer);
};
