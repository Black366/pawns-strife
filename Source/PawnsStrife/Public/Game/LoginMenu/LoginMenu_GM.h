// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Game/AllLevels/PawnsStrife_GM.h"
#include "LoginMenu_GM.generated.h"


/**
 *
 */
UCLASS()
class PAWNSSTRIFE_API ALoginMenu_GM : public APawnsStrife_GM
{
	GENERATED_BODY()


protected:
	virtual void BeginPlay() override;
	virtual void EndPlay(EEndPlayReason::Type reason) override;

	/* Child function to monitor game spark change availability */
	//UFUNCTION()
		virtual void OnGameSparksAvailable(bool available) override;

public:
	ALoginMenu_GM(const FObjectInitializer& ObjectInitializer);

	UFUNCTION(Server, WithValidation, Reliable)
		void SteamLogin();
};
