// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "GridManager.generated.h"

UCLASS()
class PAWNSSTRIFE_API AGridManager : public AActor
{
	GENERATED_BODY()

public:
	AGridManager();

protected:
	virtual void BeginPlay() override;

public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		class USceneComponent* SceneComponent;

	UPROPERTY()
		TArray<class UStaticMeshComponent*> RangeMoveTileComponents;
	UPROPERTY()
		TArray<class UStaticMeshComponent*> RangeAttackTileComponents;

	int GridSizeX = 6;
	int GridSizeY = 8;

	UFUNCTION()
		const FVector IndexToLocation(const int32 index);

	UFUNCTION()
		int32 LocationToIndex(FVector location);
	UFUNCTION()
		void SpawnMoveMarkerTiles(TArray<int32> indexes);
	UFUNCTION()
		void DeleteMoveMarkerTiles();

	UFUNCTION()
		void SpawnAttackMarkerTiles(TArray<int32> indexes);
	UFUNCTION()
		void DeleteAttackMarkerTiles();

	UFUNCTION()
		bool IsUnitOnTile(const int32 index);

private:
	UPROPERTY(Replicated)
		class APlaying_GS* GameStateInstance;

	class UStaticMesh* DefaultTileMesh;
	class UStaticMesh* RangeMoveTileStaticMesh;
	class UStaticMesh* AttackRangeTileStaticMesh;
	class UStaticMesh* AttackRangeTrueTileStaticMesh;

	float TileBoundsX;
	float TileBoundsY;
	float TileBoundsZ;

	TArray<FVector> VectorFieldArray;
};
