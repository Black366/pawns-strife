// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Game/AllLevels/PawnsStrife_GM.h"
#include "Playing_GM.generated.h"

/**
 *
 */
UCLASS()
class PAWNSSTRIFE_API APlaying_GM : public APawnsStrife_GM
{
	GENERATED_BODY()

protected:
	virtual void BeginPlay() override;
	virtual void EndPlay(EEndPlayReason::Type reason) override;

	virtual void PostLogin(APlayerController* NewPlayer) override;
	virtual void Logout(AController* Exisiting) override;

	virtual bool ReadyToStartMatch_Implementation() override;

	/* Called when the state transitions to InProgress */
	void HandleMatchHasStarted() override;

public:
	TArray<class APlaying_PC*> PlayerControllerList;

	APlaying_GM(const FObjectInitializer& ObjectInitializer);
};
