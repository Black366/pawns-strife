// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameState.h"
#include "Runtime/Engine/Public/TimerManager.h"
#include "Playing_GS.generated.h"


USTRUCT()
struct FGameTime
{
	GENERATED_BODY()

		UPROPERTY()
		int SecondsGameTime = 0;
	UPROPERTY()
		int MinutesGameTime = 5;

};

/**
 *
 */
UCLASS()
class PAWNSSTRIFE_API APlaying_GS : public AGameState
{
	GENERATED_BODY()

public:
	UFUNCTION(Server, Unreliable, WithValidation)
		void Server_OnGameStart();
	UFUNCTION()
		void OnGameEnd(bool isPlayer1Win);

	UFUNCTION()
		TArray<class AUnit*> GetUnitsOnBoard(const int32 playerID);
	UFUNCTION()
		TArray<class AUnit*> GetAllUnitsOnBoard();
	UFUNCTION()
		TArray<int> GetAllUnitsOnBoardIndexes();

	UFUNCTION()
		class AUnit* GetUnitByTileIndex(int32 index);
	UFUNCTION()
		void UpdateBoardUnitsIndexes();

	UFUNCTION()
		void AddUnitToBoard(class AUnit* unit, int8 playerID);
	UFUNCTION()
		void RemoveUnitFromBoard(class AUnit* unit, int8 playerID);

	UFUNCTION(Server, Reliable, WithValidation)
		void Server_ChangeTurn();

	/* TIME */
	UPROPERTY(Replicated)
		FGameTime GameTimePlayer1;
	UPROPERTY(Replicated)
		FGameTime GameTimePlayer2;

	UFUNCTION()
		void GameTimer(int32 playerID);

	UFUNCTION()
		void HandleGameTimer(int32 playerID);

	FTimerHandle TimerHandle;

private:
	UPROPERTY(Replicated)
		TArray<class AUnit*> UnitsOnBoardPlayer1;
	UPROPERTY(Replicated)
		TArray<class AUnit*> UnitsOnBoardPlayer2;
	UPROPERTY(Replicated)
		TArray<AUnit*> AllUnitsOnBoard;
	UPROPERTY(Replicated)
		TArray<int32> AllUnitsOnBoardIndexes;

	UPROPERTY()
		TArray<class APlaying_PC*> PlayerTurnArray;

	void ReturnToMainMenu();
};
