// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Tile.generated.h"

UCLASS()
class PAWNSSTRIFE_API ATile : public AActor
{
	GENERATED_BODY()
	
public:	
	ATile();

	UPROPERTY()
		class USceneComponent* SceneComponent;

	UPROPERTY()
		class UStaticMeshComponent* TileMeshComponent;

	const float GetTileBoundX();
	const float GetTileBoundY();

private:
	class UStaticMesh* DefaultTileMesh;

	float TileBoundsX;
	float TileBoundsY;

};
