// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerState.h"
#include "PawnsStrife_PS.generated.h"

/**
 *
 */
UCLASS()
class PAWNSSTRIFE_API APawnsStrife_PS : public APlayerState
{
	GENERATED_BODY()


public:

	//UPROPERTY()
		//FString PlayerName;
	UPROPERTY()
		FString AuthToken;
	UPROPERTY()
		FString UserId;
	UPROPERTY()
		bool IsNewPlayer;
	UPROPERTY(meta = (ClampMin = 0))
		int Level;

};
