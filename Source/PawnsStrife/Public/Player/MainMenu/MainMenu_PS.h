// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "player/AllLevels/PawnsStrife_PS.h"
#include "MainMenu_PS.generated.h"

/**
 * 
 */
UCLASS()
class PAWNSSTRIFE_API AMainMenu_PS : public APawnsStrife_PS
{
	GENERATED_BODY()
	
public:
	UPROPERTY(meta = (ClampMin = 0))
		int Gold;
	
	
};
