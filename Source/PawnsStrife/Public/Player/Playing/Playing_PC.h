// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "Playing_PC.generated.h"

UENUM()
enum class ETurnState : uint8
{
	ETurnActive			UMETA(DisplayName = "Turn Active"),
	ETurnInactive		UMETA(DisplayName = "Turn Inactive")
};

UENUM()
enum class EPlayerState : uint8
{
	EUnitInteraction	UMETA(DisplayName = "Unit Interaction"),
	EPendingAction		UMETA(DisplayName = "Pending Action")
};

/**
 *
 */
UCLASS()
class PAWNSSTRIFE_API APlaying_PC : public APlayerController
{
	GENERATED_BODY()

protected:
	virtual void BeginPlay() override;
	virtual void SetupInputComponent() override;

public:
	APlaying_PC(const FObjectInitializer& ObjectInitializer);

	UFUNCTION()
		void PlayUnitFromSet(const FString& unitName, int32 boardIndex, FRotator rotation);

	UFUNCTION(Server, Reliable, WithValidation)
		void Server_UpdatePlayerState();

	UFUNCTION(Client, Reliable, WithValidation)
		void Client_OnPostLoginSetup();

	UFUNCTION(Client, Reliable, WithValidation)
		void Client_OnMatchEnd(bool isPlayer1Win, int8 playerID);

	UFUNCTION()
		void OnGameStart();
	UFUNCTION()
		void OnSetupSet();

	UFUNCTION(Client, Reliable)
		void Client_LoadSet();
	UFUNCTION(Server, Reliable, WithValidation)
		void Server_ReturnPlayerSet(const FString& SetName, const TArray<FString>& Units);

	UFUNCTION(Client, Reliable, WithValidation)
		void Client_UpdateGameUI();


	UFUNCTION(Server, Reliable, WithValidation)
		void Server_RequestChangeTurn();

	UFUNCTION(Server, Reliable, WithValidation)
		void Server_RequestEndGame(bool isPlayer1Win);

	UFUNCTION()
		void ChangePlayerTurnState(bool isTurnActive);

	UFUNCTION()
		void DealDamageToUnit(class AUnit* DamageCauser, class AUnit* unitToDamage);

	UFUNCTION(Server, Reliable, WithValidation)
		void Server_RequestDealDamageToUnit(class AUnit* DamageCauser, class AUnit* unitToDamage);

	UFUNCTION(Server, Unreliable, WithValidation)
		void Server_OnSelectedUnit(AUnit* interactionUnit, int8 selectingPlayerID);
	UFUNCTION(Server, Unreliable, WithValidation)
		void Server_DisableUnitSelection(AUnit* unitToDeselection);

private:
	UPROPERTY(Replicated)
		class AGridManager* GridManagerInstance;

	UPROPERTY(Replicated)
		TArray<FString> PlayerSet;

	UPROPERTY()
		EPlayerState PlayerStateEnum = EPlayerState::EPendingAction;

	/* TURNS START */
	UPROPERTY(ReplicatedUsing = OnRep_TurnState)
		ETurnState TurnState;
	UFUNCTION()
		virtual void OnRep_TurnState();

	UPROPERTY(ReplicatedUsing = OnRep_IsTurnActive)
		bool IsTurnActive;
	UFUNCTION()
		virtual void OnRep_IsTurnActive();
	/* END TURNS */

	UFUNCTION()
		void SetupSet(FString SetName, TArray<FString> Units);

	UFUNCTION()
		TArray<FString> LoadSetUnits();
	UFUNCTION()
		FString GetPlayerSetName();

	void RemoveUnitFromSet(const int32 setIndex);

	void SetupGameUI();



	UPROPERTY(Replicated)
		class AUnit* HitUnit;
	UPROPERTY(Replicated)
		class AUnit* InteractionUnit;
	UPROPERTY()
		int32 HitTileIndex;
	UPROPERTY()
		int32 HitAttackTileIndex;

	UPROPERTY()
		TArray<int> AllowedMovedIndexes;
	UPROPERTY()
		TArray<int> AllowedAttackIndexes;

	UPROPERTY(Replicated)
		bool IsUnitSelected;


	void OnLeftMouseDoubleClicked();
	void OnLeftMouseClicked();
	void OnRightMouseClicked();
	void OnMiddleMouseClicked();
	void OnSurrenderGame();

	void UnitInterations();
	bool DetectGridTiles();

	void SetPlayerState(EPlayerState newPlayerState);

	void CleanUnitInteractionState();


	UFUNCTION(Server, Reliable, WithValidation)
		void Server_SetUnitLocation(FVector location, AUnit* unit);


	UPROPERTY()
		class UGameUI_UW* GameUIWidget;
	TSubclassOf<class UUserWidget> PlayingPlayerUIClass;

	UPROPERTY()
		class UGameResult_UW* GameResultWidget;
	TSubclassOf<class UUserWidget> GameResultUIClass;

	UPROPERTY()
		class USurrenderPanel_UW* SurrenderPanelWidget;
	TSubclassOf<class UUserWidget> SurrenderPanelUIClass;

	UFUNCTION(Client, Reliable)
		void Client_RequestChangeState(EGameState newState);
};
