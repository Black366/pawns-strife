// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerState.h"
#include "Playing_PS.generated.h"

/**
 *
 */
UCLASS()
class PAWNSSTRIFE_API APlaying_PS : public APlayerState
{
	GENERATED_BODY()

public:
	APlaying_PS(const FObjectInitializer& ObjectInitializer);


	UPROPERTY(Replicated)
		FString PSPlayerName;
	UPROPERTY(Replicated)
		int32 PSPlayerLevel;

	UPROPERTY(Replicated)
		int8 PawnsStrifePlayerID;

	UPROPERTY(Replicated)
		int32 MaxUnitsInSet;

	UPROPERTY(ReplicatedUsing = OnRep_TurnNumber)
		int32 TurnNumber;
	UFUNCTION()
		virtual void OnRep_TurnNumber();

	void UpdatePlayerUnitsState(int32 unitsOnBoard, int32 unitsInSet);

private:
	UPROPERTY(ReplicatedUsing = OnRep_UnitsOnBoard)
		int32 UnitsOnBoard;
	UFUNCTION()
		virtual void OnRep_UnitsOnBoard();

	UPROPERTY(ReplicatedUsing = OnRep_UnitsInSet)
		int32 UnitsInSet;
	UFUNCTION()
		virtual void OnRep_UnitsInSet();

public:
	const int32 GetUnitsOnBoardCount();
	const int32 GetUnitsInSetCount();
};
