// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/SaveGame.h"
#include "PlayerInfo.h"
#include "SetsSaveGame.generated.h"

/**
 *
 */
UCLASS()
class PAWNSSTRIFE_API USetsSaveGame : public USaveGame
{
	GENERATED_BODY()


public:
	/* All sets which player have, one name slot */
	UPROPERTY()
	TArray<FString> SetsNames;

};
