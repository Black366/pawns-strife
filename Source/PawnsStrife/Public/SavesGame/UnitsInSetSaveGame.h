// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/SaveGame.h"
#include "UnitsInSetSaveGame.generated.h"

/**
 * 
 */
UCLASS()
class PAWNSSTRIFE_API UUnitsInSetSaveGame : public USaveGame
{
	GENERATED_BODY()
	
public:
	/* Units in specific set, slot name = set name */
	UPROPERTY()
	TArray<FString> Units;
	
};
