// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include <string>

#include "Json.h"
#include "JsonUtilities.h"
#include "GameSparksService.generated.h"

namespace GameSparks {
	namespace Api {
		namespace Responses {
			class AuthenticationResponse;
			class AccountDetailsResponse;
			class ListVirtualGoodsResponse;
		}
	}
	namespace Core {
		class GS;
		class GSData;
		class GSTypedResponse;
	}
}


/**
 *
 */
UCLASS()
class PAWNSSTRIFE_API UGameSparksService : public UObject
{
	GENERATED_BODY()

public:
	/* Init necessary data to later use */
	UFUNCTION()
		static void InitStatics(UWorld* world);


	template <typename StructType>
	static StructType GetStructFromJsonString(FString Response)
	{
		StructType structData;
		//FString JsonString = FString(UTF8_TO_TCHAR(Response.c_str()));

		FJsonObjectConverter::JsonObjectStringToUStruct<StructType>(Response, &structData, 0, 0);
		return structData;
	}

	/* LOGIN */

	/* Response from server after login request */
	static void OnLoginResponse(GameSparks::Core::GS& gs,
		const GameSparks::Api::Responses::AuthenticationResponse& response);

	static void OnAccountDetailsResponse(GameSparks::Core::GS& gs,
		const GameSparks::Api::Responses::AccountDetailsResponse& response);

private:
	/* World from game */
	static UWorld* World;
	/* Variable to sure before initialize necessary data */
	static bool Initialized;


};


