// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "Runtime/Engine/Classes/Engine/DataTable.h"
#include "PlayerInfo.h"
#include "UnitFunctionsLibrary.generated.h"

/**
 * 
 */
UCLASS()
class PAWNSSTRIFE_API UUnitFunctionsLibrary : public UObject
{
	GENERATED_BODY()
	

private:
	/* Data Table Object */
	static UDataTable* UnitsDataTable;

public:
	UUnitFunctionsLibrary(const FObjectInitializer& ObjectInitializer);

	static FUnitInfo GetUnitData(const FString& UnitName);

	static void SetupUnit(class AUnit* unit, int8 owningPlayerID, FString unitName);
};
