// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "LoginMenu_UW.generated.h"

/**
 * 
 */
UCLASS()
class PAWNSSTRIFE_API ULoginMenu_UW : public UUserWidget
{
	GENERATED_BODY()
	
protected:
		/* Init. Called when login menu start */
		virtual bool Initialize() override;

private:

	/* UMG ELEMENTS */

	/* Button to log in user */
	UPROPERTY(meta = (BindWidget))
		class UButton* LoginButton;
	/* Button to quit game */
	UPROPERTY(meta = (BindWidget))
		class UButton* QuitButton;

	/* UMG EXECUTE FUNCTIONS */

	/* Log in user */
	UFUNCTION()
		void LoginPressed();
	/* Quit game */
	UFUNCTION()
		void QuitPressed();
};
