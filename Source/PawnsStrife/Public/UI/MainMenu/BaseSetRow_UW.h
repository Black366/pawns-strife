// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "BaseSetRow_UW.generated.h"

/**
 *
 */
UCLASS(Abstract)
class PAWNSSTRIFE_API UBaseSetRow_UW : public UUserWidget
{
	GENERATED_BODY()

protected:
	virtual bool Initialize() override;

	UPROPERTY(meta = (BindWidget))
		class UButton* SetButton;
	UPROPERTY(meta = (BindWidget))
		class UTextBlock* SetNameTextBlock;

	FString SetName;

	UFUNCTION()
		virtual void OnSelectionButtonClicked() { unimplemented(); };

public:
	/* Reference to parent widget */
	class UUserWidget* ParentWidget;

	UFUNCTION()
		virtual void SetupRow(FString setName) { unimplemented(); };
};
