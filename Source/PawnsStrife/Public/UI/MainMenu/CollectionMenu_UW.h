// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "GameSparksService.h"
#include <string>
#include "CollectionMenu_UW.generated.h"



/**
 *
 */
UCLASS()
class PAWNSSTRIFE_API UCollectionMenu_UW : public UUserWidget
{
	GENERATED_BODY()

protected:
	virtual bool Initialize() override;

public:
	UCollectionMenu_UW(const FObjectInitializer& ObjectInitializer);

	class UUserWidget* ParentWidget;

	UFUNCTION()
		void RefreshUnitsList();
	UFUNCTION()
		void RefreshSetsList();

private:
	/* Units Grid */

	UPROPERTY()
		TSubclassOf<UUserWidget> UnitCollectionClass;

	UPROPERTY()
		TSubclassOf<UUserWidget> SetCollectionRowClass;

	UPROPERTY(meta = (BindWidget))
		class UUniformGridPanel* UnitsUniformGridPanel;
	UPROPERTY(meta = (BindWidget))
		class UVerticalBox* SetsVerticalBox;


	/* Helper Buttons */

	UPROPERTY(meta = (BindWidget))
		class UButton* SaveButton;
	UPROPERTY(meta = (BindWidget))
		class UButton* Returnbutton;

	UFUNCTION()
		void SavePressed();
	UFUNCTION()
		void ReturnPressed();
};

