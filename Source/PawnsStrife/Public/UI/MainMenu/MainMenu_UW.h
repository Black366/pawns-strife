// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "MainMenu_UW.generated.h"

/**
 *
 */
UCLASS()
class PAWNSSTRIFE_API UMainMenu_UW : public UUserWidget
{
	GENERATED_BODY()


protected:
	virtual bool Initialize() override;

private:
	/* MAIN PANEL */

	/* Switcher to change specific group of buttons for example
	when clicked Multiplayer button it will show widget with
	Fast Game Button, Ranking Game Button and Ranking Button */
	UPROPERTY(meta = (BindWidget))
		class UWidgetSwitcher* MenuFieldsWidgetSwitcher;

	/* Muliplayer Menu with group of items, include FastGame, 
	RankingGame, Ranking Buttons */
	UPROPERTY(meta = (BindWidget))
		class UWidget* MultiplayerMenu;
	/* Singleplayer Menu with group of items, 
	include StartSingleplayerGame Button */
	UPROPERTY(meta = (BindWidget))
		class UWidget* SingleplayerMenu;
	/* Collction Menu with group of items, 
	include Collection, Sets Buttons */
	UPROPERTY(meta = (BindWidget))
		class UWidget* CollectionMenu;
	/* Collction Menu with group of items, 
	include BuyStones Button */
	UPROPERTY(meta = (BindWidget))
		class UWidget* ShopMenu;

	/* Button used to change main panel to MultiplayerMenu
	by MenuFieldsWidgetSwitcher */
	UPROPERTY(meta = (BindWidget))
		class UButton* MultiplayerButton;
	/* Button used to change main panel to SingleplayerMenu
	by MenuFieldsWidgetSwitcher */
	UPROPERTY(meta = (BindWidget))
		class UButton* SingleplayerButton;
	/* Button used to change main panel to CollectionMenu
	by MenuFieldsWidgetSwitcher */
	UPROPERTY(meta = (BindWidget))
		class UButton* CollectionButton;
	/* Button used to change main panel to ShopMenu
	by MenuFieldsWidgetSwitcher */
	UPROPERTY(meta = (BindWidget))
		class UButton* ShopButton;
	/* Button used to exit game */
	UPROPERTY(meta = (BindWidget))
		class UButton* ExitButton;

	/* MultiplayerButton execute function */
	UFUNCTION()
		void OpenMultiplayerMenu();
	/* SingleplayerButton execute function */
	UFUNCTION()
		void OpenSingleplayerMenu();
	/* CollectionButton execute function */
	UFUNCTION()
		void OpenCollectionMenu();
	/* ShopButton execute function */
	UFUNCTION()
		void OpenShopMenu();
	/* ExitButton execute function */
	UFUNCTION()
		void QuitPressed();


	/* OTHER PANELS */

	/* Switcher to change panel with specific widget
	for example when clicked OpenCollectionPanelButton
	in CollectionMenu it will change displayed widget
	to panel with collection */
	UPROPERTY(meta = (BindWidget))
		class UWidgetSwitcher* PanelWidgetSwitcher;

	UPROPERTY(meta = (BindWidget))
		class UPanelWidget* MainPanel;
	UPROPERTY(meta = (BindWidget))
		class USetSelectionMenu_UW* SetSelectionMenu_WB;

	UPROPERTY(meta = (BindWidget))
		class UUserPanelMenu_UW* UserPanelMenu_WB;

	/* Button used to start fast game */
	UPROPERTY(meta = (BindWidget))
		class UButton* FastGameButton;

	/* FastGameButton execute function */
	UFUNCTION()
		void OpenSelectionDeckMenu();


	/* COLLECTION SUB MENU */

	/* Button used to open collection menu */
	UPROPERTY(meta = (BindWidget))
		class UButton* CollectionPanelButton;

	UFUNCTION()
	void OpenCollectionPanelMenu();

	UPROPERTY(meta = (BindWidget))
		class UCollectionMenu_UW* CollectionMenu_WB;

public:
	UFUNCTION()
		void StartFindOpponent();

	UFUNCTION()
		void OpenMainPanel();
};
