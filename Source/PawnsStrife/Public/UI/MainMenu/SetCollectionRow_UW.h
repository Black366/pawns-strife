// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UI/MainMenu/BaseSetRow_UW.h"
#include "SetCollectionRow_UW.generated.h"

/**
 * 
 */
UCLASS()
class PAWNSSTRIFE_API USetCollectionRow_UW : public UBaseSetRow_UW
{
	GENERATED_BODY()
	
protected:
	virtual bool Initialize() override;
	
	//UFUNCTION()
		virtual void OnSelectionButtonClicked() override;

public:
	//UFUNCTION()
		virtual void SetupRow(FString setName) override;
};
