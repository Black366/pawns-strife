// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "SetSelectionMenu_UW.generated.h"

/**
 *
 */
UCLASS()
class PAWNSSTRIFE_API USetSelectionMenu_UW : public UUserWidget
{
	GENERATED_BODY()


protected:
	virtual bool Initialize() override;

public:
	USetSelectionMenu_UW(const FObjectInitializer& ObjectInitializer);

	void RefreshDeckList();

	/* Reference to parent widget */
	class UUserWidget* ParentWidget;

private:
	UPROPERTY()
		TSubclassOf<UUserWidget> SetSelectionRowClass;

	UPROPERTY(meta = (BindWidget))
		class UPanelWidget* SetsVerticalBox;

	UPROPERTY(meta = (BindWidget))
		class UButton* ReturnButton;

	UFUNCTION()
		void ReturnToMainPanel();
};
