// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
//#include "Blueprint/UserWidget.h"
#include "BaseSetRow_UW.h"
#include "SetSelectionRow_UW.generated.h"

/**
 *
 */
UCLASS()
class PAWNSSTRIFE_API USetSelectionRow_UW : public UBaseSetRow_UW
{
	GENERATED_BODY()

protected:
	virtual bool Initialize() override;

//public:
//	/* Reference to parent widget */
//	class UUserWidget* ParentWidget;
//
//	UPROPERTY(meta = (BindWidget))
//		class UButton* SetSelectionButton;
//	UPROPERTY(meta = (BindWidget))
//		class UTextBlock* SetNameTextBlock;

//private:
//	FString SetName;

	//UFUNCTION()
		virtual void OnSelectionButtonClicked() override;

public:
	//UFUNCTION()
	virtual void SetupRow(FString setName) override;
};
