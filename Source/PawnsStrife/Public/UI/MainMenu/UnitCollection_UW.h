// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "UnitCollection_UW.generated.h"

/**
 * 
 */
UCLASS()
class PAWNSSTRIFE_API UUnitCollection_UW : public UUserWidget
{
	GENERATED_BODY()
	
	
protected:
	virtual bool Initialize() override;
	
private:
	UPROPERTY(meta = (BindWidget))
		class UTextBlock* NameTextBlock;
	UPROPERTY(meta = (BindWidget))
		class UTextBlock* AttackPointsTextBlock;
	UPROPERTY(meta = (BindWidget))
		class UTextBlock* HealthPointsTextBlock;
	UPROPERTY(meta = (BindWidget))
		class UTextBlock* CountTextBlock;

public:
	void SetUnitCollectionData(FString name, int32 attackPoints, int32 healthPoints, int32 count);

};
