// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "UserPanelMenu_UW.generated.h"

/**
 * 
 */
UCLASS()
class PAWNSSTRIFE_API UUserPanelMenu_UW : public UUserWidget
{
	GENERATED_BODY()
	
	
protected:
	virtual bool Initialize() override;

private:
		UPROPERTY(meta = (BindWidget))
		class UTextBlock* PlayerNameTextBlock;
	UPROPERTY(meta = (BindWidget))
		class UTextBlock* LevelTextBlock;
	UPROPERTY(meta = (BindWidget))
		class UTextBlock* GoldTextBlock;

public:
	class UUserWidget* ParentWidget;

	void SetUserPanelMenuData(FString playerName, int level, int gold);
};
