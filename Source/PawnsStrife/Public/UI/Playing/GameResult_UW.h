// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "GameResult_UW.generated.h"

/**
 *
 */
UCLASS()
class PAWNSSTRIFE_API UGameResult_UW : public UUserWidget
{
	GENERATED_BODY()


protected:
	virtual bool Initialize() override;

private:
	UPROPERTY(meta = (BindWidget))
		class UTextBlock* ResultGameTextBlock;

public:
	void SetGameResultWidget(bool isPlayer1Win, int8 playerID);
};
