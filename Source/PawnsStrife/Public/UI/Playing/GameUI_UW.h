// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "GameUI_UW.generated.h"

/**
 *
 */
UCLASS()
class PAWNSSTRIFE_API UGameUI_UW : public UUserWidget
{
	GENERATED_BODY()


protected:
	virtual bool Initialize() override;

public:
	UPROPERTY(meta = (BindWidget))
		class UPlayingUserPanel_UW* UserPanel_WB;

	UPROPERTY(meta = (BindWidget))
		class UPlayingOponnentPanel_UW* OponnentPanel_WB;
};
