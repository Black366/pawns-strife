// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "PlayingOponnentPanel_UW.generated.h"

/**
 *
 */
UCLASS()
class PAWNSSTRIFE_API UPlayingOponnentPanel_UW : public UUserWidget
{
	GENERATED_BODY()


protected:
	virtual bool Initialize() override;

private:
	UPROPERTY(meta = (BindWidget))
		class UTextBlock* BoardUnitsTextBlock;
	UPROPERTY(meta = (BindWidget))
		class UTextBlock* TurnsNumberTextBlock;

	UPROPERTY(meta = (BindWidget))
		class UTextBlock* PlayerGameTimeTextBlock;

	int PlayerID;

public:
	class UUserWidget* ParentWidget;

	class APlaying_GS* GS;
	class APlaying_PC* PC;
	class APlaying_PS* OponnentPS;

	void UpdateOponnentPanel();

	UFUNCTION()
		void UpdateGameTime(bool isTurnActive, int8 playerID);
	UFUNCTION()
		void SetTextGameTimePlayer1();
	UFUNCTION()
		void SetTextGameTimePlayer2();
};
