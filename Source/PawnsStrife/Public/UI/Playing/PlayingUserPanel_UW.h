// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "PlayingUserPanel_UW.generated.h"

/**
 *
 */
UCLASS()
class PAWNSSTRIFE_API UPlayingUserPanel_UW : public UUserWidget
{
	GENERATED_BODY()


protected:
	virtual bool Initialize() override;

private:
	UPROPERTY(meta = (BindWidget))
		class UImage* TurnImage;

	UPROPERTY(meta = (BindWidget))
		class UTextBlock* BoardUnitsTextBlock;
	UPROPERTY(meta = (BindWidget))
		class UTextBlock* TurnsNumberTextBlock;

	UPROPERTY(meta = (BindWidget))
		class UTextBlock* PlayerGameTimeTextBlock;

	class APlaying_PC* pc;
	class APlaying_PS* ps;

	int PlayerID;

public:
	class UUserWidget* ParentWidget;

	UFUNCTION()
		void UpdatePlayerUserPanel();
	UFUNCTION()
		void UpdateTurnImage(bool IsTurnActive);
	UFUNCTION()
		void UpdateGameTime(bool isTurnActive, int8 playerID);
	UFUNCTION()
		void SetTextGameTimePlayer1();
	UFUNCTION()
		void SetTextGameTimePlayer2();
};
