// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "SurrenderPanel_UW.generated.h"

/**
 * 
 */
UCLASS()
class PAWNSSTRIFE_API USurrenderPanel_UW : public UUserWidget
{
	GENERATED_BODY()
	
protected:
	virtual bool Initialize() override;

private:
	UPROPERTY(meta = (BindWidget))
		class UButton* YesSurrenderButton;
	UPROPERTY(meta = (BindWidget))
		class UButton* NoSurrenderButton;

	UFUNCTION()
		void OnSurrender();

	UFUNCTION()
		void OnNoSurrender();
	
};
