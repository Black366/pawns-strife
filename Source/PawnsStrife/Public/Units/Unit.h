// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "PlayerInfo.h"
#include "Unit.generated.h"



/**
 *
 */
UCLASS()
class PAWNSSTRIFE_API AUnit : public APawn
{
	GENERATED_BODY()


public:
	AUnit();

	virtual void OnConstruction(const FTransform& Transform) override;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
		class USceneComponent* SceneComponent;
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
		class UBoxComponent* BoxComponent;
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Replicated)
		class USkeletalMeshComponent* SkeletalMeshComponent;
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
		class UTextRenderComponent* NameTextComponent;
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
		class UTextRenderComponent* AttackPointsTextComponent;
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
		class UTextRenderComponent* HealthPointsTextComponent;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
		class UParticleSystemComponent* OnDamagePSC;

	UPROPERTY()
		class ASceneCapture2D* SceneCapture;

	UPROPERTY()
		class UTextureRenderTarget2D* TextureRenderTarget;

	UPROPERTY(ReplicatedUsing = OnRep_SkeletalMesh)
		class USkeletalMesh* SkeletalMesh;
	UFUNCTION()
		virtual void OnRep_SkeletalMesh();

	UPROPERTY(ReplicatedUsing = OnRep_Animation)
		class UAnimBlueprint* Animation;
	UFUNCTION()
		virtual void OnRep_Animation();

	UPROPERTY(Replicated)
		class UParticleSystem* OnDamagePS;

	UFUNCTION(Server, Reliable, WithValidation)
		void Server_SetUnitData(const FString& unitName);

	UFUNCTION(NetMulticast, Reliable, WithValidation)
		void Mulitcast_SetUnitVisual();

	UFUNCTION(NetMulticast, Unreliable)
		void Mulitcast_UpdateUnitVisual(bool bRenderDepth);

	UFUNCTION()
		void SpawnEffects(class UParticleSystem* PS, FTransform transform);

	UFUNCTION(NetMulticast, Unreliable)
		void Mulitcast_SpawnEffects(class UParticleSystem* PS, FTransform transform);

	UFUNCTION(Server, Reliable, WithValidation)
		void Server_SetUnitLocation(FVector Location);

	UFUNCTION()
		void TakesDamage(class AUnit* DamageCauser);

	UFUNCTION(Server, Reliable, WithValidation)
		void Server_OnDestroy(UParticleSystemComponent* psc);

	UFUNCTION(Server, Reliable, WithValidation)
		void Server_OnSelected(int8 selectingPlayerID);

	UFUNCTION()
		void OnSelected(int8 selectingPlayerID);
	UFUNCTION()
		void OnDeselected();

	UFUNCTION()
		void OnBeginCursor(class AActor* TouchedActor);
	UFUNCTION()
		void OnEndCursor(class AActor* TouchedActor);

	UFUNCTION()
		TArray<int> PossibleMovedIndexes();
	UFUNCTION()
		TArray<int> PossibleAttackIndexes();

private:
	UPROPERTY(Replicated)
		class AGridManager* GridManagerInstance;
	UPROPERTY(Replicated)
		class APlaying_GS* GameStateInstance;

	UFUNCTION()
		void SetUnitData(const FString& dataTableName);

	UFUNCTION()
		void SetUnitVisual();

	UFUNCTION(NetMulticast, Reliable, WithValidation)
		void Multicast_UpdateVisualComponents();

	UPROPERTY(ReplicatedUsing = OnRep_Name)
		FString Name;
	UFUNCTION()
		virtual void OnRep_Name();

	UPROPERTY(ReplicatedUsing = OnRep_AttackPoints)
		int32 AttackPoints;
	UFUNCTION()
		void OnRep_AttackPoints();

	UPROPERTY(ReplicatedUsing = OnRep_HealthPoints)
		int32 HealthPoints;
	UFUNCTION()
		void OnRep_HealthPoints();


	UPROPERTY(Replicated)
		FUnitInfo UnitInfo;

	UPROPERTY(Replicated)
		EMoveType MoveType;
	UPROPERTY(Replicated)
		EAttackType AttackType;

	UPROPERTY(ReplicatedUsing = OnRep_Scale)
		FVector Scale;
	UFUNCTION()
		void OnRep_Scale();

	UPROPERTY(ReplicatedUsing = OnRep_SceneCaptureLocation)
		FVector SceneCaptureLocation;
	UFUNCTION()
		void OnRep_SceneCaptureLocation();

	UPROPERTY(ReplicatedUsing = OnRep_TurnsAlive)
		int32 TurnsAlive;
	UFUNCTION()
		virtual void OnRep_TurnsAlive();

	UPROPERTY()
		bool IsSelected;

public:

	UPROPERTY(Replicated)
		int8 OwningPlayerID;

	UPROPERTY(Replicated)
		FText DataTableName;

	UPROPERTY(Replicated)
		int32 CurrentIndex;

	UFUNCTION()
		const int32 GetAttackPoints();

private:

	/* MOVE */
	TArray<int> GetSimpleMoveIndexes();
	TArray<int> GetSimple2MoveIndexes();
	TArray<int> GetSimple3MoveIndexes();
	TArray<int> GetSimple4MoveIndexes();
	TArray<int> GetSimple5MoveIndexes();
	TArray<int> GetStraightInfinitelyMoveIndexes();
	TArray<int> GetAroundMoveIndexes();
	TArray<int> GetAround2MoveIndexes();
	TArray<int> GetSlantInfinitelyMoveIndexes();
	TArray<int> GetUpInfSimpleMoveIndexes();
	TArray<int> GetRightLeftInfUpMoveIndexes();
	TArray<int> GetLeftRightSlantDownMoveIndexes();
	TArray<int> GetHardMoveIndexes();


	void Up1(TArray<int>& tempIndexes);
	void UpInfinity(TArray<int>& indexes);
	void Down1(TArray<int>& tempIndexes);
	void DownInfinity(TArray<int>& indexes);
	void Left1(TArray<int>& tempIndexes);
	void LeftInfinity(TArray<int>& indexes);
	void Right1(TArray<int>& tempIndexes);
	void RightInfinity(TArray<int>& indexes);
	void UpLeft1(TArray<int>& tempIndexes);
	void UpLeftSlantInfinity(TArray<int>& tempIndexes);
	void UpRight1(TArray<int>& tempIndexes);
	void UpRightSlantInfinity(TArray<int>& tempIndexes);
	void DownLeft1(TArray<int>& tempIndexes);
	void DownLeftSlantInfinity(TArray<int>& tempIndexes);
	void DownRight1(TArray<int>& tempIndexes);
	void DownRightSlantInfinity(TArray<int>& tempIndexes);

	/* END MOVE */


	/* ATTACK */
	TArray<int> GetSimpleAttackIndexes();
	TArray<int> GetSimple2AttackIndexes();
	TArray<int> GetSimple3AttackIndexes();
	TArray<int> GetSimple4AttackIndexes();
	TArray<int> GetSimple5AttackIndexes();

	void Up1Attack(TArray<int>& tempIndexes);
	void Down1Attack(TArray<int>& tempIndexes);
	void Left1Attack(TArray<int>& tempIndexes);
	void Right1Attack(TArray<int>& tempIndexes);
	void UpLeft1Attack(TArray<int>& tempIndexes);
	void UpRight1Attack(TArray<int>& tempIndexes);
	void DownLeft1Attack(TArray<int>& tempIndexes);
	void DownRight1Attack(TArray<int>& tempIndexes);

	/* END ATTACK */
};
